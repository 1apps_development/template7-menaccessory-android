package com.workdo.manaccessory.base


import android.app.Application
import androidx.multidex.MultiDex


class BaseApplication : Application() {

    companion object {
        lateinit var app: BaseApplication



        fun getInstance(): BaseApplication {
            return app
        }
    }
    override fun onCreate() {
        super.onCreate()

        app = this

        MultiDex.install(this)

    }
}