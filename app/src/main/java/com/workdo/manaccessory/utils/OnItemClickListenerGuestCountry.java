package com.workdo.manaccessory.utils;

import com.workdo.manaccessory.model.CountryDataItem;

public interface OnItemClickListenerGuestCountry {
    void onItemClickGuest(CountryDataItem item);
}
