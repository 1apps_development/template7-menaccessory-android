package com.workdo.manaccessory.utils;

import com.workdo.manaccessory.model.CountryDataItem;

public interface OnItemClickListenerCountry {
    void onItemClick(CountryDataItem item);
}
