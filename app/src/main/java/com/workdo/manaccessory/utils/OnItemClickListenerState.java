package com.workdo.manaccessory.utils;

import com.workdo.manaccessory.model.StateListData;

public interface OnItemClickListenerState {
    void onItemClickState(StateListData item);
}
