package com.workdo.manaccessory.utils

import android.app.Activity
import android.view.View
import android.view.inputmethod.InputMethodManager

object ExtensionFunctions {

    fun View.show(){
        this.visibility = View.VISIBLE
    }

    fun View.hide(){
        this.visibility = View.GONE
    }


    fun Activity.hideKeyboard() {
        val imm: InputMethodManager =
            getSystemService(Activity.INPUT_METHOD_SERVICE) as InputMethodManager
        val view = currentFocus ?: View(this)
        imm.hideSoftInputFromWindow(view.windowToken, InputMethodManager.HIDE_NOT_ALWAYS)
    }

    fun View.isDisable(){
        this.isClickable=false
        this.isEnabled=false
    }

    fun View.isEnable(){
        this.isClickable=true
        this.isEnabled=true
    }

}