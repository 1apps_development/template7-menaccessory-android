package com.workdo.manaccessory.utils;

import com.workdo.manaccessory.model.StateListData;

public interface OnItemClickListenerGuestState {
    void onItemClickStateGuest(StateListData item);
}
