package com.workdo.manaccessory.utils;

import com.workdo.manaccessory.model.CityListData;

public interface OnItemClickListenerGuestCity {
    void onItemClickCityGuest(CityListData item);
}
