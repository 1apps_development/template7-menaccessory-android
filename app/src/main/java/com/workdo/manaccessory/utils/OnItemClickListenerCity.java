package com.workdo.manaccessory.utils;

import com.workdo.manaccessory.model.CityListData;

public interface OnItemClickListenerCity {
    void onItemClickCity(CityListData item);
}
