package com.workdo.manaccessory.adapter

import android.app.Activity
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide

import com.workdo.manaccessory.api.ApiClient
import com.workdo.manaccessory.databinding.CellTestimonialsBinding
import com.workdo.manaccessory.model.ProductReview

class RattingAdapter(
    var context: Activity,
    private val rattingList: ArrayList<ProductReview>,
    private val onItemClick: (String, String) -> Unit
) : RecyclerView.Adapter<RattingAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view =CellTestimonialsBinding.inflate(LayoutInflater.from(parent.context),parent,false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {

        holder.bindItems(rattingList[position])

    }

    override fun getItemCount(): Int {
        return rattingList.size
    }

    class ViewHolder(val binding: CellTestimonialsBinding) : RecyclerView.ViewHolder(binding.root) {


        fun bindItems(data :ProductReview)= with(binding)
        {
            Glide.with(itemView.context).load(ApiClient.ImageURL.BASE_URL.plus(data.productImage)).into(binding.ivProductImage)
            Glide.with(itemView.context).load(ApiClient.ImageURL.paymentUrl.plus(data.userImage)).into(binding.ivClient)
            binding.tvReviewTitle.text=data.title.toString()
            binding.tvRattingMessage.text=data.review.toString()
            binding.tvClientName.text=data.userName.toString()
            binding.tvClientRatting.text=data.rating?.toFloat().toString()
            binding.ivratting.rating=data.rating?.toFloat()?:0.0f
        }

    }

    private fun onFilterClick(id: String, name: String) {
        onItemClick.invoke(id, name)
    }
}