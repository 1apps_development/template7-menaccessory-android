package com.workdo.manaccessory.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.res.ResourcesCompat
import androidx.recyclerview.widget.RecyclerView
import com.workdo.manaccessory.R
import com.workdo.manaccessory.databinding.CellDescriptionBinding
import com.workdo.manaccessory.model.OtherDescriptionArrayItem
import com.workdo.manaccessory.utils.ExtensionFunctions.hide
import com.workdo.manaccessory.utils.ExtensionFunctions.show

class DescriptionAdapter (val otherDescriptionArray:ArrayList<OtherDescriptionArrayItem>) :RecyclerView.Adapter<DescriptionAdapter.DescViewHolder>(){


    inner class DescViewHolder(val itemBinding: CellDescriptionBinding):RecyclerView.ViewHolder(itemBinding.root)
    {    var firsttime = 0

        fun bindItems(data: OtherDescriptionArrayItem, position: Int)= with(itemBinding)
        {
            itemBinding.tvDescription.text=data.description.toString()
            itemBinding.tvDescTitle.text=data.title.toString()
            val isVisible: Boolean = data.expand
         

        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): DescViewHolder {
        val view =CellDescriptionBinding.inflate(LayoutInflater.from(parent.context),parent,false)
        return DescViewHolder(view)
    }

    override fun onBindViewHolder(holder: DescViewHolder, position: Int) {
        holder.bindItems(otherDescriptionArray[position],position)
    }



    override fun getItemCount(): Int {
        return otherDescriptionArray.size
    }
}