package com.workdo.manaccessory.adapter

import android.app.Activity
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.workdo.manaccessory.databinding.CellTaxBinding
import com.workdo.manaccessory.model.TaxItem
import com.workdo.manaccessory.utils.SharePreference
import com.workdo.manaccessory.utils.Utils

class CartTaxListAdapter(
    private val context: Activity,
    private val taxlist: ArrayList<TaxItem>,
    private val itemClick: (Int, String) -> Unit
) : RecyclerView.Adapter<CartTaxListAdapter.TaxViewHolder>() {
    var currency = SharePreference.getStringPref(context, SharePreference.currency)

    inner class TaxViewHolder(private val binding: CellTaxBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun bind(
            data: TaxItem,
            context: Activity,
            position: Int,
            itemClick: (Int, String) -> Unit
        ) = with(binding)
        {
            binding.tvVatLabel.text = data.taxString
            binding.tvVat.text = currency.plus(Utils.getPrice(data.taxPrice.toString()))
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): TaxViewHolder {
        val view =
            CellTaxBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return TaxViewHolder(view)
    }

    override fun onBindViewHolder(holder: TaxViewHolder, position: Int) {
        holder.bind(taxlist[position], context, position, itemClick)
    }

    override fun getItemCount(): Int {

        return taxlist.size
    }
}