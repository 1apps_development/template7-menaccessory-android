package com.workdo.manaccessory.adapter

import android.annotation.SuppressLint
import android.app.Activity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.content.ContextCompat
import androidx.core.content.res.ResourcesCompat
import androidx.recyclerview.widget.RecyclerView
import com.workdo.manaccessory.R
import com.workdo.manaccessory.model.CategorylistData
import com.workdo.manaccessory.utils.ItemClick

class TagListAdapter(
    var context: Activity,
    private val mList: ArrayList<CategorylistData>,
    val callBack: ItemClick
) :
    RecyclerView.Adapter<TagListAdapter.ViewHolder>() {
    var currency: String = ""
    var currencyPosition: String = ""
    var price = ""


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.cell_tag, parent, false)

        return ViewHolder(view)
    }


    @SuppressLint("SetTextI18n", "NotifyDataSetChanged")
    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val itemSubAddons = mList[position]

        holder.tagName.text = itemSubAddons.name
        if (!itemSubAddons.isSelect) {
            holder.card.background =
                ResourcesCompat.getDrawable(context.resources, R.drawable.border_offwhite_10, null)
            holder.tagName.setTextColor(ContextCompat.getColor(context, R.color.white))
        } else {
            holder.card.background =
                ResourcesCompat.getDrawable(context.resources, R.drawable.btn_appcolor_10, null)
            holder.tagName.setTextColor(ContextCompat.getColor(context, R.color.appcolor))
        }

        holder.itemView.setOnClickListener {
            if (!itemSubAddons.isSelect) {
                holder.card.background =
                    ResourcesCompat.getDrawable(context.resources, R.drawable.border_offwhite_10, null)
               holder.tagName.setTextColor(ContextCompat.getColor(context, R.color.white))

                mList[position].isSelect = true
            } else {
                holder.card.background =
                    ResourcesCompat.getDrawable(context.resources, R.drawable.btn_appcolor_10, null)
                holder.tagName.setTextColor(ContextCompat.getColor(context, R.color.appcolor))
                mList[position].isSelect = false
            }
            callBack.onClick("Click",getName(),getId())
        }
    }

    private fun getName():  ArrayList<String> {
        val list = ArrayList<String>()

        for (i in 0 until mList.size) {
            if (mList[i].isSelect) {
                list.add( mList[i].name.toString())
            }
        }
        return list
    }

    private fun getId():  ArrayList<String> {
        val list = ArrayList<String>()

        for (i in 0 until mList.size) {
            if (mList[i].isSelect) {
                list.add( mList[i].id.toString())
            }
        }
        return list
    }


    override fun getItemCount(): Int {
        return mList.size
    }

    class ViewHolder(ItemView: View) : RecyclerView.ViewHolder(ItemView) {
        val tagName: TextView = itemView.findViewById(R.id.tagName)
        val card: ConstraintLayout = itemView.findViewById(R.id.card)
    }

}