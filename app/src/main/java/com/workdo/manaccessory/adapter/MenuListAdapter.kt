package com.workdo.manaccessory.adapter

import android.app.Activity
import android.content.Intent
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.workdo.manaccessory.R
import com.workdo.manaccessory.model.DataItems
import com.workdo.manaccessory.ui.activity.ActMenuCatProduct

class MenuListAdapter(
    var context: Activity,
    private val mList: List<DataItems>,
    private val onFilterClick: (Int, String) -> Unit
) : RecyclerView.Adapter<MenuListAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.cell_menu, parent, false)

        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {

        val categoriesModel = mList[position]
        holder.tvProductName.text = categoriesModel.name
        val isVisible: Boolean = categoriesModel.expand

        holder.itemView.setOnClickListener {
            val intent = Intent(holder.itemView.context, ActMenuCatProduct::class.java)
         //   intent.putExtra("main_id", categoriesModel.maincategoryId.toString())
            intent.putExtra("name",categoriesModel.name.toString())
            intent.putExtra("main_id",categoriesModel.id.toString())
            intent.putExtra(
                "menu",
                "menu"
            )
            holder.itemView.context.startActivity(intent)
            context.finish()

    }

    }

    override fun getItemCount(): Int {
        return mList.size
    }

    class ViewHolder(ItemView: View) : RecyclerView.ViewHolder(ItemView) {
        val tvProductName: TextView = itemView.findViewById(R.id.tvProductName)
    }

    private fun onFilterClick(id: Int, name: String) {
        onFilterClick.invoke(id, name)
    }
}