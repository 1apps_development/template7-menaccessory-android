package com.workdo.manaccessory.adapter

import android.app.Activity
import android.util.Log
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.core.content.res.ResourcesCompat
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.workdo.manaccessory.R
import com.workdo.manaccessory.api.ApiClient
import com.workdo.manaccessory.databinding.CellFeaturedProductBinding
import com.workdo.manaccessory.model.FeaturedProductsSub
import com.workdo.manaccessory.utils.Constants
import com.workdo.manaccessory.utils.ExtensionFunctions.hide
import com.workdo.manaccessory.utils.ExtensionFunctions.show
import com.workdo.manaccessory.utils.SharePreference
import com.workdo.manaccessory.utils.Utils

class FeaturedAdapter(
    private val context: Activity,
    private val providerList: ArrayList<FeaturedProductsSub>,
    private val itemClick: (Int, String) -> Unit
) : RecyclerView.Adapter<FeaturedAdapter.FeaturedViewHolder>() {
    var currency = SharePreference.getStringPref(context, SharePreference.currency_name).toString()

    inner class FeaturedViewHolder(private val binding: CellFeaturedProductBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun bind(
            data: FeaturedProductsSub,
            context: Activity,
            position: Int,
            itemClick: (Int, String) -> Unit
        ) = with(binding)
        {
            Log.e("Cuurent", currency.toString())
            Glide.with(context).load(ApiClient.ImageURL.BASE_URL.plus(data.coverImagePath))
                .into(binding.ivProduct)
            binding.tvProductName.text = data.name.toString()
            binding.tvPrice.text =
                Utils.getPrice(data.finalPrice.toString())
            binding.tvcurrenytype.text = currency
            binding.tvTagName.text = data.tagApi.toString()

            if (data.inWhishlist== true) {
                binding.ivWishlist.background =
                    ResourcesCompat.getDrawable(context.resources, R.drawable.ic_like, null)
            } else {
                binding.ivWishlist.background =
                    ResourcesCompat.getDrawable(context.resources, R.drawable.ic_dislike, null)
            }
            if(data.variantName.isNullOrEmpty())
            {
                constraintItemDetail.hide()
            }
            if(Utils.isLogin(context))
            {
                binding.ivWishlist.show()
            }else
            {
                binding.ivWishlist.hide()

            }
            binding.ivWishlist.setOnClickListener {
                itemClick(position, Constants.FavClick)
            }
            binding.btnAddToCart.setOnClickListener {
                itemClick(position, Constants.CartClick)
            }
            itemView.setOnClickListener {
                itemClick(position, Constants.ItemClick)
            }




        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): FeaturedViewHolder {
        val view =
            CellFeaturedProductBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return FeaturedViewHolder(view)
    }

    override fun onBindViewHolder(holder: FeaturedViewHolder, position: Int) {
        holder.bind(providerList[position], context, position, itemClick)
    }

    override fun getItemCount(): Int {

        return providerList.size
    }
}