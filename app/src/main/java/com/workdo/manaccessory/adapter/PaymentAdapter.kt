package com.workdo.manaccessory.adapter

import android.app.Activity
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.core.content.res.ResourcesCompat
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.workdo.manaccessory.R
import com.workdo.manaccessory.api.ApiClient
import com.workdo.manaccessory.databinding.CellPaymentBinding
import com.workdo.manaccessory.model.PaymentData
import com.workdo.manaccessory.utils.Constants
import com.workdo.manaccessory.utils.SharePreference

class PaymentAdapter(
    private val context: Activity,
    private val paymentList: ArrayList<PaymentData>,
    private val itemClick: (Int, String) -> Unit
) : RecyclerView.Adapter<PaymentAdapter.PaymentViewHolder>() {
    var firsttime = 0

    inner class PaymentViewHolder(private val binding: CellPaymentBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun bind(
            data: PaymentData,
            context: Activity,
            position: Int,
            itemClick: (Int, String) -> Unit
        ) = with(binding)
        {
            if (firsttime == 0) {
                for (i in 0 until paymentList.size) {
                    paymentList[0].isSelect = true
                }
            }
            if (data.isSelect == true) {
                binding.ivChecked.setImageDrawable(
                    ResourcesCompat.getDrawable(
                        itemView.context.resources,
                        R.drawable.ic_round_checked, null
                    )
                )
                binding.clMain.background =
                    ResourcesCompat.getDrawable(
                        context.resources,
                        R.drawable.border_offwhite_10,
                        null
                    )
            } else {
                binding.ivChecked.setImageDrawable(
                    ResourcesCompat.getDrawable(
                        itemView.context.resources,
                        R.drawable.ic_round_unchecked, null
                    )
                )
                binding.clMain.background =
                    ResourcesCompat.getDrawable(context.resources, R.drawable.bg_gray_10, null)
            }
            Glide.with(context).load(ApiClient.ImageURL.paymentUrl.plus(data.image))
                .into(binding.ivPaymentType)
            binding.tvPaymentDesc.text = paymentList[position].detail
            binding.tvPaymentName.text = paymentList[position].nameString
            itemView.setOnClickListener {
                firsttime = 1
                paymentList[0].isSelect = false
                for (element in paymentList) {
                    element.isSelect = false

                }
                data.isSelect = true
                notifyDataSetChanged()
                itemClick(position, Constants.ItemClick)
            }

            Log.e("PaymnetType", data.name.toString())

            if (data.isSelect == true) {
                    val paymentName = data.name.toString()
                    val stripeKey = data.stripePublishableKey.toString()

                    Log.e("PaymnetType", paymentName)
                    SharePreference.setStringPref(
                        context,
                        SharePreference.Payment_Type,
                        paymentName
                    )
                    SharePreference.setStringPref(
                        context,
                        SharePreference.stripeKey,
                        stripeKey
                    )
                    SharePreference.setStringPref(
                        context,
                        SharePreference.PaymentImage,
                        paymentList[position].image.toString()
                    )
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PaymentViewHolder {
        val view =
            CellPaymentBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return PaymentViewHolder(view)
    }

    override fun onBindViewHolder(holder: PaymentViewHolder, position: Int) {
        holder.bind(paymentList[position], context, position, itemClick)
    }

    override fun getItemCount(): Int {

        return paymentList.size
    }
}