package com.workdo.manaccessory.adapter

import android.app.Activity
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.workdo.manaccessory.api.ApiClient
import com.workdo.manaccessory.databinding.CellTrandingCategoryBinding
import com.workdo.manaccessory.model.TrandingCategoriesItem

class TrendingCategoriesAdapter (private val context: Activity,
                                 private val providerList: ArrayList<TrandingCategoriesItem>,
                                 private val itemClick: (Int, String) -> Unit
) : RecyclerView.Adapter<TrendingCategoriesAdapter.FeaturedViewHolder>() {

    inner class FeaturedViewHolder(private val binding: CellTrandingCategoryBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun bind(
            data: TrandingCategoriesItem,
            context: Activity,
            position: Int,
            itemClick: (Int, String) -> Unit
        ) = with(binding)
        {

            Glide.with(context).load(ApiClient.ImageURL.BASE_URL.plus(data.imagePath))
                .into(binding.ivCategories)
            binding.tvCatName.text = data.name.toString()
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): FeaturedViewHolder {
        val view =
            CellTrandingCategoryBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return FeaturedViewHolder(view)
    }

    override fun onBindViewHolder(holder: FeaturedViewHolder, position: Int) {
        holder.bind(providerList[position], context, position, itemClick)
    }

    override fun getItemCount(): Int {

        return if(providerList.size>0) {
            1
        }else {
            0
        }

//        return providerList.size
    }
}