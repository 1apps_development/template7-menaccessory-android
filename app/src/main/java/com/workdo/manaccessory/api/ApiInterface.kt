package com.workdo.manaccessory.api

import com.workdo.manaccessory.model.OrderModel
import com.workdo.manaccessory.model.*
import com.workdo.manaccessory.model.CountryModel
import com.workdo.manaccessory.model.FargotPasswordSendOtpModel
import com.workdo.manaccessory.remote.NetworkResponse
import okhttp3.MultipartBody
import okhttp3.RequestBody
import retrofit2.http.*

interface ApiInterface {

    @POST("menswear")
    suspend fun getApiurl(@Body map: HashMap<String, String>): NetworkResponse<ApiModel, SingleResponse>

    @POST("extra-url")
    suspend fun extraUrl(@Body map: HashMap<String, String>): NetworkResponse<CommonModel, SingleResponse>

    //Todo login api
    @POST("login")
    suspend fun getLogin(@Body map: HashMap<String, String>): NetworkResponse<LoginModel, SingleResponse>

    @POST("register")
    suspend fun setRegistration(@Body map: HashMap<String, String>): NetworkResponse<RegisterModel, SingleResponse>

    @POST("fargot-password-send-otp")
    suspend fun setforgotpasswordsendotp(@Body map: HashMap<String, String>): NetworkResponse<FargotPasswordSendOtpModel, SingleResponse>

    @POST("fargot-password-verify-otp")
    suspend fun setforgotpasswordverifyotp(@Body map: HashMap<String, String>): NetworkResponse<FargotPasswordSendOtpModel, SingleResponse>

    @POST("fargot-password-save")
    suspend fun setforgotpasswordsave(@Body map: HashMap<String, String>): NetworkResponse<FargotPasswordSendOtpModel, SingleResponse>

    @POST("update-user-image")
    @Multipart
    suspend fun setUpdateUserImage(
        @Part("theme_id") themeId: RequestBody,
        @Part("user_id") userId: RequestBody,
        @Part profileimage: MultipartBody.Part?
    ): NetworkResponse<EditProfileUpdateModel, SingleResponse>

    @POST("logout")
    suspend fun setLogout(@Body map: HashMap<String, String>): NetworkResponse<SingleResponse, SingleResponse>


    @POST("currency")
    suspend fun setcurrency(@Body map: HashMap<String, String>): NetworkResponse<CommonModel, SingleResponse>

    @POST("landingpage")
    suspend fun setLandingPage(@Body map: HashMap<String, String>): NetworkResponse<LandingPageModel, SingleResponse>

    @POST("featured-products")
    suspend fun setFeaturedProducts(@Body map: HashMap<String, String>): NetworkResponse<FeaturedProductsModel, SingleResponse>

    @POST("categorys-product")
    suspend fun setCategorysProduct(
        @Query("page") page: String,
        @Body map: HashMap<String, String>
    ): NetworkResponse<FeaturedProductsSubModel, SingleResponse>

    @POST("categorys-product-guest")
    suspend fun setCategorysProductGuest(
        @Query("page") page: String,
        @Body map: HashMap<String, String>
    ): NetworkResponse<FeaturedProductsSubModel, SingleResponse>

    @POST("wishlist")
    suspend fun setWishlist(@Body map: HashMap<String, String>): NetworkResponse<CommonModel, SingleResponse>

    @POST("addtocart")
    suspend fun addtocart(
        @Body map: HashMap<String, String>
    ): NetworkResponse<AddToCartModel, SingleResponse>

    @POST("cart-qty")
    suspend fun cartQty(@Body map: HashMap<String, String>): NetworkResponse<CommonModel, SingleResponse>

    @POST("home-categoty")
    suspend fun setHomeCategorys(
        @Query("page") page: String,
        @Body map: HashMap<String, String>
    ): NetworkResponse<HomeCategoriesModel, SingleResponse>

    /*@POST("sub-categoty")
    suspend fun getSubCategoryList(@Body map: HashMap<String, String>): NetworkResponse<SubCategoryListModel, SingleResponse>

    @POST("sub-categoty-guest")
    suspend fun getSubCategoryListGuest(@Body map: HashMap<String, String>): NetworkResponse<SubCategoryListModel, SingleResponse>
*/
    @POST("product_banner")
    suspend fun productBanner(@Body map: HashMap<String, String>): NetworkResponse<ProductBannerModel, SingleResponse>

    @POST("bestseller")
    suspend fun setBestSeller(
        @Body map: HashMap<String, String>
    ): NetworkResponse<BestSellersModel, SingleResponse>

    @POST("bestseller-guest")
    suspend  fun setBestSellerGuest(
        @Body map: HashMap<String, String>
    ): NetworkResponse<BestSellersModel, SingleResponse>

    @POST("category-list")
    suspend fun getCategoryList(@Body map: HashMap<String, String>): NetworkResponse<CategoryListModel, SingleResponse>

    @POST("wishlist-list")
    suspend fun getwishliast(
        @Query("page") page: String,
        @Body map: HashMap<String, String>
    ): NetworkResponse<WishListListModel, SingleResponse>

    @POST("search")
    suspend fun searchList(
        @Query("page") page: String,
        @Body map: HashMap<String, String>
    ): NetworkResponse<FeaturedProductsSubModel, SingleResponse>

    @POST("search-guest")
    suspend fun searchListGuests(
        @Query("page") page: String,
        @Body map: HashMap<String, String>
    ): NetworkResponse<FeaturedProductsSubModel, SingleResponse>


    @POST("profile-update")
    suspend fun setProfileUpdate(@Body map: HashMap<String, String>): NetworkResponse<EditProfileModel, SingleResponse>

    @POST("change-password")
    suspend fun setChangePassword(@Body map: HashMap<String, String>): NetworkResponse<CommonModel, SingleResponse>

    @POST("add-address")
    suspend fun addAddress(@Body map: HashMap<String, String>): NetworkResponse<AddAddressModel, SingleResponse>

    @POST("address-list")
    suspend fun addressList(
        @Query("page") page: String,
        @Body map: HashMap<String, String>
    ): NetworkResponse<AddressListModel, SingleResponse>


    @POST("delete-address")
    suspend fun deleteAddress(@Body map: HashMap<String, String>): NetworkResponse<AddAddressModel, SingleResponse>

    @POST("update-address")
    suspend fun updateAddress(@Body map: HashMap<String, String>): NetworkResponse<AddAddressModel, SingleResponse>

    @POST("country-list")
    suspend fun setCountryList(@Body map: HashMap<String, String>): NetworkResponse<CountryModel, SingleResponse>

    @POST("state-list")
    suspend fun setStateList(@Body map: HashMap<String, String>): NetworkResponse<StateListModel, SingleResponse>

    @POST("city-list")
    suspend fun setCityList(@Body map: HashMap<String, String>): NetworkResponse<CityListModel, SingleResponse>

    @POST("navigation")
    suspend fun navigation(@Body map: HashMap<String, String>): NetworkResponse<MenulistModel, SingleResponse>

    @POST("order-list")
    suspend fun getOrderList(
        @Query("page") page: String,
        @Body map: HashMap<String, String>
    ): NetworkResponse<OrderHistoryListModel, SingleResponse>

    @POST("order-detail")
    suspend fun getOrderDetail(
        @Body map: HashMap<String, String>
    ): NetworkResponse<OrderDetailsModel, SingleResponse>

    @POST("order-status-change")
    suspend fun orderStatusChanges(@Body map: HashMap<String, String>): NetworkResponse<CommonModel, SingleResponse>

    @POST("product-return")
    suspend fun orderProductReturn(@Body map: HashMap<String, String>): NetworkResponse<CommonModel, SingleResponse>

    @POST("return-order-list")
    suspend fun getReturnOrderList(
        @Query("page") page: String,
        @Body map: HashMap<String, String>
    ): NetworkResponse<ReturnOrderModel, SingleResponse>

    @POST("loyality-reward")
    suspend fun loyalityReward(@Body map: HashMap<String, String>): NetworkResponse<CommonModel, SingleResponse>

    @POST("loyality-program-json")
    suspend fun loyalityProgramJson(@Body map: HashMap<String, String>): NetworkResponse<LoyalityModel, SingleResponse>

    @POST("product-rating")
    suspend fun productRating(
        @Body map: HashMap<String, String>
    ): NetworkResponse<CommonModel, SingleResponse>

    @POST("notify_user")
    suspend fun notifyUser(@Body map: HashMap<String, String>): NetworkResponse<CommonModel, SingleResponse>

    @POST("check-variant-stock")
    suspend fun variantStock(@Body map: HashMap<String, String>): NetworkResponse<VariantStockResponse, SingleResponse>

    @POST("product-detail-guest")
    suspend fun productDetailGuest(@Body map: HashMap<String, String>): NetworkResponse<ProductDetailResponseData, SingleResponse>

    @POST("product-detail")
    suspend fun productDetail(@Body map: HashMap<String, String>): NetworkResponse<ProductDetailResponseData, SingleResponse>

    @POST("cart-check")
    suspend fun cartcheck(@Body map: HashMap<String, String>): NetworkResponse<CouponModel, SingleResponse>

    @POST("tax-guest")
    suspend fun taxGuest(@Body map: HashMap<String, String>): NetworkResponse<TaxGuestResponse, SingleResponse>


    @POST("cart-list")
    suspend fun cartList(@Body map: HashMap<String, String>): NetworkResponse<CartListModel, SingleResponse>

    @POST("apply-coupon")
    suspend fun applyCoupon(@Body map: HashMap<String, String>): NetworkResponse<CouponModel, SingleResponse>


    @POST("delivery-list")
    suspend fun deliveryList(@Body map: HashMap<String, String>): NetworkResponse<DeliveryModel, SingleResponse>

    @POST("payment-list")
    suspend fun paymentList(@Body map: HashMap<String, String>): NetworkResponse<PyamentListModel, SingleResponse>

    @POST("confirm-order")
    suspend fun confirmOrder(@Body confirmModel: ConfirmModel): NetworkResponse<ConfirmOrderModel, SingleResponse>

    @POST("place-order")
    suspend fun placeOrder(@Body confirmModel: ConfirmModel): NetworkResponse<OrderModel, SingleResponse>

    @POST("place-order-guest")
    suspend fun guestPlaceOrder(@Body requestData: GuestRequest): NetworkResponse<OrderModel, SingleResponse>

    @POST("payment-sheet")
    suspend fun paymentSheet(@Body map: HashMap<String, String>): NetworkResponse<PaymentSheetModel, SingleResponse>

    @POST("tranding-category")
    suspend fun setTrandingCategory(@Body map: HashMap<String, String>): NetworkResponse<TrandingCategoriesModel, SingleResponse>

    @POST("tranding-category-product")
    suspend fun setTrendingProduct(
        @Body map: HashMap<String, String>
    ): NetworkResponse<FeaturedProductsSubModel, SingleResponse>


    @POST("tranding-category-product-guest")
    suspend fun setTrendingProductGuest(
        @Body map: HashMap<String, String>
    ): NetworkResponse<FeaturedProductsSubModel, SingleResponse>


    @POST("releted-product-guest")
    suspend fun relatedProductGuest(@Query("page") page: String, @Body map: HashMap<String, String>):NetworkResponse<RelatedProductResponse,SingleResponse>


    @POST("releted-product")
    suspend fun relatedProduct(@Query("page") page: String, @Body map: HashMap<String, String>):NetworkResponse<RelatedProductResponse,SingleResponse>




}

