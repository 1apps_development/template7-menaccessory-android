package com.workdo.manaccessory.api

import com.google.gson.annotations.SerializedName

data class PaymentSheetModel(

	@field:SerializedName("clientSecret")
	val clientSecret: String? = null
)
