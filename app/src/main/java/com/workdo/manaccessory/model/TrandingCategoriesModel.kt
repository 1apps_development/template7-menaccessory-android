package com.workdo.manaccessory.model

import com.google.gson.annotations.SerializedName

data class TrandingCategoriesModel(

	@field:SerializedName("data")
	val data: ArrayList<TrandingCategoriesItem>? = null,

	@field:SerializedName("message")
	val message: String? = null,

	@field:SerializedName("status")
	val status: Int? = null
)

data class TrandingCategoriesItem(
	var isSelect: Boolean = false,
	var isFirstPositio: Boolean = true,

	@field:SerializedName("message")
	val message: String? = null,

	@field:SerializedName("trending")
	val trending: Int? = null,

	@field:SerializedName("demo_field")
	val demoField: String? = null,

	@field:SerializedName("maincategory_id")
	val maincategoryId: Int? = null,

	@field:SerializedName("updated_at")
	val updatedAt: String? = null,

	@field:SerializedName("image_path")
	val imagePath: String? = null,

	@field:SerializedName("icon_path")
	val iconPath: String? = null,

	@field:SerializedName("name")
	val name: String? = null,

	@field:SerializedName("theme_id")
	val themeId: String? = null,

	@field:SerializedName("created_at")
	val createdAt: String? = null,

	@field:SerializedName("category_item")
	val categoryItem: Int? = null,

	@field:SerializedName("id")
	val id: Int? = null,

	@field:SerializedName("status")
	val status: Int? = null
)
