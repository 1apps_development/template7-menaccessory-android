package com.workdo.manaccessory.model

import com.google.gson.annotations.SerializedName

data class PyamentListModel(

    @field:SerializedName("data")
    val data: ArrayList<PaymentData>? = null,

    @field:SerializedName("message")
    val message: String? = null,

    @field:SerializedName("status")
    val status: Int? = null
)

data class PaymentData(
    @field:SerializedName("message")
    val message: String? = null,

    var isSelect: Boolean? = false,

    @field:SerializedName("image")
    val image: String? = null,

    @field:SerializedName("name")
    val name: String? = null,

    @field:SerializedName("name_string")
    val nameString: String? = null,

    @field:SerializedName("detail")
    val detail: String? = null,

    @field:SerializedName("status")
    val status: String? = null,

    @field:SerializedName("stripe_publishable_key")
    val stripePublishableKey: String? = null,

    @field:SerializedName("stripe_secret_key")
    val stripeSecretKey: String? = null
)
