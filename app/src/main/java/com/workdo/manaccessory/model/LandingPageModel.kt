package com.workdo.manaccessory.model

import com.google.gson.annotations.SerializedName

data class LandingPageModel(

	@field:SerializedName("data")
	val data: LandingPageData? = null,

	@field:SerializedName("message")
	val message: String? = null,

	@field:SerializedName("status")
	val status: Int? = null
)

data class HomepageHeader(

	@field:SerializedName("homepage-header-btn3")
	val homepageHeaderBtn3: String? = null,

	@field:SerializedName("homepage-header-sub-text3")
	val homepageHeaderSubText3: String? = null,

	@field:SerializedName("homepage-header-title-text1")
	val homepageHeaderTitleText1: String? = null,

	@field:SerializedName("homepage-header-title-text2")
	val homepageHeaderTitleText2: String? = null,

	@field:SerializedName("homepage-header-title-text3")
	val homepageHeaderTitleText3: String? = null,

	@field:SerializedName("homepage-header-sub-text1")
	val homepageHeaderSubText1: String? = null,

	@field:SerializedName("homepage-header-sub-text2")
	val homepageHeaderSubText2: String? = null,

	@field:SerializedName("homepage-header-btn2")
	val homepageHeaderBtn2: String? = null,

	@field:SerializedName("homepage-header-btn1")
	val homepageHeaderBtn1: String? = null
)

data class LandingPageData(

	@field:SerializedName("them_json")
	val themJson: ThemJson? = null,

	@field:SerializedName("message")
	val message: String? = null,

	@field:SerializedName("loyality_section")
	val loyalitySection: String? = null
)

data class HomepageNewsletter(

	@field:SerializedName("homepage-newsletter-sub-text")
	val homepageNewsletterSubText: String? = null,

	@field:SerializedName("homepage-newsletter-title-text")
	val homepageNewsletterTitleText: String? = null
)

data class HomepageFeatureProducts(

	@field:SerializedName("homepage-feature-products-title-text")
	val homepageFeatureProductsTitleText: String? = null,

	@field:SerializedName("homepage-feature-products-heading")
	val homepageFeatureProductsHeading: String? = null
)

data class HomepageTrendingProducts(

	@field:SerializedName("homepage-trending-products-heading")
	val homepageTrendingProductsHeading: String? = null,

	@field:SerializedName("homepage-trending-products-title-text")
	val homepageTrendingProductsTitleText: String? = null
)

data class ThemJson(

	@field:SerializedName("homepage-header")
	val homepageHeader: HomepageHeader? = null,

	@field:SerializedName("homepage-newsletter")
	val homepageNewsletter: HomepageNewsletter? = null,

	@field:SerializedName("homepage-products")
	val homepageProducts: HomepageProducts? = null,

	@field:SerializedName("homepage-feature-products")
	val homepageFeatureProducts: HomepageFeatureProducts? = null,

	@field:SerializedName("homepage-trending-products")
	val homepageTrendingProducts: HomepageTrendingProducts? = null
)

data class HomepageProducts(

	@field:SerializedName("homepage-products-title-text")
	val homepageProductsTitleText: String? = null,

	@field:SerializedName("homepage-products-heading")
	val homepageProductsHeading: String? = null
)
