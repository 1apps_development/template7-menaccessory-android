package com.workdo.manaccessory.model

import com.google.gson.annotations.SerializedName

data class CartListModel(

	@field:SerializedName("data")
	val data: CartData? = null,

	@field:SerializedName("message")
	val message: String? = null,

	@field:SerializedName("status")
	val status: Int? = null
)

data class CartData(

	@field:SerializedName("original_price")
	val originalPrice: String? = null,

	@field:SerializedName("product_discount_price")
	val productDiscountPrice: String? = null,

	@field:SerializedName("tax_info")
	val taxInfo: ArrayList<TaxItem>? = null,

	@field:SerializedName("final_price")
	val finalPrice: String? = null,

	@field:SerializedName("sub_total")
	val subTotal: String? = null,

	@field:SerializedName("cart_total_product")
	val cartTotalProduct: Int? = null,

	@field:SerializedName("cart_total_qty")
	val cartTotalQty: Int? = null,

	@field:SerializedName("tax_price")
	val taxPrice: String? = null,

	@field:SerializedName("product_list")
	val productList: ArrayList<ProductListItem>? = null
)

data class TaxInfoItem(

	@field:SerializedName("tax_amount")
	val taxAmount: Int? = null,

	@field:SerializedName("tax_string")
	val taxString: String? = null,

	@field:SerializedName("tax_type")
	val taxType: String? = null,

	@field:SerializedName("tax_name")
	val taxName: String? = null,

	@field:SerializedName("tax_price")
	val taxPrice: String? = null,

	@field:SerializedName("id")
	val id: Int? = null
)

data class ProductListItem(

	@field:SerializedName("image")
	val image: String? = null,

	@field:SerializedName("variant_id")
	val variantId: String? = null,

	@field:SerializedName("final_price")
	val finalPrice: String? = null,

	@field:SerializedName("discount_price")
	val discountPrice: String? = null,

	@field:SerializedName("product_id")
	val productId: String? = null,

	@field:SerializedName("qty")
	var qty: Int? = null,

	@field:SerializedName("name")
	val name: String? = null,

	@field:SerializedName("per_product_discount_price")
	val perProductDiscountPrice: String? = null,

	@field:SerializedName("total_orignal_price")
	val totalOrignalPrice: String? = null,

	@field:SerializedName("variant_name")
	val variantName: String? = null,

	@field:SerializedName("orignal_price")
	val orignalPrice: String? = null
)


