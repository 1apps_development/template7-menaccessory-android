package com.workdo.manaccessory.model

import com.google.gson.annotations.SerializedName

data class CouponModel(

	@field:SerializedName("data")
	val data: CouponData? = null,

	@field:SerializedName("message")
	val message: String? = null,

	@field:SerializedName("status")
	val status: Int? = null
)

data class CouponData(

	@field:SerializedName("original_price")
	val originalPrice: String? = null,

	@field:SerializedName("coupon_discount_type")
	val couponDiscountType: String? = null,

	@field:SerializedName("amount")
	val amount: String? = null,

	@field:SerializedName("code")
	val code: String? = null,

	@field:SerializedName("coupon_code")
	val couponCode: String? = null,

	@field:SerializedName("final_price")
	val finalPrice: String? = null,

	@field:SerializedName("coupon_discount_amount")
	val couponDiscountAmount: Int? = null,

	@field:SerializedName("discount_price")
	val discountPrice: String? = null,

	@field:SerializedName("name")
	val name: String? = null,

	@field:SerializedName("coupon_end")
	val couponEnd: String? = null,

	@field:SerializedName("id")
	val id: Int? = null,

	@field:SerializedName("message")
	val message: String? = null,

	@field:SerializedName("discount_string")
	val discountString: String? = null,

	@field:SerializedName("discount_string2")
	val discountString2: String? = null
)
