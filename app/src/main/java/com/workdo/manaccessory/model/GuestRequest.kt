package com.workdo.manaccessory.model

import com.google.gson.annotations.SerializedName

data class GuestRequest(
    @field:SerializedName("billing_info")
    var billingData: BillingDetailsAddress? = null,

    @field:SerializedName("coupon_info")
    var couponData: CouponInfoPost? = null,
    @field:SerializedName("delivery_comment")
    var deliveryComment: String? = null,
    @field:SerializedName("theme_id")
    var themeId: String? = null,
    @field:SerializedName("delivery_id")
    var deliveryId: String? = null,
    @field:SerializedName("payment_comment")
    var paymentComment: String? = null,
    @field:SerializedName("payment_type")
    var paymentType: String? = null,
    @field:SerializedName("product")
    var product: ArrayList<Product>? = null,
    @field:SerializedName("tax_info")
    var taxItem: ArrayList<TaxItem>? = null)

data class Product(var product_id:String,var qty :String,var variant_id:String,)
