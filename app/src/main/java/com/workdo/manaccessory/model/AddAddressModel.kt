package com.workdo.manaccessory.model

import com.google.gson.annotations.SerializedName

data class AddAddressModel(

	@field:SerializedName("data")
	val data: Data? = null,

	@field:SerializedName("count")
	val count: Int? = null,

	@field:SerializedName("message")
	val message: String? = null,

	@field:SerializedName("status")
	val status: Int? = null
)

