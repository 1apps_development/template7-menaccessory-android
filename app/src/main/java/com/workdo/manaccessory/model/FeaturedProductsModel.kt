package com.workdo.manaccessory.model

import com.google.gson.annotations.SerializedName

data class FeaturedProductsModel(

	@field:SerializedName("data")
	val data: ArrayList<FeaturedProducts>? = null,

	@field:SerializedName("message")
	val message: String? = null,

	@field:SerializedName("status")
	val status: Int? = null
)

data class FeaturedProducts(
	var isSelect: Boolean = false,



	@field:SerializedName("maincategory_id")
	val maincategoryId: String? = null,

	@field:SerializedName("message")
	val message: String? = null,

	@field:SerializedName("updated_at")
	val updatedAt: String? = null,

	@field:SerializedName("image_path")
	val imagePath: String? = null,

	@field:SerializedName("name")
	val name: String? = null,

	@field:SerializedName("theme_id")
	val themeId: String? = null,

	@field:SerializedName("created_at")
	val createdAt: String? = null,

	@field:SerializedName("id")
	val id: String? = null,

	@field:SerializedName("status")
	val status: Int? = null
)
