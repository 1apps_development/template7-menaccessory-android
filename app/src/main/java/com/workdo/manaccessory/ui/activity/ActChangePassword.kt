package com.workdo.manaccessory.ui.activity

import android.view.View
import androidx.lifecycle.lifecycleScope
import com.workdo.manaccessory.R
import com.workdo.manaccessory.api.ApiClient
import com.workdo.manaccessory.base.BaseActivity
import com.workdo.manaccessory.databinding.ActChangePasswordBinding
import com.workdo.manaccessory.remote.NetworkResponse
import com.workdo.manaccessory.ui.authentication.ActWelCome
import com.workdo.manaccessory.utils.SharePreference
import com.workdo.manaccessory.utils.Utils
import kotlinx.coroutines.launch

class ActChangePassword : BaseActivity() {
    private lateinit var _binding: ActChangePasswordBinding
    override fun setLayout(): View = _binding.root

    override fun initView() {
        _binding = ActChangePasswordBinding.inflate(layoutInflater)
        init()
    }

    private fun init() {
        _binding.ivBack.setOnClickListener { finish() }

        _binding.btnChangePassword.setOnClickListener {
            if (_binding.edPassword.text.isNullOrEmpty()){
                Utils.errorAlert(this@ActChangePassword,resources.getString(R.string.validation_password_))
            }else if (_binding.edConfirmPassword.text.isNullOrEmpty()){
                Utils.errorAlert(this@ActChangePassword,resources.getString(R.string.validation_confirm_password))
            }
            else if (_binding.edPassword.text.toString() != _binding.edConfirmPassword.text.toString()){
                Utils.errorAlert(
                    this@ActChangePassword,
                    resources.getString(R.string.validation_valid_cpassword)
                )
            }
            else{
                val request = HashMap<String, String>()
                request["user_id"]=SharePreference.getStringPref(this@ActChangePassword,SharePreference.userId).toString()
                request["password"] = _binding.edPassword.text.toString()
                request["theme_id"] = getString(R.string.theme_id)
                callChangePasswordApi(request)
            }
        }
    }

    private fun callChangePasswordApi(request: HashMap<String, String>) {
        Utils.showLoadingProgress(this@ActChangePassword)
        lifecycleScope.launch {
            when (val response =
                ApiClient.getClient(this@ActChangePassword).setChangePassword(request)) {
                is NetworkResponse.Success -> {
                    Utils.dismissLoadingProgress()
                    val changesPasswordResponse = response.body.data
                    when (response.body.status) {
                        1 -> {
                            finish()
                        }
                        0 -> {
                            Utils.errorAlert(
                                this@ActChangePassword,
                                response.body.data?.message.toString()
                            )
                        }
                        9 -> {
                            Utils.errorAlert(
                                this@ActChangePassword,
                                response.body.message.toString()
                            )
                            openActivity(ActWelCome::class.java)
                        }
                    }
                }
                is NetworkResponse.ApiError -> {
                    if (response.body.status == 9) {
                        Utils.setInvalidToekn(this@ActChangePassword)
                    } else {
                        Utils.errorAlert(
                            this@ActChangePassword,
                            response.body.message.toString()
                        )
                    }
                }
                is NetworkResponse.NetworkError -> {
                    Utils.dismissLoadingProgress()
                    Utils.errorAlert(
                        this@ActChangePassword,
                        resources.getString(R.string.internet_connection_error)
                    )
                }
                is NetworkResponse.UnknownError -> {
                    Utils.dismissLoadingProgress()
                    Utils.errorAlert(
                        this@ActChangePassword,
                        "Something went wrong"
                    )
                }
            }
        }
    }


}