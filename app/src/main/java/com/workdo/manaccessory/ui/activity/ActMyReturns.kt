package com.workdo.manaccessory.ui.activity

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import androidx.lifecycle.lifecycleScope
import androidx.recyclerview.widget.LinearLayoutManager
import com.workdo.manaccessory.R
import com.workdo.manaccessory.adapter.ReturnOrderlistAdapter
import com.workdo.manaccessory.api.ApiClient
import com.workdo.manaccessory.base.BaseActivity
import com.workdo.manaccessory.databinding.ActMyReturnsBinding
import com.workdo.manaccessory.model.ReturnOrderItem
import com.workdo.manaccessory.remote.NetworkResponse
import com.workdo.manaccessory.ui.authentication.ActWelCome
import com.workdo.manaccessory.utils.Constants
import com.workdo.manaccessory.utils.ExtensionFunctions.hide
import com.workdo.manaccessory.utils.ExtensionFunctions.show
import com.workdo.manaccessory.utils.SharePreference
import com.workdo.manaccessory.utils.Utils
import kotlinx.coroutines.launch


class ActMyReturns : BaseActivity() {
    private lateinit var _binding:ActMyReturnsBinding
    internal var isLoading = false
    internal var isLastPage = false
    private var currentPage = 1
    private var total_pages: Int = 0
    private var orderList = ArrayList<ReturnOrderItem>()
    private lateinit var returnOrderlistAdapter: ReturnOrderlistAdapter
    private var manager: LinearLayoutManager? = null

    override fun setLayout(): View = _binding.root

    override fun initView() {
        _binding= ActMyReturnsBinding.inflate(layoutInflater)
        init()
    }

    private fun init() {
        _binding.ivBack.setOnClickListener { finish() }

        manager = LinearLayoutManager(this@ActMyReturns)
        nestedScrollViewPagination()

    }

    private fun nestedScrollViewPagination() {
        _binding.rvMyReturn.isNestedScrollingEnabled=false
        _binding.scrollView.viewTreeObserver.addOnScrollChangedListener {
            val view = _binding.scrollView.getChildAt(_binding.scrollView.childCount - 1)
            val diff =view.bottom - (_binding.scrollView.height + _binding.scrollView.scrollY)

            if (diff == 0 && !isLastPage && !isLoading){
                currentPage++
                callOrderList()
            }
        }
    }

    private fun callOrderList() {
        Utils.showLoadingProgress(this@ActMyReturns)
        val categoriesProduct = HashMap<String, String>()
        categoriesProduct["user_id"] = SharePreference.getStringPref(this@ActMyReturns, SharePreference.userId).toString()
        categoriesProduct["theme_id"] = getString(R.string.theme_id)

        lifecycleScope.launch {
            when(val response = ApiClient.getClient(this@ActMyReturns).getReturnOrderList(currentPage.toString(),categoriesProduct)){
                is NetworkResponse.Success->{
                    Utils.dismissLoadingProgress()
                    val OrderListResponse = response.body.data
                    when(response.body.status){
                        1->{
                            if (response.body.data?.data?.size?:0 > 0) {
                                _binding.rvMyReturn.show()
                                _binding.tvNoDataFound.hide()
                                currentPage = OrderListResponse?.currentPage!!.toInt()
                                total_pages = OrderListResponse.lastPage!!.toInt()
                                OrderListResponse.data?.let {
                                    orderList.addAll(it)
                                }
                                if (currentPage >= total_pages){
                                    isLastPage=true
                                }
                                isLoading=false
                            }else
                            {
                                if(orderList.size==0)
                                {
                                    _binding.rvMyReturn.hide()
                                    _binding.tvNoDataFound.show()
                                }

                            }

                            returnOrderlistAdapter.notifyDataSetChanged()
                        }

                        0->{
                            Utils.errorAlert(this@ActMyReturns,OrderListResponse?.data?.get(0)?.message.toString())
                        }
                        9 -> {
                            Utils.errorAlert(
                                this@ActMyReturns,
                                response.body.message.toString()
                            )
                            openActivity(ActWelCome::class.java)
                        }
                    }
                }
                is NetworkResponse.ApiError ->{
                    Utils.dismissLoadingProgress()
                    if (response.body.status == 9){
                        Utils.setInvalidToekn(this@ActMyReturns)
                    }else{
                        Utils.errorAlert(this@ActMyReturns,response.body.message.toString())
                    }
                }

                is NetworkResponse.NetworkError -> {
                    Utils.dismissLoadingProgress()
                    Utils.errorAlert(
                        this@ActMyReturns,
                        resources.getString(R.string.internet_connection_error)
                    )
                }

                is NetworkResponse.UnknownError -> {
                    Utils.dismissLoadingProgress()
                    Utils.errorAlert(
                        this@ActMyReturns,
                        "Something went wrong"
                    )
                }

            }
        }
    }

    private fun returnOrderListAdapter(returnOrderList: ArrayList<ReturnOrderItem>) {
        _binding.rvMyReturn.layoutManager = manager
        returnOrderlistAdapter =
            ReturnOrderlistAdapter(this@ActMyReturns, returnOrderList) { i: Int, s: String ->
                if (s == Constants.ItemClick) {
                    startActivity(Intent(this@ActMyReturns,ActOrderDetails::class.java).putExtra("order_ID",returnOrderList[i].id.toString()))
                }
            }
        _binding.rvMyReturn.adapter = returnOrderlistAdapter
    }

    override fun onResume() {
        super.onResume()
        currentPage = 1
        isLastPage = false
        isLoading = false
        orderList.clear()
        returnOrderListAdapter(orderList)

        callOrderList()
    }

}