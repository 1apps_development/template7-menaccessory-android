package com.workdo.manaccessory.ui.activity

import android.app.AlertDialog
import android.app.Dialog
import android.content.Intent
import android.content.res.ColorStateList
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.net.Uri
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.ViewGroup
import android.view.Window
import android.view.WindowManager
import android.widget.EditText
import android.widget.ImageView
import android.widget.LinearLayout
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.content.res.AppCompatResources
import androidx.appcompat.widget.AppCompatButton
import androidx.appcompat.widget.AppCompatEditText
import androidx.core.content.ContextCompat
import androidx.core.content.res.ResourcesCompat
import androidx.core.view.get
import androidx.lifecycle.lifecycleScope
import androidx.recyclerview.widget.*
import androidx.viewpager.widget.ViewPager
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import com.workdo.manaccessory.R
import com.workdo.manaccessory.adapter.*
import com.workdo.manaccessory.adapter.variant.VariantAdapter
import com.workdo.manaccessory.api.ApiClient
import com.workdo.manaccessory.databinding.ActProductDetailsBinding
import com.workdo.manaccessory.databinding.DlgConfirmBinding
import com.workdo.manaccessory.model.*
import com.workdo.manaccessory.remote.NetworkResponse
import com.workdo.manaccessory.ui.authentication.ActWelCome
import com.workdo.manaccessory.utils.Constants
import com.workdo.manaccessory.utils.ExtensionFunctions.hide
import com.workdo.manaccessory.utils.ExtensionFunctions.show
import com.workdo.manaccessory.utils.PaginationScrollListener
import com.workdo.manaccessory.utils.SharePreference
import com.workdo.manaccessory.utils.Utils
import kotlinx.coroutines.launch
import java.lang.reflect.Type

class ActProductDetails : AppCompatActivity() {

    private lateinit var adapter: ViewPagerAdapter
    private lateinit var variantAdapter: VariantAdapter
    internal var isLoading = false
    internal var isLastPage = false
    private var currentPage = 1
    private var total_pages: Int = 0

    var rattingValue = "0"
    var product_id = ""
    var variant_id = ""
    var variantName = ""
    var finalPrice = ""
    var disCountPrice = ""
    var originalPrice = ""
    var productInfo = ProductInfo()
    var productStock = ""
    var isWishList = false
    private lateinit var relatedProductAdapter: RelatedProductAdapter
    private var manager: GridLayoutManager? = null
    private var relatedProductItemList = ArrayList<RelatedProductDataItem>()


    private lateinit var _binding: ActProductDetailsBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        _binding = ActProductDetailsBinding.inflate(layoutInflater)
        setContentView(_binding.root)

        init()
        pagination()
    }


    private fun pagination() {
        val paginationListener = object : PaginationScrollListener(manager) {
            override fun isLastPage(): Boolean {
                return isLastPage
            }

            override fun isLoading(): Boolean {
                return isLoading
            }

            override fun loadMoreItems() {
                isLoading = true
                currentPage++
                callRelatedProductApi()
            }

        }

        _binding.rvRelatedItems.addOnScrollListener(paginationListener)
    }


    //Todo Related Product Api
    private fun callRelatedProductApi() {
        Utils.showLoadingProgress(this@ActProductDetails)
        val relatedProductRequest = HashMap<String, String>()
        relatedProductRequest["theme_id"] = getString(R.string.theme_id)
        relatedProductRequest["product_id"] = intent.getStringExtra(Constants.ProductId) ?: ""
        lifecycleScope.launch {
            val response =
                if (SharePreference.getStringPref(this@ActProductDetails, SharePreference.token)
                        ?.isEmpty() == true
                ) {
                    ApiClient.getClient(this@ActProductDetails)
                        .relatedProductGuest(currentPage.toString(), relatedProductRequest)
                } else {
                    ApiClient.getClient(this@ActProductDetails)
                        .relatedProduct(currentPage.toString(), relatedProductRequest)
                }

            when (response) {
                is NetworkResponse.Success -> {
                    Utils.dismissLoadingProgress()
                    val categoriesProductResponse = response.body.data
                    when (response.body.status) {
                        1 -> {
                            if ((response.body.data?.data?.size ?: 0) > 0) {
                                _binding.tvRelatedTitle.show()

                                currentPage = categoriesProductResponse?.currentPage!!.toInt()
                                total_pages = categoriesProductResponse.lastPage!!.toInt()
                                categoriesProductResponse.data?.let {
                                    relatedProductItemList.addAll(it)
                                }
                                if (currentPage >= total_pages) {
                                    isLastPage = true
                                }
                                isLoading = false
                            } else {
                                _binding.rvRelatedItems.hide()
                                _binding.tvRelatedTitle.hide()
                            }
                            relatedProductAdapter.notifyDataSetChanged()
                        }
                        0 -> {
                            Utils.errorAlert(
                                this@ActProductDetails,
                                response.body?.message.toString()
                            )
                        }
                        9 -> {
                            Utils.errorAlert(
                                this@ActProductDetails,
                                response.body?.message.toString()
                            )
                            startActivity(Intent(this@ActProductDetails, ActWelCome::class.java))
                            finish()
                            finishAffinity()
                        }
                    }
                }

                is NetworkResponse.ApiError -> {
                    Utils.dismissLoadingProgress()

                    if (response.body.status == 9) {
                        Utils.setInvalidToekn(this@ActProductDetails)
                    } else {
                        Utils.errorAlert(
                            this@ActProductDetails,
                            response.body.message.toString()
                        )
                    }
                }

                is NetworkResponse.NetworkError -> {
                    Utils.dismissLoadingProgress()
                    Utils.errorAlert(
                        this@ActProductDetails,
                        resources.getString(R.string.internet_connection_error)
                    )
                }

                is NetworkResponse.UnknownError -> {
                    Utils.dismissLoadingProgress()
                    Utils.errorAlert(
                        this@ActProductDetails,
                        "Something went wrong"
                    )
                }
            }
        }
    }


    private fun init() {

        _binding.tvCount.text =
            SharePreference.getStringPref(this@ActProductDetails, SharePreference.cartCount)
                .toString()


        if (Utils.isLogin(this@ActProductDetails)) {
            _binding.btnFeedback.show()
            _binding.ivWishlist.show()
        } else {
            _binding.btnFeedback.hide()
            _binding.ivWishlist.hide()

        }
        manager = GridLayoutManager(this@ActProductDetails, 1, GridLayoutManager.HORIZONTAL, false)
        relatedProductItemList.clear()
        relatedProductAdapter(relatedProductItemList)
        initClickListeners()
        callProductDetailApi()
        callRelatedProductApi()

    }

    override fun onResume() {
        super.onResume()
        _binding.tvCount.text =
            SharePreference.getStringPref(this@ActProductDetails, SharePreference.cartCount)
                .toString()


    }


    private fun relatedProductAdapter(relatedItemList: ArrayList<RelatedProductDataItem>) {
        _binding.rvRelatedItems.layoutManager = manager
        relatedProductAdapter =
            RelatedProductAdapter(this@ActProductDetails, relatedItemList) { i: Int, s: String ->
                if (s == Constants.FavClick) {
                    if (SharePreference.getBooleanPref(
                            this@ActProductDetails,
                            SharePreference.isLogin
                        )
                    ) {
                        Log.e("FavClick", "FavClick")
                        if (relatedItemList[i].inWhishlist == false) {
                            callWishlist(
                                "add",
                                relatedItemList[i].id.toString(),
                                "RelatedItemProduct",
                                i
                            )
                        } else {
                            callWishlist(
                                "remove",
                                relatedItemList[i].id.toString(),
                                "RelatedItemProduct", i
                            )
                        }
                    } else {
                        startActivity(Intent(this@ActProductDetails, ActWelCome::class.java))
                        finish()
                    }
                } else if (s == Constants.CartClick) {
                    val data = relatedItemList[i]
                    if (!Utils.isLogin(this@ActProductDetails)) {
                        guestUserAddToCart(
                            data,
                            relatedItemList[i].id,
                            true,
                            relatedItemList[i].defaultVariantId.toString()
                        )
                    } else {
                        val addtocart = HashMap<String, String>()
                        addtocart["user_id"] = SharePreference.getStringPref(
                            this@ActProductDetails,
                            SharePreference.userId
                        ).toString()
                        addtocart["product_id"] = relatedItemList[i].id.toString()
                        addtocart["variant_id"] = relatedItemList[i].defaultVariantId.toString()
                        addtocart["qty"] = "1"
                        addtocart["theme_id"] = getString(R.string.theme_id)
                        addtocartApi(
                            addtocart,
                            relatedItemList[i].id,
                            relatedItemList[i].defaultVariantId,
                            i
                        )
                    }
                } else if (s == Constants.ItemClick) {
                    val intent = Intent(this@ActProductDetails, ActProductDetails::class.java)
                    intent.putExtra(Constants.ProductId, relatedItemList[i].id.toString())
                    startActivity(intent)
                }
            }
        _binding.rvRelatedItems.adapter = relatedProductAdapter
    }


    private fun setupVariantAdapter(variantList: ArrayList<VariantItem>) {
        variantAdapter = VariantAdapter(variantList, this@ActProductDetails, variantName) {
            val selectedVariantList = ArrayList<String>()
            for (i in 0 until variantList.size) {
                for (j in 0 until variantList[i].dataItem?.size!!) {
                    if (variantList[i].dataItem!![j].isSelect == true) {
                        selectedVariantList.add(
                            variantList[i].dataItem!![j].name?.trim().toString()
                        )
                    }
                }
            }
            val filterValue =
                selectedVariantList.joinToString(separator = "-") { it.replace(" ", "") }
            callVariantApi(filterValue, true)
        }

        _binding.rvVariant.apply {
            layoutManager = LinearLayoutManager(this@ActProductDetails)
            adapter = variantAdapter
            isNestedScrollingEnabled = true
        }
    }

    private fun callFirstPositionVariantCheck(variantList: ArrayList<VariantItem>) {
        if (variantList.size != 0) {
            val selectedVariantList = ArrayList<String>()

            for (i in 0 until variantList.size) {
                if (variantList[i].value?.size != 0) {

                    if (variantName.isNotEmpty()) {
                        selectedVariantList.add(variantName.trim())
                        break

                    } else {
                        selectedVariantList.add(variantList[i].value?.get(0)?.trim() ?: "")
                        break

                    }

                }
            }

            val filterValue = selectedVariantList.joinToString(separator = "-") { it.trim() }
            callVariantApi(filterValue, false)
        }
    }


    private fun initClickListeners() {

        _binding.ivBack.setOnClickListener {
            finish()
        }
        _binding.clcart.setOnClickListener {
            startActivity(Intent(this@ActProductDetails, ActShoppingCart::class.java))

        }

        _binding.ivWishlist.setOnClickListener {
            if (!isWishList) {
                callWishlist(
                    "add", product_id, "ProductDetail", 0
                )

            } else {
                callWishlist(
                    "remove", product_id, "ProductDetail", 0
                )
            }


        }

        _binding.btnFeedback.setOnClickListener {
            openFeedbackDialog()
        }
        _binding.btnMore.setOnClickListener {
            val contactUs =
                SharePreference.getStringPref(this@ActProductDetails, SharePreference.returnPolicy)
                    .toString()
            val uri: Uri =
                Uri.parse(contactUs)
            val intent = Intent(Intent.ACTION_VIEW, uri)
            intent.addCategory(Intent.CATEGORY_BROWSABLE)

            startActivity(intent)
        }


        _binding.btnAddToCart.setOnClickListener {
            Log.e("Stock", productStock)

            if (productStock == "0") {
                if (Utils.isLogin(this@ActProductDetails)) {
                    val notifyUser = HashMap<String, String>()
                    notifyUser["user_id"] =
                        SharePreference.getStringPref(
                            this@ActProductDetails,
                            SharePreference.userId
                        )
                            .toString()
                    notifyUser["product_id"] = product_id.toString()
                    notifyUser["theme_id"] = resources.getString(R.string.theme_id)

                    notifyUser(notifyUser)
                } else {
                    val intent = Intent(this@ActProductDetails, ActWelCome::class.java)
                    intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
                    startActivity(intent)
                    finish()
                }
            } else {
                if (Utils.isLogin(this@ActProductDetails)) {
                    val addtocart = HashMap<String, String>()
                    addtocart["user_id"] =
                        SharePreference.getStringPref(
                            this@ActProductDetails,
                            SharePreference.userId
                        )
                            .toString()
                    addtocart["product_id"] = product_id.toString()
                    addtocart["variant_id"] = variant_id.toString()
                    addtocart["qty"] = "1"
                    addtocart["theme_id"] = resources.getString(R.string.theme_id)

                    addtocartApi(
                        addtocart,
                        product_id.toInt(),
                        variant_id.toInt(), 0
                    )
                } else {
                    guestUserAddToCart(
                        RelatedProductDataItem(),
                        product_id.toInt(),
                        false,
                        variant_id
                    )
                }
            }
        }
    }


    private fun callWishlist(type: String, id: String?, itemType: String, pos: Int) {
        Utils.showLoadingProgress(this@ActProductDetails)
        val wishlist = HashMap<String, String>()
        wishlist["user_id"] =
            SharePreference.getStringPref(this@ActProductDetails, SharePreference.userId).toString()
        wishlist["product_id"] = id.toString()
        wishlist["wishlist_type"] = type
        wishlist["theme_id"] = getString(R.string.theme_id)

        lifecycleScope.launch {
            when (val response = ApiClient.getClient(this@ActProductDetails)
                .setWishlist(wishlist)) {
                is NetworkResponse.Success -> {
                    Utils.dismissLoadingProgress()
                    val wishlistResponse = response.body.data
                    when (response.body.status) {
                        1 -> {


                            if (type == "add") {
                                when (itemType) {
                                    "RelatedItemProduct" -> {
                                        relatedProductItemList[pos].inWhishlist = true
                                        relatedProductAdapter.notifyItemChanged(pos)
                                    }
                                    else -> {
                                        isWishList = true
                                        _binding.ivWishlist.background =
                                            ResourcesCompat.getDrawable(
                                                resources,
                                                R.drawable.ic_like,
                                                null
                                            )
                                        _binding.ivWishlist.backgroundTintList =
                                            ColorStateList.valueOf(
                                                ResourcesCompat.getColor(
                                                    resources,
                                                    R.color.black,
                                                    null
                                                )
                                            )
                                    }
                                }


                            } else {

                                when (itemType) {
                                    "RelatedItemProduct" -> {
                                        relatedProductItemList[pos].inWhishlist = false
                                        relatedProductAdapter.notifyItemChanged(pos)
                                    }
                                    else -> {
                                        isWishList = false

                                        _binding.ivWishlist.background =
                                            ResourcesCompat.getDrawable(
                                                resources,
                                                R.drawable.ic_dislike,
                                                null
                                            )
                                        _binding.ivWishlist.backgroundTintList =
                                            ColorStateList.valueOf(
                                                ResourcesCompat.getColor(
                                                    resources,
                                                    R.color.black,
                                                    null
                                                )
                                            )
                                    }
                                }


                            }
                        }
                        0 -> {
                            Utils.errorAlert(
                                this@ActProductDetails,
                                wishlistResponse?.message.toString()
                            )
                        }
                        9 -> {
                            Utils.errorAlert(
                                this@ActProductDetails,
                                wishlistResponse?.message.toString()
                            )
                            startActivity(Intent(this@ActProductDetails, ActWelCome::class.java))
                            finish()
                            finishAffinity()
                        }
                    }
                }

                is NetworkResponse.ApiError -> {
                    Utils.dismissLoadingProgress()

                    if (response.body.status == 9) {
                        Utils.setInvalidToekn(this@ActProductDetails)
                    } else {
                        Utils.errorAlert(
                            this@ActProductDetails,
                            response.body.message.toString()
                        )
                    }
                }

                is NetworkResponse.NetworkError -> {
                    Utils.dismissLoadingProgress()
                    Utils.errorAlert(
                        this@ActProductDetails,
                        resources.getString(R.string.internet_connection_error)
                    )
                }

                is NetworkResponse.UnknownError -> {
                    Utils.dismissLoadingProgress()
                    Utils.errorAlert(
                        this@ActProductDetails,
                        "Something went wrong"
                    )
                }
            }
        }
    }


    private fun dlgAlreadyCart(product_id: String, i: Int, variant_id: String) {
        val builder = AlertDialog.Builder(this@ActProductDetails)
        builder.setTitle(R.string.app_name)
        builder.setMessage(R.string.product_already_again)
        builder.setPositiveButton(getString(R.string.yes)) { dialogInterface, which ->
            dialogInterface.dismiss()
            val cartqty = java.util.HashMap<String, String>()

            if (Utils.isLogin(this@ActProductDetails)) {
                cartqty["product_id"] = product_id
                cartqty["user_id"] =
                    SharePreference.getStringPref(
                        this@ActProductDetails,
                        SharePreference.userId
                    ).toString()
                cartqty["variant_id"] = variant_id
                cartqty["quantity_type"] = "increase"
                cartqty["theme_id"] = resources.getString(R.string.theme_id)
                cartQtyApi(cartqty, "increase")
            } else {
                val cartListData = SharePreference.getStringPref(
                    this@ActProductDetails,
                    SharePreference.GuestCartList
                )
                val type: Type = object : TypeToken<java.util.ArrayList<ProductListItem>>() {}.type

                val cartList =
                    Gson().fromJson<java.util.ArrayList<ProductListItem>>(cartListData, type)
                val count = cartList[i].qty?.toInt()!!
                Log.e("Count", count.toString())
                cartList[i].qty = count.plus(1)

                manageOfflineData(cartList)

            }
        }
        builder.setNegativeButton(getString(R.string.no)) { dialogInterface, which ->
            dialogInterface.dismiss()
            builder.setCancelable(true)
        }
        val alertDialog: AlertDialog = builder.create()
        alertDialog.setCancelable(false)
        alertDialog.show()
    }


    private fun cartQtyApi(
        cartqty: java.util.HashMap<String, String>,
        type: String
    ) {
        Utils.showLoadingProgress(this@ActProductDetails)
        lifecycleScope.launch {
            when (val response = ApiClient.getClient(this@ActProductDetails)
                .cartQty(cartqty)) {
                is NetworkResponse.Success -> {
                    Utils.dismissLoadingProgress()
                    val cartQtyResponse = response.body.data
                    SharePreference.setStringPref(
                        this@ActProductDetails,
                        SharePreference.cartCount,
                        response.body.count.toString()
                    )

                    when (response.body.status) {
                        1 -> {

                            Utils.dismissLoadingProgress()
                        }
                        0 -> {
                            Utils.errorAlert(
                                this@ActProductDetails,
                                cartQtyResponse?.message.toString()
                            )
                        }
                        9 -> {
                            Utils.errorAlert(
                                this@ActProductDetails,
                                cartQtyResponse?.message.toString()
                            )
                            startActivity(Intent(this@ActProductDetails, ActWelCome::class.java))
                        }
                    }
                }

                is NetworkResponse.ApiError -> {
                    Utils.dismissLoadingProgress()

                    if (response.body.status == 9) {
                        Utils.setInvalidToekn(this@ActProductDetails)
                    } else {
                        Utils.errorAlert(
                            this@ActProductDetails, response.body.message.toString()
                        )
                    }
                }

                is NetworkResponse.NetworkError -> {
                    Utils.dismissLoadingProgress()
                    Utils.errorAlert(
                        this@ActProductDetails,
                        resources.getString(R.string.internet_connection_error)
                    )
                }

                is NetworkResponse.UnknownError -> {
                    Utils.dismissLoadingProgress()
                    Utils.errorAlert(this@ActProductDetails, "Something went wrong")
                }
            }
        }
    }


    private fun manageOfflineData(cartItemList: java.util.ArrayList<ProductListItem>) {
        SharePreference.setStringPref(
            this@ActProductDetails,
            SharePreference.GuestCartList,
            Gson().toJson(cartItemList)
        )
        SharePreference.setStringPref(
            this@ActProductDetails,
            SharePreference.cartCount,
            cartItemList.size.toString()
        )
    }

    private fun dlgConfirm(message: String?) {
        val confirmDialogBinding = DlgConfirmBinding.inflate(layoutInflater)
        val dialog = Dialog(this@ActProductDetails)
        dialog.setContentView(confirmDialogBinding.root)
        confirmDialogBinding.tvProductDesc.text = message
        confirmDialogBinding.tvCheckOut.setOnClickListener {
            startActivity(Intent(this@ActProductDetails, ActShoppingCart::class.java))

        }
        confirmDialogBinding.tvContinueShopping.setOnClickListener {
            dialog.dismiss()
        }

        confirmDialogBinding.tvCancel.setOnClickListener {
            dialog.dismiss()
        }
        dialog.show()
    }

    private fun guestUserAddToCart(
        data: RelatedProductDataItem,
        id: Int?,
        fromRelatedItem: Boolean,
        variant_id: String
    ) {
        Log.e("Position", id.toString())
        val cartData = cartData(data, fromRelatedItem)
        Log.e("cartData2", Gson().toJson(cartData))

        val cartListData = SharePreference.getStringPref(
            this@ActProductDetails,
            SharePreference.GuestCartList
        )
        val type: Type = object : TypeToken<ArrayList<ProductListItem>>() {}.type

        var cartList =
            Gson().fromJson<ArrayList<ProductListItem>>(cartListData, type)
        Log.e("cartListBefore2", Gson().toJson(cartList))

        if (cartList == null) {
            dlgConfirm(data.name + " " + R.string.addsuccess)
            val cartDataList = ArrayList<ProductListItem>()
            cartDataList.add(cartData)

            SharePreference.setStringPref(
                this@ActProductDetails,
                SharePreference.GuestCartList,
                Gson().toJson(cartDataList)
            )
            SharePreference.setStringPref(
                this@ActProductDetails,
                SharePreference.cartCount,
                cartDataList.size.toString()
            )
            _binding.tvCount.text = cartDataList.size.toString()
        } else if (isProductAlreadyAdded(cartList, id.toString(), variant_id)) {
            for (i in 0 until cartList.size) {
                if (cartList[i].productId?.toInt() == id) {
                    val position = cartList[i]
                    Log.e("SelectId", position.toString())
                    dlgAlreadyCart(data.id.toString(), i, data.defaultVariantId.toString())
                }
            }
        } else {
            dlgConfirm(cartData.name.plus(" ").plus("add successfully."))
            cartList.add(cartData)
            SharePreference.setStringPref(
                this@ActProductDetails,
                SharePreference.GuestCartList,
                Gson().toJson(cartList)
            )
            SharePreference.setStringPref(
                this@ActProductDetails,
                SharePreference.cartCount,
                cartList.size.toString()
            )
            _binding.tvCount.text = cartList.size.toString()
        }
    }

    private fun isProductAlreadyAdded(
        cartList: ArrayList<ProductListItem>,
        id: String,
        variant_id: String
    ): Boolean {
        return cartList.filter {

            it.productId == id && it.variantId == variant_id
        }.size == 1
    }

    private fun cartData(): ProductListItem {
        return ProductListItem(
            productInfo.coverImagePath.toString(),
            variant_id,
            finalPrice,
            disCountPrice,
            productInfo.id.toString(),
            1,
            productInfo.name.toString(),
            "",
            "",
            variantName,
            originalPrice
        )
    }

    private fun cartData(data: RelatedProductDataItem, fromRelatedItem: Boolean): ProductListItem {

        return if (fromRelatedItem) {
            ProductListItem(
                data.coverImagePath.toString(),
                data.defaultVariantId.toString(),
                data.finalPrice.toString(),
                data.discountPrice.toString(),
                data.id.toString(),
                1,
                data.name.toString(),
                "",
                "",
                "",
                data.originalPrice.toString()
            )
        } else {
            ProductListItem(
                productInfo.coverImagePath.toString(),
                variant_id,
                finalPrice,
                disCountPrice,
                productInfo.id.toString(),
                1,
                productInfo.name.toString(),
                "",
                "",
                variantName,
                originalPrice
            )
        }
    }


    private fun addtocartApi(
        addtocart: HashMap<String, String>,
        id: Int?,
        defaultVariantId: Int?,
        i: Int?
    ) {
        Utils.showLoadingProgress(this@ActProductDetails)
        lifecycleScope.launch {
            when (val response = ApiClient.getClient(this@ActProductDetails)
                .addtocart(addtocart)) {
                is NetworkResponse.Success -> {
                    Utils.dismissLoadingProgress()
                    val addtocart = response.body.data
                    when (response.body.status) {
                        1 -> {
                            dlgConfirm(response.body.data?.message)
                            SharePreference.setStringPref(
                                this@ActProductDetails,
                                SharePreference.cartCount,
                                addtocart?.count.toString()
                            )
                            setCount(addtocart?.count)
                        }
                        0 -> {
                            dlgAlreadyCart(id.toString(), i!!, defaultVariantId.toString())
                        }
                        9 -> {
                            Utils.errorAlert(
                                this@ActProductDetails,
                                addtocart?.message.toString()
                            )
                            startActivity(Intent(this@ActProductDetails, ActWelCome::class.java))
                            finish()
                            finishAffinity()
                        }

                    }
                }

                is NetworkResponse.ApiError -> {
                    Utils.dismissLoadingProgress()

                    if (response.body.status == 9) {
                        Utils.setInvalidToekn(this@ActProductDetails)
                    } else {
                        Utils.errorAlert(
                            this@ActProductDetails,
                            response.body.message.toString()
                        )
                    }
                }

                is NetworkResponse.NetworkError -> {
                    Utils.dismissLoadingProgress()
                    Utils.errorAlert(
                        this@ActProductDetails,
                        resources.getString(R.string.internet_connection_error)
                    )
                }

                is NetworkResponse.UnknownError -> {
                    Utils.dismissLoadingProgress()
                    Utils.errorAlert(
                        this@ActProductDetails,
                        "Something went wrong"
                    )
                }
            }
        }
    }

    private fun setCount(count: Int?) {
        _binding.tvCount.text = count.toString()
    }

    private fun notifyUser(notifyUser: HashMap<String, String>) {
        Utils.showLoadingProgress(this@ActProductDetails)
        lifecycleScope.launch {
            when (val response = ApiClient.getClient(this@ActProductDetails)
                .notifyUser(notifyUser)) {
                is NetworkResponse.Success -> {
                    Utils.dismissLoadingProgress()
                    val addtocart = response.body.data
                    when (response.body.status) {
                        1 -> {
                            //Utils.successAlert(this@ActProductDetails,addtocart?.point.toString())
                        }
                        0 -> {
                            Utils.errorAlert(
                                this@ActProductDetails,
                                addtocart?.message.toString()
                            )
                        }
                        9 -> {
                            Utils.errorAlert(
                                this@ActProductDetails,
                                addtocart?.message.toString()
                            )
                            startActivity(Intent(this@ActProductDetails, ActWelCome::class.java))
                            finish()
                            finishAffinity()
                        }

                    }
                }

                is NetworkResponse.ApiError -> {
                    Utils.dismissLoadingProgress()

                    if (response.body.status == 9) {
                        Utils.setInvalidToekn(this@ActProductDetails)
                    } else {
                        Utils.errorAlert(
                            this@ActProductDetails,
                            response.body.message.toString()
                        )
                    }
                }

                is NetworkResponse.NetworkError -> {
                    Utils.dismissLoadingProgress()
                    Utils.errorAlert(
                        this@ActProductDetails,
                        resources.getString(R.string.internet_connection_error)
                    )
                }

                is NetworkResponse.UnknownError -> {
                    Utils.dismissLoadingProgress()
                    Utils.errorAlert(
                        this@ActProductDetails,
                        "Something went wrong"
                    )
                }
            }
        }

    }


    private fun adapterSetup(imageList: ArrayList<ProductImageItem>) {
        adapter = ViewPagerAdapter(imageList, this@ActProductDetails)
        _binding.viewPager.adapter = adapter
        _binding.viewPager.addOnPageChangeListener(object : ViewPager.OnPageChangeListener {
            override fun onPageScrolled(i: Int, v: Float, i1: Int) {}
            override fun onPageSelected(i: Int) {
                setCurrentIndicator(i)
            }

            override fun onPageScrollStateChanged(i: Int) {}
        })
        setupIndicator()
    }


    private fun setupDetailData(data: ProductDetailData) {
        _binding.tvProductName.text = data.productInfo?.name
      /*  if (data.productInfo?.tagApi == "None") {
            _binding.tvTagName.hide()
        } else {
            _binding.tvTagName.show()
        }*/
        _binding.tvTagName.text = data.productInfo?.tagApi

        _binding.tvProductPrice.text = Utils.getPrice(data.productInfo?.finalPrice.toString())
        _binding.tvDiscountPrice.text = Utils.getPrice(data.productInfo?.originalPrice.toString())
        _binding.tvCurrency.text =
            SharePreference.getStringPref(this@ActProductDetails, SharePreference.currency_name)
                .toString()

        val variantList = ArrayList<VariantItem>()
        data.variant?.let { variantList.addAll(it) }
        isWishList = data.productInfo?.inWhishlist ?: false

        if (data.productInfo?.inWhishlist == true) {
            _binding.ivWishlist.background =
                ResourcesCompat.getDrawable(resources, R.drawable.ic_like, null)
            _binding.ivWishlist.backgroundTintList =
                ColorStateList.valueOf(ResourcesCompat.getColor(resources, R.color.black, null))

        } else {
            _binding.ivWishlist.background =
                ResourcesCompat.getDrawable(resources, R.drawable.ic_dislike, null)
            _binding.ivWishlist.backgroundTintList =
                ColorStateList.valueOf(ResourcesCompat.getColor(resources, R.color.black, null))

        }

        setupVariantAdapter(variantList)
        Log.e("gson", Gson().toJson(variantList))
        callFirstPositionVariantCheck(variantList)

        val imageList = ArrayList<ProductImageItem>()
        data.productImage?.let { imageList.addAll(it) }
        if (imageList.size == 0) {
            _binding.viewPager.hide()
            _binding.rvGallery.hide()
            _binding.tvGalleryTitle.hide()

        } else {
            _binding.viewPager.show()
            _binding.rvGallery.show()
            _binding.tvGalleryTitle.show()

            adapterSetup(imageList)
            setupGalleryAdapter(imageList)
            setCurrentIndicator(0)
        }



        _binding.ivRatting.rating = data.productInfo?.averageRating?.toFloat() ?: 0.0f
        _binding.tvAvgRatting.text = data.productInfo?.averageRating?.toFloat().toString()
        /*if(data.productInfo?.averageRating==0)
        {
            _binding.tvAvgRatting.hide()
            _binding.tvTotalRating.hide()
        }*/
        if (data.productReview?.size == 0) {
            _binding.rvReview.hide()
        } else {
            _binding.rvReview.show()
            val snapHelper: SnapHelper = PagerSnapHelper()
            _binding.rvReview.layoutManager =
                GridLayoutManager(this@ActProductDetails, 1, GridLayoutManager.HORIZONTAL, false)
            val adapter =
                data.productReview?.let {
                    RattingAdapter(this@ActProductDetails, it) { id: String, name: String ->
                    }
                }
            _binding.rvReview.adapter = adapter
            snapHelper.attachToRecyclerView(_binding.rvReview);

        }

        val otherDescriptionArray = data.productInfo?.otherDescriptionArray
        otherDescriptionArray?.removeAll { it.description?.isEmpty() == true }
        if (data.productInfo?.otherDescriptionArray?.size == 0) {
            _binding.rvDescription.hide()
        } else {
            _binding.rvDescription.show()

            _binding.rvDescription.apply {
                layoutManager = LinearLayoutManager(this@ActProductDetails)
                adapter = otherDescriptionArray?.let { DescriptionAdapter(it) }
            }
        }
    }

    private fun setupGalleryAdapter(imageList: ArrayList<ProductImageItem>) {


        _binding.rvGallery.apply {
            layoutManager =
                LinearLayoutManager(this@ActProductDetails, RecyclerView.HORIZONTAL, false)
            adapter = GalleryAdapter(imageList)
            isNestedScrollingEnabled = true
        }
    }

    private fun callVariantApi(variant_sku: String, fromClick: Boolean) {
        if (fromClick) {
            Utils.showLoadingProgress(this@ActProductDetails)

        }
        val productDetailRequest = HashMap<String, String>()
        productDetailRequest["product_id"] = intent.getStringExtra(Constants.ProductId) ?: ""
        productDetailRequest["variant_sku"] = variant_sku
        productDetailRequest["theme_id"] = resources.getString(R.string.theme_id)

        lifecycleScope.launch {
            when (val response =
                ApiClient.getClient(this@ActProductDetails).variantStock(productDetailRequest)) {
                is NetworkResponse.Success -> {
                    Utils.dismissLoadingProgress()
                    if (response.body.status == 1) {
                        val data = response.body.data
                        _binding.tvProductPrice.text = Utils.getPrice(data?.finalPrice.toString())
                        _binding.tvCurrency.text = SharePreference.getStringPref(
                            this@ActProductDetails,
                            SharePreference.currency_name
                        ).toString()
                        _binding.tvDiscountPrice.text =
                            Utils.getPrice(data?.originalPrice.toString())
                        variant_id = data?.id.toString()
                        disCountPrice = data?.discountPrice.toString()
                        originalPrice = data?.originalPrice.toString()
                        finalPrice = data?.finalPrice.toString()
                        variantName = data?.variant.toString()
                        productStock = data?.stock.toString()

                        if (data?.stock == 0) {

                            Utils.errorAlert(
                                this@ActProductDetails,
                                resources.getString(R.string.out_of_stock)
                            )
                            _binding.tvAddToCart.text = getString(R.string.notify_me)
                            _binding.tvOutStock.show()

                        } else {

                            _binding.tvOutStock.hide()
                            _binding.tvAddToCart.text = getString(R.string.add_to_cart)

                        }
                    }

                }
                is NetworkResponse.ApiError -> {
                    Utils.dismissLoadingProgress()

                    if (response.body.status == 9) {
                        Utils.setInvalidToekn(this@ActProductDetails)
                    } else {
                        Utils.errorAlert(
                            this@ActProductDetails,
                            response.body.message.toString()
                        )
                    }
                }

                is NetworkResponse.NetworkError -> {
                    Utils.dismissLoadingProgress()
                    Utils.errorAlert(
                        this@ActProductDetails,
                        resources.getString(R.string.internet_connection_error)
                    )
                }

                is NetworkResponse.UnknownError -> {
                    Utils.dismissLoadingProgress()
                    Utils.errorAlert(
                        this@ActProductDetails,
                        "Something went wrong"
                    )
                }
            }
        }
    }


    private fun callProductDetailApi() {
        Utils.showLoadingProgress(this@ActProductDetails)

        lifecycleScope.launch {
            val productDetailRequest = HashMap<String, String>()
            productDetailRequest["id"] = intent.getStringExtra(Constants.ProductId) ?: ""
            productDetailRequest["theme_id"] = resources.getString(R.string.theme_id)
            val response =
                if (SharePreference.getStringPref(this@ActProductDetails, SharePreference.token)
                        ?.isEmpty() == true
                ) {
                    ApiClient.getClient(this@ActProductDetails)
                        .productDetailGuest(productDetailRequest)
                } else {
                    ApiClient.getClient(this@ActProductDetails).productDetail(productDetailRequest)
                }

            when (response) {
                is NetworkResponse.Success -> {
                    Utils.dismissLoadingProgress()
                    if (response.body.status == 1) {

                        response.body.data?.let { setupDetailData(it) }
                        productInfo = response.body.data?.productInfo!!
                        product_id = response.body.data.productInfo.id.toString()
                        variant_id = response.body.data.productInfo.defaultVariantId.toString()
                        variantName = response.body.data.productInfo.defaultVariantName.toString()
                        disCountPrice = response.body.data?.productInfo?.discountPrice.toString()
                        originalPrice = response.body.data?.productInfo?.originalPrice.toString()
                        finalPrice = response.body.data?.productInfo?.finalPrice.toString()
                        _binding.tvReturn.text =
                            response.body.data?.productInfo?.retuen_text.toString()
                        if (response.body.data?.productInfo?.is_review == 0) {
                            if (Utils.isLogin(this@ActProductDetails)) {
                                _binding.btnFeedback.show()
                            } else {
                                _binding.btnFeedback.hide()
                            }
                        } else {
                            _binding.btnFeedback.hide()
                        }
                        productStock = response.body.data.productInfo.productStock.toString()
                        Log.e("productStok", productStock.toString())

                        if (response.body.data.variant?.size == 0) {
                            if (response.body.data.productInfo.productStock == 0) {

                                _binding.tvAddToCart.text = getString(R.string.notify_me)
                                _binding.tvOutStock.show()
                            } else {
                                _binding.tvOutStock.hide()
                                _binding.tvAddToCart.text = getString(R.string.add_to_cart)
                            }
                        }


                        _binding.vieww.hide()

                    } else {

                        _binding.vieww.show()

                    }


                }
                is NetworkResponse.ApiError -> {
                    Utils.dismissLoadingProgress()
                    if (response.body.status == 9) {
                        Utils.setInvalidToekn(this@ActProductDetails)
                    } else {
                        Utils.errorAlert(
                            this@ActProductDetails,
                            response.body.message.toString()
                        )
                    }
                }

                is NetworkResponse.NetworkError -> {
                    Utils.dismissLoadingProgress()
                    Utils.errorAlert(
                        this@ActProductDetails,
                        resources.getString(R.string.internet_connection_error)
                    )
                }

                is NetworkResponse.UnknownError -> {
                    Utils.dismissLoadingProgress()
                    Utils.errorAlert(
                        this@ActProductDetails,
                        "Something went wrong"
                    )
                }
            }
        }
    }

    private fun openFeedbackDialog() {
        var dialog: Dialog? = null
        try {
            dialog?.dismiss()
            dialog = Dialog(this@ActProductDetails, R.style.AppCompatAlertDialogStyleBig)
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
            dialog.window!!.setLayout(
                WindowManager.LayoutParams.MATCH_PARENT,
                WindowManager.LayoutParams.MATCH_PARENT
            )
            dialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
            dialog.setCancelable(false)
            val mInflater = LayoutInflater.from(this@ActProductDetails)
            val mView = mInflater.inflate(R.layout.dlg_feedback, null, false)
            val btnCancel: AppCompatButton = mView.findViewById(R.id.btnCancel)
            val btnRateNow: AppCompatButton = mView.findViewById(R.id.btnRateNow)
            val ivStar1: ImageView = mView.findViewById(R.id.ivStar1)
            val ivStar2: ImageView = mView.findViewById(R.id.ivStar2)
            val ivStar3: ImageView = mView.findViewById(R.id.ivStar3)
            val ivStar4: ImageView = mView.findViewById(R.id.ivStar4)
            val ivStar5: ImageView = mView.findViewById(R.id.ivStar5)
            val title: EditText = mView.findViewById(R.id.edTitle)
            val edNote: AppCompatEditText = mView.findViewById(R.id.edNote)
            ivStar1.imageTintList =
                AppCompatResources.getColorStateList(this@ActProductDetails, R.color.gray)
            ivStar2.imageTintList =
                AppCompatResources.getColorStateList(this@ActProductDetails, R.color.gray)
            ivStar3.imageTintList =
                AppCompatResources.getColorStateList(this@ActProductDetails, R.color.gray)
            ivStar4.imageTintList =
                AppCompatResources.getColorStateList(this@ActProductDetails, R.color.gray)
            ivStar5.imageTintList =
                AppCompatResources.getColorStateList(this@ActProductDetails, R.color.gray)
            ivStar1.setOnClickListener {
                ivStar1.imageTintList =
                    AppCompatResources.getColorStateList(this@ActProductDetails, R.color.appcolor)
                ivStar2.imageTintList =
                    AppCompatResources.getColorStateList(this@ActProductDetails, R.color.gray)
                ivStar3.imageTintList =
                    AppCompatResources.getColorStateList(this@ActProductDetails, R.color.gray)
                ivStar4.imageTintList =
                    AppCompatResources.getColorStateList(this@ActProductDetails, R.color.gray)
                ivStar5.imageTintList =
                    AppCompatResources.getColorStateList(this@ActProductDetails, R.color.gray)
                rattingValue = "1"
            }
            ivStar2.setOnClickListener {
                ivStar1.imageTintList =
                    AppCompatResources.getColorStateList(this@ActProductDetails, R.color.appcolor)
                ivStar2.imageTintList =
                    AppCompatResources.getColorStateList(this@ActProductDetails, R.color.appcolor)
                ivStar3.imageTintList =
                    AppCompatResources.getColorStateList(this@ActProductDetails, R.color.gray)
                ivStar4.imageTintList =
                    AppCompatResources.getColorStateList(this@ActProductDetails, R.color.gray)
                ivStar5.imageTintList =
                    AppCompatResources.getColorStateList(this@ActProductDetails, R.color.gray)
                rattingValue = "2"
            }
            ivStar3.setOnClickListener {
                ivStar1.imageTintList =
                    AppCompatResources.getColorStateList(this@ActProductDetails, R.color.appcolor)
                ivStar2.imageTintList =
                    AppCompatResources.getColorStateList(this@ActProductDetails, R.color.appcolor)
                ivStar3.imageTintList =
                    AppCompatResources.getColorStateList(this@ActProductDetails, R.color.appcolor)
                ivStar4.imageTintList =
                    AppCompatResources.getColorStateList(this@ActProductDetails, R.color.gray)
                ivStar5.imageTintList =
                    AppCompatResources.getColorStateList(this@ActProductDetails, R.color.gray)
                rattingValue = "3"
            }
            ivStar4.setOnClickListener {
                ivStar1.imageTintList =
                    AppCompatResources.getColorStateList(this@ActProductDetails, R.color.appcolor)
                ivStar2.imageTintList =
                    AppCompatResources.getColorStateList(this@ActProductDetails, R.color.appcolor)
                ivStar3.imageTintList =
                    AppCompatResources.getColorStateList(this@ActProductDetails, R.color.appcolor)
                ivStar4.imageTintList =
                    AppCompatResources.getColorStateList(this@ActProductDetails, R.color.appcolor)
                ivStar5.imageTintList =
                    AppCompatResources.getColorStateList(this@ActProductDetails, R.color.gray)
                rattingValue = "4"
            }
            ivStar5.setOnClickListener {
                ivStar1.imageTintList =
                    AppCompatResources.getColorStateList(this@ActProductDetails, R.color.appcolor)
                ivStar2.imageTintList =
                    AppCompatResources.getColorStateList(this@ActProductDetails, R.color.appcolor)
                ivStar3.imageTintList =
                    AppCompatResources.getColorStateList(this@ActProductDetails, R.color.appcolor)
                ivStar4.imageTintList =
                    AppCompatResources.getColorStateList(this@ActProductDetails, R.color.appcolor)
                ivStar5.imageTintList =
                    AppCompatResources.getColorStateList(this@ActProductDetails, R.color.appcolor)
                rattingValue = "5"
            }
            val finalDialog: Dialog = dialog
            btnRateNow.setOnClickListener {
                when {
                    rattingValue == "0" -> {
                        Utils.errorAlert(
                            this@ActProductDetails,
                            resources.getString(R.string.validation_alert)
                        )
                    }
                    edNote.text?.isEmpty() == true -> {
                        Utils.errorAlert(
                            this@ActProductDetails,
                            resources.getString(R.string.validation_alert)
                        )
                    }
                    title.text?.isEmpty() == true -> {
                        Utils.errorAlert(
                            this@ActProductDetails,
                            resources.getString(R.string.validation_alert)
                        )
                    }
                    else -> {
                        val ratting = HashMap<String, String>()
                        ratting["theme_id"] = getString(R.string.theme_id)
                        ratting["id"] = intent.getStringExtra(Constants.ProductId) ?: ""
                        ratting["user_id"] = SharePreference.getStringPref(
                            this@ActProductDetails,
                            SharePreference.userId
                        ).toString()
                        ratting["rating_no"] = rattingValue
                        ratting["title"] = title.text.toString()
                        ratting["description"] = edNote.text.toString()
                        callProductRatting(ratting)
                        finalDialog.dismiss()
                    }
                }
            }
            btnCancel.setOnClickListener {
                finalDialog.dismiss()
            }
            dialog.setContentView(mView)
            dialog.show()
        } catch (e: java.lang.Exception) {
            e.printStackTrace()
        }
    }

    private fun callProductRatting(ratting: HashMap<String, String>) {
        Utils.showLoadingProgress(this@ActProductDetails)
        lifecycleScope.launch {
            when (val response = ApiClient.getClient(this@ActProductDetails)
                .productRating(ratting)) {
                is NetworkResponse.Success -> {
                    Utils.dismissLoadingProgress()
                    val rattingResponse = response.body.data
                    when (response.body.status) {
                        1 -> {
                            callProductDetailApi()
                            Utils.successAlert(
                                this@ActProductDetails,
                                rattingResponse?.message.toString()
                            )
                        }
                        0 -> {
                            Utils.errorAlert(
                                this@ActProductDetails,
                                rattingResponse?.message.toString()
                            )
                        }
                        9 -> {
                            Utils.errorAlert(
                                this@ActProductDetails,
                                response.body.message.toString()
                            )

                            startActivity(Intent(this@ActProductDetails, ActWelCome::class.java))
                        }
                    }
                }

                is NetworkResponse.ApiError -> {
                    Utils.dismissLoadingProgress()
                    if (response.body.status == 9) {
                        Utils.setInvalidToekn(this@ActProductDetails)
                    } else {
                        Utils.errorAlert(
                            this@ActProductDetails,
                            response.body.message.toString()
                        )
                    }
                }

                is NetworkResponse.NetworkError -> {
                    Utils.dismissLoadingProgress()
                    Utils.errorAlert(
                        this@ActProductDetails,
                        resources.getString(R.string.internet_connection_error)
                    )
                }

                is NetworkResponse.UnknownError -> {
                    Utils.dismissLoadingProgress()
                    Utils.errorAlert(
                        this@ActProductDetails,
                        "Something went wrong"
                    )
                }
            }
        }
    }


    private fun setupIndicator() {
        _binding.indicatorContainer.removeAllViews()
        val indicators = arrayOfNulls<ImageView>(adapter.count)
        val layoutParams: LinearLayout.LayoutParams =
            LinearLayout.LayoutParams(
                ViewGroup.LayoutParams.WRAP_CONTENT,
                ViewGroup.LayoutParams.WRAP_CONTENT
            )
        layoutParams.setMargins(8, 0, 8, 0)
        for (i in indicators.indices) {
            indicators[i] = ImageView(applicationContext)
            indicators[i]?.apply {
                this.setImageDrawable(
                    ContextCompat.getDrawable(
                        this@ActProductDetails,
                        R.drawable.tab_indicator_default
                    )
                )
                this.layoutParams = layoutParams
            }
            _binding.indicatorContainer.addView(indicators[i])
        }
    }


    private fun setCurrentIndicator(index: Int) {
        val childCount = _binding.indicatorContainer.childCount
        for (i in 0 until childCount) {
            val imageview = _binding.indicatorContainer[i] as ImageView
            if (i == index) {
                imageview.setImageDrawable(
                    ContextCompat.getDrawable(
                        this@ActProductDetails,
                        R.drawable.tab_indicator_selected
                    )
                )
            } else {
                imageview.setImageDrawable(
                    ContextCompat.getDrawable(
                        this@ActProductDetails,
                        R.drawable.tab_indicator_default
                    )
                )

            }
        }
    }
}