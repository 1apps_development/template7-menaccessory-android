package com.workdo.manaccessory.ui.activity

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import androidx.lifecycle.lifecycleScope
import androidx.recyclerview.widget.GridLayoutManager
import com.workdo.manaccessory.R
import com.workdo.manaccessory.adapter.DeliveryAdapter
import com.workdo.manaccessory.api.ApiClient
import com.workdo.manaccessory.base.BaseActivity
import com.workdo.manaccessory.databinding.ActDeliveryBinding
import com.workdo.manaccessory.model.DeliveryData
import com.workdo.manaccessory.remote.NetworkResponse
import com.workdo.manaccessory.ui.authentication.ActWelCome
import com.workdo.manaccessory.utils.ExtensionFunctions.hide
import com.workdo.manaccessory.utils.ExtensionFunctions.show
import com.workdo.manaccessory.utils.SharePreference
import com.workdo.manaccessory.utils.Utils
import kotlinx.coroutines.launch

class ActDelivery : BaseActivity() {
    private lateinit var _binding: ActDeliveryBinding
    private var deliveryList = ArrayList<DeliveryData>()
    private lateinit var deliveryAdapter: DeliveryAdapter
    private var manager: GridLayoutManager? = null
    var comment = ""

    override fun setLayout(): View = _binding.root

    override fun initView() {
        _binding= ActDeliveryBinding.inflate(layoutInflater)
        init()
    }

    private fun init() {
        _binding.ivBack.setOnClickListener { finish() }
        _binding.btnContinue.setOnClickListener {
            comment =_binding.edDescription.text.toString()
            SharePreference.setStringPref(this@ActDelivery,SharePreference.Delivery_Comment,comment.toString())
            openActivity(ActPayment::class.java)
        }
        manager = GridLayoutManager(this@ActDelivery, 1, GridLayoutManager.VERTICAL, false)
    }

    private fun callDeliveryList() {
        Utils.showLoadingProgress(this@ActDelivery)
        val hashmap = HashMap<String,String>()
        hashmap["theme_id"] = getString(R.string.theme_id)

        lifecycleScope.launch {
            when (val response = ApiClient.getClient(this@ActDelivery)
                .deliveryList(hashmap)) {
                is NetworkResponse.Success -> {
                    Utils.dismissLoadingProgress()
                    val paymentListResponse = response.body
                    when (response.body.status) {
                        1 -> {
                            if ((response.body.data?.size ?: 0) > 0) {
                                _binding.rvDelivery.show()
                                _binding.vieww.hide()
                                paymentListResponse.data?.let {
                                    deliveryList.addAll(it)
                                }
                            } else {
                                _binding.rvDelivery.hide()
                                _binding.vieww.show()
                            }
                            deliveryAdapter.notifyDataSetChanged()
                        }
                        0 -> {
                            Utils.errorAlert(
                                this@ActDelivery,
                                paymentListResponse.message.toString()
                            )
                        }
                        9 -> {
                            Utils.errorAlert(
                                this@ActDelivery,
                                response.body.message.toString()
                            )
                            openActivity(ActWelCome::class.java)
                        }
                    }
                }

                is NetworkResponse.ApiError -> {
                    Utils.dismissLoadingProgress()

                    if (response.body.status == 9) {
                        Utils.setInvalidToekn(this@ActDelivery)
                    } else {
                        Utils.errorAlert(
                            this@ActDelivery,
                            response.body.message.toString()
                        )
                    }
                }

                is NetworkResponse.NetworkError -> {
                    Utils.dismissLoadingProgress()
                    Utils.errorAlert(
                        this@ActDelivery,
                        resources.getString(R.string.internet_connection_error)
                    )
                }

                is NetworkResponse.UnknownError -> {
                    Utils.dismissLoadingProgress()
                    Utils.errorAlert(
                        this@ActDelivery,
                        "Something went wrong"
                    )
                }
            }
        }
    }

    private fun DeliveryListAdapter(deliveryList: ArrayList<DeliveryData>) {
        _binding.rvDelivery.layoutManager = manager
        deliveryAdapter =
            DeliveryAdapter(this@ActDelivery, deliveryList) { i: Int, s: String ->

                if (deliveryList[i].isSelect == true) {
                    SharePreference.setStringPref(
                        this@ActDelivery,
                        SharePreference.Delivery_Id,
                        deliveryList[i].id.toString()
                    )

                    SharePreference.setStringPref(
                        this@ActDelivery,
                        SharePreference.DeliveryImage,
                        deliveryList[i].imagePath.toString()
                    )
                }
            }
        _binding.rvDelivery.adapter = deliveryAdapter
    }

    override fun onResume() {
        super.onResume()
        deliveryList.clear()
        DeliveryListAdapter(deliveryList)
        callDeliveryList()
    }


}