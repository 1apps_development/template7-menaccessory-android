package com.workdo.manaccessory.ui.fragment

import android.annotation.SuppressLint
import android.app.AlertDialog
import android.app.Dialog
import android.content.Intent
import android.view.View
import androidx.lifecycle.lifecycleScope
import androidx.recyclerview.widget.LinearLayoutManager
import com.workdo.manaccessory.R
import com.workdo.manaccessory.adapter.WishlistsAdapter
import com.workdo.manaccessory.api.ApiClient
import com.workdo.manaccessory.base.BaseFragment
import com.workdo.manaccessory.databinding.DlgConfirmBinding
import com.workdo.manaccessory.databinding.FragWishlistBinding
import com.workdo.manaccessory.model.WishListDataItem
import com.workdo.manaccessory.remote.NetworkResponse
import com.workdo.manaccessory.ui.activity.ActProductDetails
import com.workdo.manaccessory.ui.activity.ActShoppingCart
import com.workdo.manaccessory.ui.authentication.ActWelCome
import com.workdo.manaccessory.utils.Constants
import com.workdo.manaccessory.utils.ExtensionFunctions.hide
import com.workdo.manaccessory.utils.ExtensionFunctions.show
import com.workdo.manaccessory.utils.PaginationScrollListener
import com.workdo.manaccessory.utils.SharePreference
import com.workdo.manaccessory.utils.Utils
import kotlinx.coroutines.launch


class FragWishList : BaseFragment<FragWishlistBinding>() {
    private lateinit var _binding:FragWishlistBinding
    internal var isLoading = false
    internal var isLastPage = false

    private var currentPage = 1
    private var total_pages: Int = 0
    var count = 1
    private var wishList = ArrayList<WishListDataItem>()
    private lateinit var wishlistAdapter: WishlistsAdapter
    private var manager: LinearLayoutManager? = null


    override fun initView(view: View) {
        init()
    }

    override fun getBinding(): FragWishlistBinding {
        _binding = FragWishlistBinding.inflate(layoutInflater)
        return _binding
    }

    @SuppressLint("NotifyDataSetChanged")
    private fun init() {
        manager = LinearLayoutManager(requireActivity())
        pagination()
        _binding.tvCount.text =
            SharePreference.getStringPref(requireActivity(), SharePreference.cartCount).toString()
        _binding.clcart.setOnClickListener { openActivity(ActShoppingCart::class.java) }
    }


    private fun pagination() {
        val paginationListener = object : PaginationScrollListener(manager) {
            override fun isLastPage(): Boolean {
                return isLastPage
            }

            override fun isLoading(): Boolean {
                return isLoading
            }

            override fun loadMoreItems() {
                isLoading=true
                currentPage++
                callWishList()
            }
        }
        _binding.rvWishlist.addOnScrollListener(paginationListener)
    }

    private fun wishListAdapter(wishList: ArrayList<WishListDataItem>) {
        _binding.rvWishlist.layoutManager = manager
        wishlistAdapter =
            WishlistsAdapter(requireActivity(), wishList) { i: Int, s: String ->
                when (s) {
                    Constants.ItemDelete -> {
                        val wishlistRemove = HashMap<String, String>()
                        wishlistRemove["user_id"] =
                            SharePreference.getStringPref(requireActivity(), SharePreference.userId).toString()
                        wishlistRemove["product_id"] = wishList[i].productId.toString()
                        wishlistRemove["wishlist_type"] = "remove"
                        wishlistRemove["theme_id"] = getString(R.string.theme_id)

                        callremoveWishListApi(wishlistRemove, i)

                    }
                    Constants.CartClick -> {
                        val addtocart = HashMap<String, String>()
                        addtocart["user_id"] = SharePreference.getStringPref(requireActivity(), SharePreference.userId).toString()
                        addtocart["product_id"] = wishList[i].productId.toString()
                        addtocart["variant_id"] = wishList[i].variantId.toString()
                        addtocart["qty"] = "1"
                        addtocart["theme_id"]=getString(R.string.theme_id)
                        addtocartApi(addtocart,i,wishList[i].variantId)
                    }
                    Constants.ItemClick -> {
                        val intent = Intent(requireActivity(), ActProductDetails::class.java)
                        intent.putExtra(Constants.ProductId, wishList[i].productId.toString())
                        startActivity(intent)
                    }
                }
            }
        _binding.rvWishlist.adapter = wishlistAdapter
    }

    private fun callremoveWishListApi(wishlistRemove: HashMap<String, String>, i: Int) {

        lifecycleScope.launch {
            when (val response = ApiClient.getClient(requireActivity()).setWishlist(wishlistRemove)) {
                is NetworkResponse.Success -> {
                    Utils.dismissLoadingProgress()
                    val wishlistResponse = response.body.data
                    when (response.body.status) {
                        1 -> {
                            currentPage = 1
                            isLastPage = false
                            isLoading = false
                            wishList.clear()
                            callWishList()
                        }
                        0 -> {
                            Utils.errorAlert(requireActivity(), wishlistResponse?.message.toString())
                        }
                        9 -> {
                            Utils.errorAlert(requireActivity(), wishlistResponse?.message.toString())
                            openActivity(ActWelCome::class.java)
                        }
                    }
                }

                is NetworkResponse.ApiError -> {
                    Utils.dismissLoadingProgress()
                    if (response.body.status == 9) {
                        Utils.setInvalidToekn(requireActivity())
                    } else {
                        Utils.errorAlert(
                            requireActivity(),
                            response.body.message.toString()
                        )
                    }
                }

                is NetworkResponse.NetworkError -> {
                    Utils.dismissLoadingProgress()
                    Utils.errorAlert(
                        requireActivity(),
                        resources.getString(R.string.internet_connection_error)
                    )
                }

                is NetworkResponse.UnknownError -> {
                    Utils.dismissLoadingProgress()
                    Utils.errorAlert(
                        requireActivity(),
                        "Something went wrong"
                    )
                }
            }
        }
    }


    private fun callWishList() {
        Utils.showLoadingProgress(requireActivity())
        val categoriesProduct = HashMap<String, String>()
        categoriesProduct["user_id"] = SharePreference.getStringPref(requireActivity(), SharePreference.userId).toString()
        categoriesProduct["theme_id"] = getString(R.string.theme_id)

        lifecycleScope.launch {
            when (val response = ApiClient.getClient(requireActivity())
                .getwishliast(currentPage.toString(), categoriesProduct)) {
                is NetworkResponse.Success -> {
                    Utils.dismissLoadingProgress()
                    val addressListResponse = response.body.data
                    when (response.body.status) {
                        1 -> {
                            if ((response.body.data?.data?.size ?: 0) > 0) {
                                _binding.rvWishlist.show()
                                _binding.tvNoDataFound.hide()
                                currentPage = addressListResponse?.currentPage!!.toInt()
                               total_pages = addressListResponse.lastPage!!.toInt()
                                addressListResponse.data?.let {
                                    wishList.addAll(it)
                                }
                                if (currentPage >= total_pages) {
                                    isLastPage = true
                                }
                                isLoading = false
                            } else {
                                _binding.rvWishlist.hide()
                                _binding.tvNoDataFound.show()
                            }
                            wishlistAdapter.notifyDataSetChanged()
                        }
                        0 -> {
                            Utils.errorAlert(
                                requireActivity(),
                                response.body.message.toString()
                            )
                        }
                        9 -> {
                            Utils.errorAlert(
                                requireActivity(),
                                response.body.message.toString()
                            )
                            openActivity(ActWelCome::class.java)
                        }
                    }
                }

                is NetworkResponse.ApiError -> {
                    Utils.dismissLoadingProgress()
                    if (response.body.status == 9) {
                        Utils.setInvalidToekn(requireActivity())
                    } else {
                        Utils.errorAlert(
                            requireActivity(),
                            response.body.message.toString()
                        )
                    }
                }

                is NetworkResponse.NetworkError -> {
                    Utils.dismissLoadingProgress()
                    Utils.errorAlert(
                        requireActivity(),
                        resources.getString(R.string.internet_connection_error)
                    )
                }

                is NetworkResponse.UnknownError -> {
                    Utils.dismissLoadingProgress()
                    Utils.errorAlert(
                        requireActivity(),
                        "Something went wrong"
                    )
                }
            }
        }
    }


    private fun addtocartApi(addtocart: HashMap<String, String>, i: Int, variantId: Int?) {
        Utils.showLoadingProgress(requireActivity())
        lifecycleScope.launch {
            when (val response = ApiClient.getClient(requireActivity())
                .addtocart(addtocart)) {
                is NetworkResponse.Success -> {
                    Utils.dismissLoadingProgress()
                    val addtocart = response.body.data
                    when (response.body.status) {
                        1 -> {
                            dlgConfirm(response.body.data?.message)
                            SharePreference.setStringPref(
                                requireActivity(),
                                SharePreference.cartCount,
                                addtocart?.count.toString()
                            )
                            setCount(addtocart?.count)
                            wishlistAdapter.notifyItemChanged(i)
                        }
                        0 -> {
                            dlgAlreadyCart(response.body.data?.message, wishList[i].productId, variantId)
                        }
                        9 -> {
                            Utils.errorAlert(
                                requireActivity(),
                                addtocart?.message.toString()
                            )
                            openActivity(ActWelCome::class.java)
                            requireActivity().finish()
                            requireActivity().finishAffinity()
                        }
                    }
                }

                is NetworkResponse.ApiError -> {
                    Utils.dismissLoadingProgress()
                    if (response.body.status == 9) {
                        Utils.setInvalidToekn(requireActivity())
                    } else {
                        Utils.errorAlert(
                            requireActivity(),
                            response.body.message.toString()
                        )
                    }
                }

                is NetworkResponse.NetworkError -> {
                    Utils.dismissLoadingProgress()
                    Utils.errorAlert(requireActivity(), resources.getString(R.string.internet_connection_error))
                }

                is NetworkResponse.UnknownError -> {
                    Utils.dismissLoadingProgress()
                    Utils.errorAlert(requireActivity(), "Something went wrong")
                }
            }
        }
    }


    //TODO checkput or continue shopping dialog
    private fun dlgConfirm(message: String?) {
        val confirmDialogBinding = DlgConfirmBinding.inflate(layoutInflater)
        val dialog = Dialog(requireActivity())
        dialog.setContentView(confirmDialogBinding.root)
        confirmDialogBinding.tvProductDesc.text = message
        confirmDialogBinding.tvCheckOut.setOnClickListener {
            dialog.dismiss()
            openActivity(ActShoppingCart::class.java)
        }
        confirmDialogBinding.tvContinueShopping.setOnClickListener {
            dialog.dismiss()
        }

        dialog.show()
    }

    //TODO item already cart dialog
    private fun dlgAlreadyCart(message: String?, id: Int?, defaultVariantId: Int?) {
        val builder = AlertDialog.Builder(requireActivity())
        builder.setTitle(R.string.app_name)
        builder.setMessage(R.string.product_already_again)
        builder.setPositiveButton(getString(R.string.yes)) { dialogInterface, which ->
            val cartqty = java.util.HashMap<String, String>()
            count = 1
            cartqty["product_id"] = id.toString()
            cartqty["user_id"] =
                SharePreference.getStringPref(
                    requireActivity(),
                    SharePreference.userId
                ).toString()
            cartqty["variant_id"] = defaultVariantId.toString()
            cartqty["quantity_type"] = "increase"
            cartqty["theme_id"]=getString(R.string.theme_id)
            cartQtyApi(cartqty, "increase")

        }
        builder.setNegativeButton(getString(R.string.no)) { dialogInterface, which ->
            builder.setCancelable(true)
        }
        val alertDialog: AlertDialog = builder.create()
        alertDialog.setCancelable(false)
        alertDialog.show()
    }


    //TODO qty api
    private fun cartQtyApi(
        cartqty: java.util.HashMap<String, String>,
        type: String
    ) {
        Utils.showLoadingProgress(requireActivity())
        lifecycleScope.launch {
            when (val response = ApiClient.getClient(requireActivity())
                .cartQty(cartqty)) {
                is NetworkResponse.Success -> {
                    Utils.dismissLoadingProgress()
                    val cartQtyResponse = response.body.data
                    SharePreference.setStringPref(
                        requireActivity(),
                        SharePreference.cartCount,
                        response.body.count.toString()
                    )

                    when (response.body.status) {
                        1 -> {
                            Utils.dismissLoadingProgress()
                            wishlistAdapter.notifyDataSetChanged()

                        }
                        0 -> {
                            Utils.errorAlert(
                                requireActivity(),
                                cartQtyResponse?.message.toString()
                            )
                        }
                        9 -> {
                            Utils.errorAlert(
                                requireActivity(),
                                cartQtyResponse?.message.toString()
                            )
                            openActivity(ActWelCome::class.java)
                        }
                    }
                }

                is NetworkResponse.ApiError -> {
                    Utils.dismissLoadingProgress()

                    if (response.body.status == 9) {
                        Utils.setInvalidToekn(requireActivity())
                    } else {
                        Utils.errorAlert(
                            requireActivity(), response.body.message.toString()
                        )
                    }
                }

                is NetworkResponse.NetworkError -> {
                    Utils.dismissLoadingProgress()
                    Utils.errorAlert(
                        requireActivity(),
                        resources.getString(R.string.internet_connection_error)
                    )
                }

                is NetworkResponse.UnknownError -> {
                    Utils.dismissLoadingProgress()
                    Utils.errorAlert(requireActivity(), "Something went wrong")
                }
            }
        }
    }

    private fun setCount(count: Int?) {
        _binding.tvCount.text = count.toString()
    }

    override fun onResume() {
        super.onResume()
        currentPage = 1
        isLastPage = false
        isLoading = false
        wishList.clear()
        wishListAdapter(wishList)
        callWishList()
        _binding.tvCount.text = SharePreference.getStringPref(requireActivity(), SharePreference.cartCount).toString()
    }


}