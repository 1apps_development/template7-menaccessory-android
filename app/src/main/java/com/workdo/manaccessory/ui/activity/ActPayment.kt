package com.workdo.manaccessory.ui.activity

import android.content.Intent
import android.content.SharedPreferences
import android.net.Uri
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import androidx.lifecycle.lifecycleScope
import androidx.recyclerview.widget.GridLayoutManager
import com.workdo.manaccessory.R
import com.workdo.manaccessory.adapter.PaymentAdapter
import com.workdo.manaccessory.api.ApiClient
import com.workdo.manaccessory.base.BaseActivity
import com.workdo.manaccessory.databinding.ActPaymentBinding
import com.workdo.manaccessory.model.PaymentData
import com.workdo.manaccessory.remote.NetworkResponse
import com.workdo.manaccessory.ui.authentication.ActWelCome
import com.workdo.manaccessory.utils.ExtensionFunctions.hide
import com.workdo.manaccessory.utils.ExtensionFunctions.show
import com.workdo.manaccessory.utils.SharePreference
import com.workdo.manaccessory.utils.Utils
import kotlinx.coroutines.launch


class ActPayment : BaseActivity() {
    private lateinit var _binding: ActPaymentBinding
    private var paymentList = ArrayList<PaymentData>()
    private lateinit var paymentAdapter: PaymentAdapter
    private var manager: GridLayoutManager? = null
    var comment = ""
    var paymentName = ""
    var stripeKey = ""

    override fun setLayout(): View = _binding.root

    override fun initView() {
       _binding= ActPaymentBinding.inflate(layoutInflater)
        init()
    }

    private fun init(){

        manager = GridLayoutManager(this@ActPayment, 1, GridLayoutManager.VERTICAL, false)

        _binding.ivBack.setOnClickListener { finish() }

        _binding.btnContinue.setOnClickListener {
            if (_binding.chbTermsCondition.isChecked){
                comment=_binding.edDescription.text.toString()
                SharePreference.setStringPref(this@ActPayment,SharePreference.Payment_Comment.toString(),comment)
                openActivity(ActConfirm::class.java)
            }
        }

        _binding.tvTerms.setOnClickListener {
            val tems = SharePreference.getStringPref(this@ActPayment,SharePreference.Terms).toString()
            val uri:Uri = Uri.parse(tems)
            val intent = Intent(Intent.ACTION_VIEW,uri)
            intent.addCategory(Intent.CATEGORY_BROWSABLE)

            startActivity(intent)
        }

        _binding.chbTermsCondition.setOnClickListener {
            _binding.btnContinue.isEnabled = _binding.chbTermsCondition.isChecked
        }


        _binding.tvTerms.setOnClickListener {

            val contactUs=
                SharePreference.getStringPref(this@ActPayment, SharePreference.Terms).toString()
            val uri: Uri =
                Uri.parse(contactUs)
            val intent = Intent(Intent.ACTION_VIEW, uri)
            intent.addCategory(Intent.CATEGORY_BROWSABLE)

            startActivity(intent)
        }
    }

    private fun callPaymentList() {
        Utils.showLoadingProgress(this@ActPayment)
        val paymentRequest = HashMap<String,String>()
        paymentRequest["theme_id"] = getString(R.string.theme_id)

        lifecycleScope.launch {
            when (val response = ApiClient.getClient(this@ActPayment)
                .paymentList(paymentRequest)) {
                is NetworkResponse.Success -> {
                    Utils.dismissLoadingProgress()
                    val paymentListResponse = response.body
                    when (response.body.status) {
                        1 -> {
                            if ((response.body.data?.size ?: 0) > 0) {
                                _binding.rvPayment.show()
                                _binding.vieww.hide()

                                runOnUiThread {
                                    paymentListResponse.data?.let {
                                        paymentList.addAll(it)
                                    }
                                }
                                paymentList.removeAll {
                                    it.status == "off"
                                }
                                Log.e("paymentList",paymentList.size.toString())
                                paymentListAdapter(paymentList)

                            } else {
                                _binding.rvPayment.hide()
                                _binding.vieww.show()
                            }
                        }
                        0 -> {
                            Utils.errorAlert(
                                this@ActPayment,
                                paymentListResponse.message.toString()
                            )
                        }
                        9 -> {
                            Utils.errorAlert(
                                this@ActPayment,
                                response.body.message.toString()
                            )
                            openActivity(ActWelCome::class.java)
                        }
                    }
                }

                is NetworkResponse.ApiError -> {
                    Utils.dismissLoadingProgress()
                    if (response.body.status == 9) {
                        Utils.setInvalidToekn(this@ActPayment)
                    } else {
                        Utils.errorAlert(
                            this@ActPayment,
                            response.body.message.toString()
                        )
                    }
                }

                is NetworkResponse.NetworkError -> {
                    Utils.dismissLoadingProgress()
                    Utils.errorAlert(
                        this@ActPayment,
                        resources.getString(R.string.internet_connection_error)
                    )
                }

                is NetworkResponse.UnknownError -> {
                    Utils.dismissLoadingProgress()
                    Utils.errorAlert(
                        this@ActPayment,
                        "Something went wrong"
                    )
                }
            }
        }
    }

    private fun paymentListAdapter(paymentList: ArrayList<PaymentData>) {
        _binding.rvPayment.layoutManager=manager
        paymentAdapter = PaymentAdapter(this@ActPayment,paymentList) { i:Int,s:String ->
            paymentName = paymentList[i].nameString.toString()

            SharePreference.setStringPref(
                this@ActPayment,
                SharePreference.Payment_Type,
                paymentName
            )

            SharePreference.setStringPref(
                this@ActPayment,
                SharePreference.PaymentImage,
                paymentList[i].image.toString()
            )
        }
        _binding.rvPayment.adapter=paymentAdapter
    }

    override fun onResume() {
        super.onResume()
        paymentList.clear()

        callPaymentList()
    }


}