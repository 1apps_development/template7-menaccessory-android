package com.workdo.manaccessory.ui.activity

import android.app.AlertDialog
import android.util.Log
import android.view.View
import androidx.core.app.ActivityCompat
import androidx.fragment.app.Fragment
import com.workdo.manaccessory.R
import com.workdo.manaccessory.base.BaseActivity
import com.workdo.manaccessory.databinding.ActivityMainBinding
import com.workdo.manaccessory.ui.authentication.ActWelCome
import com.workdo.manaccessory.ui.fragment.*
import com.workdo.manaccessory.utils.SharePreference
import com.workdo.manaccessory.utils.Utils

class MainActivity : BaseActivity() {
    private lateinit var _binding: ActivityMainBinding
    var pos = ""
    override fun setLayout(): View = _binding.root

    override fun initView() {
        _binding = ActivityMainBinding.inflate(layoutInflater)

        val name = SharePreference.getStringPref(this@MainActivity, SharePreference.userName)
        Log.e("name", name.toString())
        pos = intent.getStringExtra("pos").toString()
        Log.e("Pos", pos)
        if (SharePreference.getStringPref(this@MainActivity, SharePreference.cartCount)
                .isNullOrEmpty()
        ) {
            SharePreference.setStringPref(this@MainActivity, SharePreference.cartCount, "0")
        }

        when (pos) {
            "1" -> {
                setCurrentFragment(FragHome())
                _binding.bottomNavigation.selectedItemId = R.id.ivHome
            }
            "2" -> {
                setCurrentFragment(FragProducts())
                _binding.bottomNavigation.selectedItemId = R.id.ivHome
            }
            "3" -> {
                setCurrentFragment(FragCategories())
                _binding.bottomNavigation.selectedItemId = R.id.ivCategories
            }
            "4" -> {
                setCurrentFragment(FragWishList())
                _binding.bottomNavigation.selectedItemId = R.id.ivDashBoard
            }
            "5" -> {
                setCurrentFragment(FragSettings())
                _binding.bottomNavigation.selectedItemId = R.id.ivGifts
            }
            else -> {
                setCurrentFragment(FragHome())
            }
        }
        bottomSheetItemNavigation()
    }

    private fun bottomSheetItemNavigation() {
        _binding.bottomNavigation.setOnItemSelectedListener { item ->
            val fragment = supportFragmentManager.findFragmentById(R.id.mainContainer)

            when (item.itemId) {
                R.id.ivHome -> {
                    if (fragment !is FragHome) {
                        supportFragmentManager.fragments.clear()
                        setCurrentFragment(FragHome())
                    }
                    true
                }
                R.id.ivProduct -> {
                    if (fragment !is FragProducts) {
                        supportFragmentManager.fragments.clear()
                        setCurrentFragment(FragProducts())
                    }
                    true
                }
                R.id.ivDashBoard -> {

                    if (fragment !is FragCategories) {
                        supportFragmentManager.fragments.clear()
                        setCurrentFragment(FragCategories())
                        SharePreference.setStringPref(this@MainActivity,
                            SharePreference.clickMenu, "click")
                    } else {
                        //fragment.showLayout()
                    }
                    true

                }
                R.id.ivOccasions -> {
                    if (Utils.isLogin(this@MainActivity)) {
                        if (fragment !is FragWishList) {
                            supportFragmentManager.fragments.clear()
                            setCurrentFragment(FragWishList())
                        }
                    } else {
                        Utils.openWelcomeScreen(this@MainActivity)
                    }

                    true
                }
                R.id.ivGifts -> {
                    if (Utils.isLogin(this@MainActivity)) {
                        if (fragment !is FragSettings) {
                            supportFragmentManager.fragments.clear()
                            setCurrentFragment(FragSettings())
                        }
                    } else {
                        Utils.openWelcomeScreen(this@MainActivity)
                    }
                    true
                }


                else -> {
                    false
                }
            }
        }
    }


    private fun setCurrentFragment(fragment: Fragment) =
        supportFragmentManager.beginTransaction().apply {
            replace(R.id.mainContainer, fragment)
            commit()
        }

    override fun onBackPressed() {
        if (supportFragmentManager.backStackEntryCount == 1) {
            finishAffinity()
        } else {
            mExitDialog()
        }
    }

    private fun mExitDialog() {
        val builder = AlertDialog.Builder(this@MainActivity)
        builder.setTitle(R.string.app_name)
        builder.setMessage(R.string.exit)
        builder.setPositiveButton(getString(R.string.yes)) { dialogInterface, which ->
            dialogInterface.dismiss()
            builder.setCancelable(true)
            ActivityCompat.finishAfterTransition(this@MainActivity)
            ActivityCompat.finishAffinity(this@MainActivity)
            finish()
        }
        builder.setNegativeButton(getString(R.string.no)) { dialogInterface, which ->
            dialogInterface.dismiss()
            builder.setCancelable(true)
        }
        val alertDialog: AlertDialog = builder.create()
        alertDialog.setCancelable(false)
        alertDialog.show()
    }

}
