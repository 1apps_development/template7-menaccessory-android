package com.workdo.manaccessory.ui.authentication

import android.content.Intent
import android.view.View
import com.workdo.manaccessory.base.BaseActivity
import com.workdo.manaccessory.databinding.ActWelComeBinding
import com.workdo.manaccessory.ui.activity.MainActivity

class ActWelCome : BaseActivity() {
    private lateinit var _binding: ActWelComeBinding

    override fun setLayout(): View = _binding.root

    override fun initView() {
        _binding = ActWelComeBinding.inflate(layoutInflater)
        init()
    }

    private fun init() {
        _binding.btnLogin.setOnClickListener {
            openActivity(ActLoginOption::class.java)
        }
        _binding.btnSignup.setOnClickListener {
            startActivity(
                Intent(
                    this@ActWelCome,
                    ActRegisterOption::class.java
                )
            )
        }
        _binding.btnGuest.setOnClickListener {
            startActivity(
                Intent(
                    this@ActWelCome,
                    MainActivity::class.java
                )
            )
            finish()
        }

    }

}