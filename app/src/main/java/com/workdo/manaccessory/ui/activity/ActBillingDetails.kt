package com.workdo.manaccessory.ui.activity

import android.content.Intent
import android.util.Log
import android.view.View
import androidx.lifecycle.lifecycleScope
import androidx.recyclerview.widget.GridLayoutManager
import com.google.gson.Gson
import com.workdo.manaccessory.R
import com.workdo.manaccessory.adapter.*
import com.workdo.manaccessory.api.ApiClient
import com.workdo.manaccessory.base.BaseActivity
import com.workdo.manaccessory.databinding.ActBillingDetailsBinding
import com.workdo.manaccessory.model.*
import com.workdo.manaccessory.remote.NetworkResponse
import com.workdo.manaccessory.ui.authentication.ActWelCome
import com.workdo.manaccessory.utils.*
import com.workdo.manaccessory.utils.ExtensionFunctions.hide
import com.workdo.manaccessory.utils.ExtensionFunctions.show
import kotlinx.coroutines.launch

class ActBillingDetails : BaseActivity(),
    OnItemClickListenerCountry, OnItemClickListenerState,
    OnItemClickListenerCity, OnItemClickListenerGuestCountry, OnItemClickListenerGuestState,
    OnItemClickListenerGuestCity {

    private lateinit var _binding: ActBillingDetailsBinding
    private lateinit var countryAdapterauto: AutoCompleteCountryAdapter
    private lateinit var countryAdapterGuestauto: AutoCompleteCountryGuestAdapter
    private lateinit var countryAdapterautoState: AutoCompleteStateAdapter
    private lateinit var countryAdapterautoGuestState: AutoCompleteStateGuestAdapter
    private lateinit var countryAdapterautoCity: AutoCompleteCityAdapter
    private lateinit var countryAdapterautoGuestCity: AutoCompleteCityGuestAdapter
    private var cityList = ArrayList<CityListData>()
    internal var isLoading = false
    internal var isLastPage = false
    private var currentPage = 1
    private var total_pages: Int = 0
    private var addressList = ArrayList<AddressListData>()
    private lateinit var billingaddressAdapter: BillingAddressAdapter
    private var countryList = ArrayList<CountryDataItem>()
    private var guestCountryList = ArrayList<CountryDataItem>()
    private var stateList = ArrayList<StateListData>()
    private var guestStateList = ArrayList<StateListData>()

    var countryID = ""
    var cityID = ""
    var guestCityID = ""
    var guestCountryID = ""
    var stateId = ""
    var guestStateId = ""
    var delivery_Address = ""
    var delivery_Postcode = ""
    var delivery_Country = ""
    var delivery_State = ""
    var delivery_City = ""
    private var manager: GridLayoutManager? = null


    override fun setLayout(): View = _binding.root

    override fun initView() {
        _binding = ActBillingDetailsBinding.inflate(layoutInflater)
        init()
        onTouchListener()
    }

    private fun onTouchListener()
    {
        _binding.linearGuestBillingAddress.autoCompleteCountry.onFocusChangeListener =
            View.OnFocusChangeListener { v, hasFocus ->
                if (hasFocus) {
                    _binding.linearGuestBillingAddress.autoCompleteCountry.showDropDown()
                }

            }
        _binding.linearGuestBillingAddress.autoCompleteCity.onFocusChangeListener = View.OnFocusChangeListener { v, hasFocus ->
            if (hasFocus) {
                _binding.linearGuestBillingAddress.autoCompleteCity.showDropDown()
            }

        }
        _binding.linearGuestBillingAddress.autoCompleteState.onFocusChangeListener = View.OnFocusChangeListener { v, hasFocus ->
            if (hasFocus) {
                _binding.linearGuestBillingAddress.autoCompleteState.showDropDown()
            }

        }

        _binding.autoCompleteCityBill.onFocusChangeListener = View.OnFocusChangeListener { v, hasFocus ->
            if (hasFocus) {
                _binding.autoCompleteCityBill.showDropDown()
            }

        }
        _binding.autoCompleteCountryBill.onFocusChangeListener = View.OnFocusChangeListener { v, hasFocus ->
            if (hasFocus) {
                _binding.autoCompleteCountryBill.showDropDown()
            }

        }

        _binding.autoCompleteStateBill.onFocusChangeListener = View.OnFocusChangeListener { v, hasFocus ->
            if (hasFocus) {
                _binding.autoCompleteStateBill.showDropDown()
            }

        }


    }

    private fun init() {
        _binding.tvAddress1.hide()
        _binding.clAddress1.hide()
        _binding.tvCity.hide()
        _binding.clCity.hide()
        _binding.tvpostcode.hide()
        _binding.clPostCode.hide()
        _binding.tvCountry.hide()
        _binding.clCountry.hide()
        _binding.tvRegionState.hide()
        _binding.clRegionState.hide()
        getCountryApi()
        getguestCountryApi()
        if (Utils.isLogin(this@ActBillingDetails)) {
            _binding.edFirstName.setText(
                SharePreference.getStringPref(
                    this@ActBillingDetails,
                    SharePreference.userFirstName
                )
            )
            _binding.edLastName.setText(
                SharePreference.getStringPref(
                    this@ActBillingDetails,
                    SharePreference.userLastName
                )
            )
            _binding.edEmail.setText(
                SharePreference.getStringPref(
                    this@ActBillingDetails,
                    SharePreference.userEmail
                )
            )
            _binding.edPhone.setText(
                SharePreference.getStringPref(
                    this@ActBillingDetails,
                    SharePreference.userMobile
                )
            )
        }

        _binding.btnContinue.setOnClickListener {
            if (_binding.chbTermsCondition.isChecked) {
                if (_binding.edFirstName.text?.isEmpty() == true) {
                    Utils.errorAlert(
                        this@ActBillingDetails,
                        resources.getString(R.string.validation_f_name)
                    )
                } else if (_binding.edLastName.text?.isEmpty() == true) {
                    Utils.errorAlert(
                        this@ActBillingDetails,
                        resources.getString(R.string.validation_l_name)
                    )
                } else if (_binding.edEmail.text?.isEmpty() == true) {
                    Utils.errorAlert(
                        this@ActBillingDetails,
                        resources.getString(R.string.validation_email)
                    )
                } else if (_binding.edPhone.text?.isEmpty() == true) {
                    Utils.errorAlert(
                        this@ActBillingDetails,
                        resources.getString(R.string.validation_phone_number)
                    )
                } else {
                    if (!Utils.isLogin(this@ActBillingDetails)) {
                        if (isBillingAdded()) {
                            val data = BillingDetailsAddress(
                                _binding.linearGuestBillingAddress.edCompanyName.text.toString(),
                                _binding.linearGuestBillingAddress.edBillingAddress.text.toString(),
                                _binding.linearGuestBillingAddress.autoCompleteCity.text.toString(),
                                _binding.linearGuestBillingAddress.edBillingPostCode.text.toString(),
                                guestCountryID,
                                guestStateId,
                                _binding.linearGuestBillingAddress.autoCompleteCountry.text.toString(),
                                _binding.linearGuestBillingAddress.autoCompleteState.text.toString() ,
                                _binding.edPhone.text.toString(),
                                _binding.linearGuestBillingAddress.edBillingAddress.text.toString(),
                                _binding.linearGuestBillingAddress.autoCompleteCity.text.toString(),
                                guestCountryID,
                                _binding.linearGuestBillingAddress.edBillingPostCode.text.toString(),
                                guestStateId,
                                _binding.linearGuestBillingAddress.autoCompleteCountry.text.toString(),
                                _binding.linearGuestBillingAddress.autoCompleteState.text.toString() ,
                                _binding.edEmail.text.toString(),
                                _binding.edFirstName.text.toString(),
                                _binding.edLastName.text.toString(),
                            )
                            SharePreference.setStringPref(
                                this@ActBillingDetails,
                                SharePreference.BillingDetails,
                                Gson().toJson(data)
                            )
                            SharePreference.setStringPref(
                                this@ActBillingDetails, SharePreference.userMobile,
                                _binding.edPhone.text.toString()
                            )
                            openActivity(ActDelivery::class.java)

                        }
                    } else {
                        if (addressList.size > 0) {
                            SharePreference.setStringPref(
                                this@ActBillingDetails, SharePreference.userMobile,
                                _binding.edPhone.text.toString()
                            )
                            openActivity(ActDelivery::class.java)
                        } else {
                            Utils.errorAlert(
                                this@ActBillingDetails,
                                resources.getString(R.string.please_select_address)
                            )
                        }
                    }
                }
            } else {
                when {
                    _binding.edAddress.text?.isEmpty() == true -> {
                        Utils.errorAlert(
                            this@ActBillingDetails,
                            resources.getString(R.string.please_enter_delivery_address)
                        )
                    }
                    _binding.autoCompleteCityBill.text?.isEmpty() == true -> {
                        Utils.errorAlert(
                            this@ActBillingDetails,
                            resources.getString(R.string.please_enter_delivery_city)
                        )
                    }
                    _binding.edPostCode.text?.isEmpty() == true -> {
                        Utils.errorAlert(
                            this@ActBillingDetails,
                            resources.getString(R.string.please_enter_delivery_postcode)
                        )
                    }
                    _binding.edPostCode.text.length < 6 -> {
                        Utils.errorAlert(
                            this@ActBillingDetails,
                            resources.getString(R.string.valid_postal_code)
                        )
                    }
                    _binding.autoCompleteCountryBill.text?.isEmpty() == true -> {
                        Utils.errorAlert(
                            this@ActBillingDetails,
                            resources.getString(R.string.please_enter_country)
                        )
                    }
                    _binding.autoCompleteStateBill.text?.isEmpty() == true -> {
                        Utils.errorAlert(
                            this@ActBillingDetails,
                            resources.getString(R.string.please_enter_state)
                        )
                    }
                    else -> {

                        if (!Utils.isLogin(this@ActBillingDetails)) {
                            val data = BillingDetailsAddress(
                                _binding.linearGuestBillingAddress.edCompanyName.text.toString(),

                                _binding.linearGuestBillingAddress.edBillingAddress.text.toString(),
                                _binding.linearGuestBillingAddress.autoCompleteCity.text.toString(),
                                _binding.linearGuestBillingAddress.edBillingPostCode.text.toString(),
                                guestCountryID,
                                guestStateId,
                                _binding.linearGuestBillingAddress.autoCompleteCountry.text.toString(),
                                _binding.linearGuestBillingAddress.autoCompleteState.text.toString() ,
                                _binding.edPhone.text.toString(),
                                _binding.edAddress.text.toString(),
                                _binding.autoCompleteCityBill.text.toString(),
                                countryID,
                                _binding.edPostCode.text.toString(),
                                stateId,
                                _binding.autoCompleteCountryBill.text.toString(),
                                _binding.autoCompleteStateBill.text.toString(),
                                _binding.edEmail.text.toString(),
                                _binding.edFirstName.text.toString(),
                                _binding.edLastName.text.toString())
                            SharePreference.setStringPref(
                                this@ActBillingDetails,
                                SharePreference.BillingDetails,
                                Gson().toJson(data)
                            )
                            SharePreference.setStringPref(
                                this@ActBillingDetails, SharePreference.userMobile,
                                _binding.edPhone.text.toString()
                            )
                            openActivity(ActDelivery::class.java)
                        } else {


                            delivery_Address = _binding.edAddress.text.toString()
                            delivery_City = _binding.autoCompleteCityBill.text.toString()
                            delivery_Postcode = _binding.edPostCode.text.toString()
                            delivery_Country = countryID
                            delivery_State = stateId
                            Log.e(
                                "DeliveryAddress",
                                delivery_Address + delivery_City + delivery_State + delivery_Country + delivery_Postcode
                            )
                            SharePreference.setStringPref(
                                this@ActBillingDetails,
                                SharePreference.Delivery_Address,
                                delivery_Address
                            )
                            SharePreference.setStringPref(
                                this@ActBillingDetails,
                                SharePreference.Delivery_Postcode,
                                delivery_Postcode
                            )
                            SharePreference.setStringPref(
                                this@ActBillingDetails,
                                SharePreference.Delivery_Country,
                                delivery_Country
                            )
                            SharePreference.setStringPref(
                                this@ActBillingDetails,
                                SharePreference.Delivery_State,
                                delivery_State
                            )
                            SharePreference.setStringPref(
                                this@ActBillingDetails,
                                SharePreference.Delivery_City,
                                delivery_City
                            )
                            SharePreference.setStringPref(
                                this@ActBillingDetails, SharePreference.userMobile,
                                _binding.edPhone.text.toString()
                            )
                            if (addressList.size > 0) {
                                SharePreference.setStringPref(
                                    this@ActBillingDetails, SharePreference.userMobile,
                                    _binding.edPhone.text.toString()
                                )
                                openActivity(ActDelivery::class.java)
                            } else {
                                Utils.errorAlert(
                                    this@ActBillingDetails,
                                    resources.getString(R.string.please_select_address)
                                )
                            }
                        }
                    }
                }
            }
        }

        _binding.ivBack.setOnClickListener { finish() }
        _binding.tvAddAddress.setOnClickListener {
            val intent = Intent(this@ActBillingDetails, ActAddAddress::class.java)
            intent.putExtra("AddAddress", "AddAddress")
            startActivity(intent)
        }
        _binding.chbTermsCondition.isChecked = true
        manager =
            GridLayoutManager(this@ActBillingDetails, 1, GridLayoutManager.VERTICAL, false)
        pagination()
        _binding.chbTermsCondition.setOnClickListener {
            if (!_binding.chbTermsCondition.isChecked) {

                _binding.tvDeliveryAddress.show()
                _binding.tvAddress1.show()
                _binding.clAddress1.show()
                _binding.tvCity.show()
                _binding.clCity.show()
                _binding.tvpostcode.show()
                _binding.clPostCode.show()
                _binding.tvCountry.show()
                _binding.clCountry.show()
                _binding.tvRegionState.show()
                _binding.clRegionState.show()
            } else {

                _binding.tvDeliveryAddress.hide()
                _binding.tvAddress1.hide()
                _binding.clAddress1.hide()
                _binding.tvCity.hide()
                _binding.clCity.hide()
                _binding.tvpostcode.hide()
                _binding.clPostCode.hide()
                _binding.tvCountry.hide()
                _binding.clCountry.hide()
                _binding.tvRegionState.hide()
                _binding.clRegionState.hide()
            }
        }

    }


    override fun onItemClick(item: CountryDataItem?) {
        Log.e("StateId", item?.id.toString())
        _binding.autoCompleteStateBill.text.clear()
        _binding.autoCompleteCityBill.text.clear()
        _binding.autoCompleteCountryBill.setText(item?.name)
        countryID = item?.id.toString()
        stateId = item?.id.toString()
        getStateApi(item?.id.toString())
    }

    override fun onItemClickState(item: StateListData?) {
        _binding.autoCompleteStateBill.setText(item?.name)
        _binding.autoCompleteCityBill.text.clear()
        stateId = item?.id.toString()
        cityID = item?.id.toString()
        getCityApi(item?.id.toString())
    }

    override fun onItemClickCity(item: CityListData?) {
        _binding.autoCompleteCityBill.dismissDropDown()
        _binding.autoCompleteCityBill.setText(item?.name)
        cityID = item?.id.toString()
        _binding.edPostCode.nextFocusForwardId

    }

    private fun getguestCountryApi() {
        Utils.showLoadingProgress(this@ActBillingDetails)
        val countryHashmap = HashMap<String, String>()
        countryHashmap["theme_id"] = getString(R.string.theme_id)
        lifecycleScope.launch {
            when (val response = ApiClient.getClient(this@ActBillingDetails)
                .setCountryList(countryHashmap)) {
                is NetworkResponse.Success -> {
                    Utils.dismissLoadingProgress()
                    val countryResponse = response.body
                    when (response.body.status) {
                        1 -> {
                            guestCountryList = countryResponse.data!!
                            loadGuestSpinnerCountry(guestCountryList)
                        }
                        0 -> {
                            Utils.errorAlert(
                                this@ActBillingDetails,
                                countryResponse.message.toString()
                            )
                        }
                        9 -> {
                            Utils.errorAlert(
                                this@ActBillingDetails,
                                response.body.message.toString()
                            )
                            openActivity(ActWelCome::class.java)
                        }
                    }
                }

                is NetworkResponse.ApiError -> {
                    Utils.dismissLoadingProgress()
                    if (response.body.status == 9) {
                        Utils.setInvalidToekn(this@ActBillingDetails)
                    } else {
                        Utils.errorAlert(
                            this@ActBillingDetails,
                            response.body.message.toString()
                        )
                    }
                }

                is NetworkResponse.NetworkError -> {
                    Utils.dismissLoadingProgress()
                    Utils.errorAlert(
                        this@ActBillingDetails,
                        resources.getString(R.string.internet_connection_error)
                    )
                }

                is NetworkResponse.UnknownError -> {
                    Utils.dismissLoadingProgress()
                    Utils.errorAlert(
                        this@ActBillingDetails,
                        "Something went wrong"
                    )
                }
                else -> {}
            }
        }
    }

    private fun loadGuestSpinnerCountry(guestCountryList: ArrayList<CountryDataItem>) {
        countryAdapterGuestauto =
            AutoCompleteCountryGuestAdapter(
                this@ActBillingDetails,
                countryList,
                OnItemClickListenerGuestCountry {
                    onItemClickGuest(it)
                    Log.e("StateId", it.id.toString())

                })

        _binding.linearGuestBillingAddress.autoCompleteCountry.threshold = 0
        _binding.linearGuestBillingAddress.autoCompleteCountry.setAdapter(countryAdapterGuestauto)

    }

    private fun getCountryApi() {
        Utils.showLoadingProgress(this@ActBillingDetails)
        val countryHashmap = HashMap<String, String>()
        countryHashmap["theme_id"] = getString(R.string.theme_id)
        lifecycleScope.launch {
            when (val response = ApiClient.getClient(this@ActBillingDetails)
                .setCountryList(countryHashmap)) {
                is NetworkResponse.Success -> {
                    Utils.dismissLoadingProgress()
                    val countryResponse = response.body
                    when (response.body.status) {
                        1 -> {
                            countryList = countryResponse.data!!
                            loadSpinnerCountry(countryList)
                        }
                        0 -> {
                            Utils.errorAlert(
                                this@ActBillingDetails,
                                countryResponse.message.toString()
                            )
                        }
                        9 -> {
                            Utils.errorAlert(
                                this@ActBillingDetails,
                                response.body.message.toString()
                            )
                            openActivity(ActWelCome::class.java)
                        }
                    }
                }

                is NetworkResponse.ApiError -> {
                    Utils.dismissLoadingProgress()
                    if (response.body.status == 9) {
                        Utils.setInvalidToekn(this@ActBillingDetails)
                    } else {
                        Utils.errorAlert(
                            this@ActBillingDetails,
                            response.body.message.toString()
                        )
                    }
                }

                is NetworkResponse.NetworkError -> {
                    Utils.dismissLoadingProgress()
                    Utils.errorAlert(
                        this@ActBillingDetails,
                        resources.getString(R.string.internet_connection_error)
                    )
                }

                is NetworkResponse.UnknownError -> {
                    Utils.dismissLoadingProgress()
                    Utils.errorAlert(
                        this@ActBillingDetails,
                        "Something went wrong"
                    )
                }
            }
        }
    }

    private fun loadSpinnerCountry(countryList: ArrayList<CountryDataItem>) {
        countryAdapterauto =
            AutoCompleteCountryAdapter(
                this@ActBillingDetails,
                countryList,
                OnItemClickListenerCountry {
                    onItemClick(it)
                    Log.e("StateId", it.id.toString())

                })

        _binding.autoCompleteCountryBill.threshold = 0
        _binding.autoCompleteCountryBill.setAdapter(countryAdapterauto)
    }

    private fun isBillingAdded(): Boolean {
        _binding.linearGuestBillingAddress.apply {

            when {

                edBillingAddress.text.isEmpty() -> {
                    Utils.errorAlert(
                        this@ActBillingDetails,
                        resources.getString(R.string.please_enter_address)
                    )
                    return false
                }

                autoCompleteCity.text.isEmpty() -> {
                    Utils.errorAlert(
                        this@ActBillingDetails,
                        resources.getString(R.string.please_enter_city)
                    )
                    return false
                }
                edBillingPostCode.text.isEmpty() -> {
                    Utils.errorAlert(
                        this@ActBillingDetails,
                        resources.getString(R.string.please_enter_postcode)
                    )
                    return false
                }
                edBillingPostCode.text.length < 6 -> {
                    Utils.errorAlert(this@ActBillingDetails,resources.getString(R.string.valid_postal_code))
                    return false
                }
                autoCompleteCountry.text.isEmpty() -> {
                    Utils.errorAlert(
                        this@ActBillingDetails,
                        resources.getString(R.string.please_enter_country)
                    )
                    return false
                }
                autoCompleteState.text.isEmpty() -> {
                    Utils.errorAlert(
                        this@ActBillingDetails,
                        resources.getString(R.string.please_enter_state)
                    )
                    return false
                }
                else -> {
                    return true
                }

            }

        }
    }


    private fun pagination() {
        val paginationListener = object : PaginationScrollListener(manager) {
            override fun isLastPage(): Boolean {
                return isLastPage
            }

            override fun isLoading(): Boolean {
                return isLoading
            }

            override fun loadMoreItems() {
                isLoading = true
                currentPage++
                callAddressList()
            }
        }
        _binding.rvGetAddress.addOnScrollListener(paginationListener)
    }

    private fun getGuestStateApi(countryID: String) {
        Utils.showLoadingProgress(this@ActBillingDetails)
        val stateListMap = HashMap<String, String>()
        stateListMap["country_id"] = countryID
        stateListMap["theme_id"] = getString(R.string.theme_id)
        lifecycleScope.launch {
            when (val response = ApiClient.getClient(this@ActBillingDetails)
                .setStateList(stateListMap)) {
                is NetworkResponse.Success -> {
                    Utils.dismissLoadingProgress()
                    val stateListResponse = response.body
                    when (response.body.status) {
                        1 -> {
                            guestStateList = stateListResponse.data!!
                            loadGuestSpinnerState(guestStateList)
                        }
                        0 -> {
                            Utils.errorAlert(
                                this@ActBillingDetails,
                                stateListResponse.message.toString()
                            )
                        }
                        9 -> {
                            Utils.errorAlert(
                                this@ActBillingDetails,
                                response.body.message.toString()
                            )
                            openActivity(ActWelCome::class.java)
                        }
                    }
                }

                is NetworkResponse.ApiError -> {
                    Utils.dismissLoadingProgress()
                    if (response.body.status == 9) {
                        Utils.setInvalidToekn(this@ActBillingDetails)
                    } else {
                        Utils.errorAlert(
                            this@ActBillingDetails,
                            response.body.message.toString()
                        )
                    }
                }

                is NetworkResponse.NetworkError -> {
                    Utils.dismissLoadingProgress()
                    Utils.errorAlert(
                        this@ActBillingDetails,
                        resources.getString(R.string.internet_connection_error)
                    )
                }

                is NetworkResponse.UnknownError -> {
                    Utils.dismissLoadingProgress()
                    Utils.errorAlert(
                        this@ActBillingDetails,
                        "Something went wrong"
                    )
                }
            }
        }
    }

    private fun loadGuestSpinnerState(stateList: ArrayList<StateListData>) {
        countryAdapterautoGuestState =
            AutoCompleteStateGuestAdapter(
                this@ActBillingDetails,
                stateList
            ) {
                onItemClickStateGuest(it)
                Log.e("StateId", it.id.toString())

            }

        _binding.linearGuestBillingAddress.autoCompleteState.threshold = 0
        _binding.linearGuestBillingAddress.autoCompleteState.setAdapter(countryAdapterautoGuestState)
    }

    private fun getStateApi(countryID: String) {
        Utils.showLoadingProgress(this@ActBillingDetails)
        val stateListMap = HashMap<String, String>()
        stateListMap["country_id"] = countryID
        stateListMap["theme_id"] = getString(R.string.theme_id)
        lifecycleScope.launch {
            when (val response = ApiClient.getClient(this@ActBillingDetails)
                .setStateList(stateListMap)) {
                is NetworkResponse.Success -> {
                    Utils.dismissLoadingProgress()
                    val stateListResponse = response.body
                    when (response.body.status) {
                        1 -> {
                            stateList = stateListResponse.data!!
                            loadSpinnerState(stateList)
                        }
                        0 -> {
                            Utils.errorAlert(
                                this@ActBillingDetails,
                                stateListResponse.message.toString()
                            )
                        }
                        9 -> {
                            Utils.errorAlert(
                                this@ActBillingDetails,
                                response.body.message.toString()
                            )
                            openActivity(ActWelCome::class.java)
                        }
                    }
                }

                is NetworkResponse.ApiError -> {
                    Utils.dismissLoadingProgress()
                    if (response.body.status == 9) {
                        Utils.setInvalidToekn(this@ActBillingDetails)
                    } else {
                        Utils.errorAlert(
                            this@ActBillingDetails,
                            response.body.message.toString()
                        )
                    }
                }

                is NetworkResponse.NetworkError -> {
                    Utils.dismissLoadingProgress()
                    Utils.errorAlert(
                        this@ActBillingDetails,
                        resources.getString(R.string.internet_connection_error)
                    )
                }

                is NetworkResponse.UnknownError -> {
                    Utils.dismissLoadingProgress()
                    Utils.errorAlert(
                        this@ActBillingDetails,
                        "Something went wrong"
                    )
                }
            }
        }
    }

    private fun loadSpinnerState(stateList: ArrayList<StateListData>) {
        countryAdapterautoState =
            AutoCompleteStateAdapter(
                this@ActBillingDetails,
                stateList
            ) {
                onItemClickState(it)
                Log.e("StateId", it.id.toString())

            }

        _binding.autoCompleteStateBill.threshold = 0
        _binding.autoCompleteStateBill.setAdapter(countryAdapterautoState)
    }

    private fun getCityApi(stateId: String) {
        Utils.showLoadingProgress(this@ActBillingDetails)
        val stateListMap = HashMap<String, String>()
        stateListMap["state_id"] = stateId
        stateListMap["theme_id"] = getString(R.string.theme_id)
        lifecycleScope.launch {
            when (val response = ApiClient.getClient(this@ActBillingDetails)
                .setCityList(stateListMap)) {
                is NetworkResponse.Success -> {
                    Utils.dismissLoadingProgress()
                    val stateListResponse = response.body
                    when (response.body.status) {
                        1 -> {
                            cityList = stateListResponse.data!!

                            loadCityAdapter(cityList)
                        }
                        0 -> {
                            Utils.errorAlert(
                                this@ActBillingDetails,
                                stateListResponse.message.toString()
                            )
                        }
                        9 -> {
                            Utils.errorAlert(
                                this@ActBillingDetails,
                                response.body.message.toString()
                            )
                            openActivity(ActWelCome::class.java)
                        }
                    }
                }

                is NetworkResponse.ApiError -> {
                    Utils.dismissLoadingProgress()
                    if (response.body.status == 9) {
                        Utils.setInvalidToekn(this@ActBillingDetails)
                    } else {
                        Utils.errorAlert(
                            this@ActBillingDetails,
                            response.body.message.toString()
                        )
                    }
                }

                is NetworkResponse.NetworkError -> {
                    Utils.dismissLoadingProgress()
                    Utils.errorAlert(
                        this@ActBillingDetails,
                        resources.getString(R.string.internet_connection_error)
                    )
                }

                is NetworkResponse.UnknownError -> {
                    Utils.dismissLoadingProgress()
                    Utils.errorAlert(
                        this@ActBillingDetails,
                        "Something went wrong"
                    )
                }
            }
        }
    }

    private fun loadCityAdapter(cityList: ArrayList<CityListData>) {
        countryAdapterautoCity =
            AutoCompleteCityAdapter(
                this@ActBillingDetails,
                cityList
            ) {
                onItemClickCity(it)
                Log.e("StateId", it.id.toString())

            }

        _binding.autoCompleteCityBill.threshold = 0
        _binding.autoCompleteCityBill.setAdapter(countryAdapterautoCity)
    }


    private fun getGuestCityApi(stateId: String) {
        Utils.showLoadingProgress(this@ActBillingDetails)
        val stateListMap = HashMap<String, String>()
        stateListMap["state_id"] = stateId
        stateListMap["theme_id"] = getString(R.string.theme_id)
        lifecycleScope.launch {
            when (val response = ApiClient.getClient(this@ActBillingDetails)
                .setCityList(stateListMap)) {
                is NetworkResponse.Success -> {
                    Utils.dismissLoadingProgress()
                    val stateListResponse = response.body
                    when (response.body.status) {
                        1 -> {
                            cityList = stateListResponse.data!!

                            loadGuestCityAdapter(cityList)
                        }
                        0 -> {
                            Utils.errorAlert(
                                this@ActBillingDetails,
                                stateListResponse.message.toString()
                            )
                        }
                        9 -> {
                            Utils.errorAlert(
                                this@ActBillingDetails,
                                response.body.message.toString()
                            )
                            openActivity(ActWelCome::class.java)
                        }
                    }
                }

                is NetworkResponse.ApiError -> {
                    Utils.dismissLoadingProgress()
                    if (response.body.status == 9) {
                        Utils.setInvalidToekn(this@ActBillingDetails)
                    } else {
                        Utils.errorAlert(
                            this@ActBillingDetails,
                            response.body.message.toString()
                        )
                    }
                }

                is NetworkResponse.NetworkError -> {
                    Utils.dismissLoadingProgress()
                    Utils.errorAlert(
                        this@ActBillingDetails,
                        resources.getString(R.string.internet_connection_error)
                    )
                }

                is NetworkResponse.UnknownError -> {
                    Utils.dismissLoadingProgress()
                    Utils.errorAlert(
                        this@ActBillingDetails,
                        "Something went wrong"
                    )
                }
            }
        }
    }

    private fun loadGuestCityAdapter(cityList: ArrayList<CityListData>) {
        countryAdapterautoGuestCity =
            AutoCompleteCityGuestAdapter(
                this@ActBillingDetails,
                cityList
            ) {
                onItemClickCityGuest(it)
                Log.e("StateId", it.id.toString())

            }

        _binding.linearGuestBillingAddress.autoCompleteCity.threshold = 0
        _binding.linearGuestBillingAddress.autoCompleteCity.setAdapter(countryAdapterautoGuestCity)
    }


    private fun AddressListAdapter(addressList: ArrayList<AddressListData>) {
        _binding.rvGetAddress.layoutManager = manager
        billingaddressAdapter =
            BillingAddressAdapter(this@ActBillingDetails, addressList) { i: Int, s: String ->
                if (addressList[i].isSelect == true) {
                    SharePreference.setStringPref(
                        this@ActBillingDetails,
                        SharePreference.Billing_Company_Name,
                        addressList[i].companyName.toString()
                    )
                    SharePreference.setStringPref(
                        this@ActBillingDetails,
                        SharePreference.Billing_Address,
                        addressList[i].address.toString()
                    )
                    SharePreference.setStringPref(
                        this@ActBillingDetails,
                        SharePreference.Billing_Postecode,
                        addressList[i].postcode.toString()
                    )
                    SharePreference.setStringPref(
                        this@ActBillingDetails,
                        SharePreference.Billing_Country,
                        addressList[i].countryId.toString()
                    )
                    SharePreference.setStringPref(
                        this@ActBillingDetails,
                        SharePreference.Billing_State,
                        addressList[i].stateId.toString()
                    )
                    SharePreference.setStringPref(
                        this@ActBillingDetails,
                        SharePreference.Billing_City,
                        addressList[i].city.toString()
                    )
                    Log.e(
                        "HomeAddress",
                        SharePreference.getStringPref(
                            this@ActBillingDetails,
                            SharePreference.Billing_Company_Name
                        ).toString()
                    )
                    if (_binding.chbTermsCondition.isChecked) {
                        SharePreference.setStringPref(
                            this@ActBillingDetails,
                            SharePreference.Delivery_Address,
                            addressList[i].address.toString()
                        )
                        SharePreference.setStringPref(
                            this@ActBillingDetails,
                            SharePreference.Delivery_Postcode,
                            addressList[i].postcode.toString()
                        )
                        SharePreference.setStringPref(
                            this@ActBillingDetails,
                            SharePreference.Delivery_Country,
                            addressList[i].countryId.toString()
                        )
                        SharePreference.setStringPref(
                            this@ActBillingDetails,
                            SharePreference.Delivery_State,
                            addressList[i].stateId.toString()
                        )
                        SharePreference.setStringPref(
                            this@ActBillingDetails,
                            SharePreference.Delivery_City,
                            addressList[i].city.toString()
                        )
                        Log.e(
                            "CheckBox",
                            SharePreference.getStringPref(
                                this@ActBillingDetails,
                                SharePreference.Billing_Company_Name
                            ).toString()
                        )
                    }
                }
            }
        _binding.rvGetAddress.adapter = billingaddressAdapter
    }

    private fun callAddressList() {
        Utils.showLoadingProgress(this@ActBillingDetails)
        val categoriesProduct = HashMap<String, String>()
        categoriesProduct["user_id"] =
            SharePreference.getStringPref(this@ActBillingDetails, SharePreference.userId)
                .toString()
        categoriesProduct["theme_id"] = getString(R.string.theme_id)
        lifecycleScope.launch {
            when (val response = ApiClient.getClient(this@ActBillingDetails)
                .addressList(currentPage.toString(), categoriesProduct)) {
                is NetworkResponse.Success -> {
                    Utils.dismissLoadingProgress()
                    val addressListResponse = response.body.data
                    when (response.body.status) {
                        1 -> {
                            if ((response.body.data?.data?.size ?: 0) > 0) {
                                _binding.rvGetAddress.show()
                                this@ActBillingDetails.currentPage =
                                    addressListResponse?.currentPage!!.toInt()
                                this@ActBillingDetails.total_pages =
                                    addressListResponse.lastPage!!.toInt()
                                addressListResponse.data?.let {
                                    addressList.addAll(it)
                                }
                                if (currentPage >= total_pages) {
                                    isLastPage = true
                                }
                                isLoading = false
                            } else {
                                _binding.rvGetAddress.hide()
                            }
                            billingaddressAdapter.notifyDataSetChanged()
                        }
                        0 -> {
                            Utils.errorAlert(
                                this@ActBillingDetails,
                                addressListResponse?.message.toString()
                            )
                        }
                        9 -> {
                            Utils.errorAlert(
                                this@ActBillingDetails,
                                response.body.message.toString()
                            )
                            openActivity(ActWelCome::class.java)
                        }
                    }
                }

                is NetworkResponse.ApiError -> {
                    Utils.dismissLoadingProgress()
                    if (response.body.status == 9) {
                        Utils.setInvalidToekn(this@ActBillingDetails)
                    } else {
                        Utils.errorAlert(
                            this@ActBillingDetails,
                            response.body.message.toString()
                        )
                    }
                }

                is NetworkResponse.NetworkError -> {
                    Utils.dismissLoadingProgress()
                    Utils.errorAlert(
                        this@ActBillingDetails,
                        resources.getString(R.string.internet_connection_error)
                    )
                }

                is NetworkResponse.UnknownError -> {
                    Utils.dismissLoadingProgress()
                    Utils.errorAlert(
                        this@ActBillingDetails,
                        "Something went wrong"
                    )
                }
            }
        }
    }

    //Guest User
    override fun onItemClickGuest(item: CountryDataItem?) {
        _binding.linearGuestBillingAddress.autoCompleteState.text.clear()
        _binding.linearGuestBillingAddress.autoCompleteCity.text.clear()
        _binding.linearGuestBillingAddress.autoCompleteCountry.setText(item?.name)
        guestCountryID = item?.id.toString()
        guestStateId = item?.id.toString()
        getGuestStateApi(item?.id.toString())
    }

    override fun onItemClickStateGuest(item: StateListData?) {
        _binding.linearGuestBillingAddress.autoCompleteCity.text.clear()
        _binding.linearGuestBillingAddress.autoCompleteState.setText(item?.name)
        guestStateId = item?.id.toString()
        guestCityID = item?.id.toString()
        getGuestCityApi(item?.id.toString())
    }

    override fun onItemClickCityGuest(item: CityListData?) {
        _binding.linearGuestBillingAddress.autoCompleteCity.dismissDropDown()
        _binding.linearGuestBillingAddress.autoCompleteCity.setText(item?.name)
        guestCityID = item?.id.toString()
        _binding.linearGuestBillingAddress.edBillingPostCode.nextFocusForwardId
    }


    override fun onResume() {
        super.onResume()
        if (Utils.isLogin(this@ActBillingDetails)) {
            _binding.tvAddAddress.show()
            _binding.rvGetAddress.show()
            _binding.clGuestAddressLayout.hide()
            currentPage = 1
            addressList.clear()
            AddressListAdapter(addressList)
            callAddressList()
        } else {
            _binding.tvAddAddress.hide()
            _binding.rvGetAddress.hide()
            _binding.clGuestAddressLayout.show()
        }
    }


}