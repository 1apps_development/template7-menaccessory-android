package com.workdo.manaccessory.ui.authentication

import android.content.Intent
import android.net.Uri
import android.view.View
import androidx.lifecycle.lifecycleScope
import com.workdo.manaccessory.R
import com.workdo.manaccessory.api.ApiClient
import com.workdo.manaccessory.base.BaseActivity
import com.workdo.manaccessory.databinding.ActForgotPasswordBinding
import com.workdo.manaccessory.remote.NetworkResponse
import com.workdo.manaccessory.utils.SharePreference
import com.workdo.manaccessory.utils.Utils
import kotlinx.coroutines.launch

class ActForgotPassword : BaseActivity() {
    private lateinit var _binding: ActForgotPasswordBinding
    override fun setLayout(): View =_binding.root

    override fun initView() {
        _binding= ActForgotPasswordBinding.inflate(layoutInflater)
        init()
    }

    private fun init(){
        _binding.ivBack.setOnClickListener { finish() }
        _binding.btnSendCode.setOnClickListener {
            when {
                _binding.edEmailAddress.text?.isEmpty() == true -> {
                    Utils.errorAlert(this@ActForgotPassword, resources.getString(R.string.validation_email))
                }
                !Utils.isValidEmail(_binding.edEmailAddress.text.toString()) -> {
                    Utils.errorAlert(this@ActForgotPassword, resources.getString(R.string.validation_valid_email))
                }
                else -> {
                    val forgotPasswordOtpApi = HashMap<String, String>()
                    forgotPasswordOtpApi["email"] = _binding.edEmailAddress.text.toString()
                    forgotPasswordOtpApi["theme_id"]=getString(R.string.theme_id)
                    callForgotPasswordSendOpt(forgotPasswordOtpApi)
                }
            }
        }

        _binding.tvContactUs.setOnClickListener {
            val contactUs= SharePreference.getStringPref(this@ActForgotPassword, SharePreference.Contact_Us).toString()
            val uri: Uri = Uri.parse(contactUs)
            val intent = Intent(Intent.ACTION_VIEW, uri)
            intent.addCategory(Intent.CATEGORY_BROWSABLE)

            startActivity(intent)
        }
    }

    //TODO send otp api
    private fun callForgotPasswordSendOpt(forgotPasswordOtpApi: HashMap<String, String>) {
        Utils.showLoadingProgress(this@ActForgotPassword)
        lifecycleScope.launch {
            when (val response = ApiClient.getClient(this@ActForgotPassword).setforgotpasswordsendotp(forgotPasswordOtpApi)) {
                is NetworkResponse.Success -> {
                    Utils.dismissLoadingProgress()
                    val forgotPasswordOtpResponse = response.body.data
                    when (response.body.status) {
                        1 -> {
                            startActivity(
                                Intent(
                                    this@ActForgotPassword,
                                    ActAuthentication::class.java
                                ).putExtra(
                                    "email",
                                    _binding.edEmailAddress.text.toString()
                                )
                            )
                        }

                        0 -> {
                            Utils.errorAlert(this@ActForgotPassword, forgotPasswordOtpResponse?.message.toString())
                        }
                        9->{
                            Utils.errorAlert(this@ActForgotPassword, forgotPasswordOtpResponse?.message.toString())
                            openActivity(ActWelCome::class.java)
                        }
                    }
                }

                is NetworkResponse.ApiError -> {
                    Utils.dismissLoadingProgress()

                    if (response.body.status == 9) {
                        Utils.setInvalidToekn(this@ActForgotPassword)
                    }else{
                        Utils.errorAlert(
                            this@ActForgotPassword,
                            response.body.message.toString()
                        )
                    }
                }

                is NetworkResponse.NetworkError -> {
                    Utils.dismissLoadingProgress()
                    Utils.errorAlert(
                        this@ActForgotPassword,
                        resources.getString(R.string.internet_connection_error)
                    )
                }

                is NetworkResponse.UnknownError -> {
                    Utils.dismissLoadingProgress()
                    Utils.errorAlert(this@ActForgotPassword, "Something went wrong")
                }
            }
        }
    }
}