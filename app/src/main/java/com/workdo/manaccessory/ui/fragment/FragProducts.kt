package com.workdo.manaccessory.ui.fragment

import android.annotation.SuppressLint
import android.app.AlertDialog
import android.app.Dialog
import android.content.Intent
import android.util.Log
import android.view.View
import androidx.core.widget.NestedScrollView
import androidx.lifecycle.lifecycleScope
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.LinearSnapHelper
import com.bumptech.glide.Glide
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import com.workdo.manaccessory.R
import com.workdo.manaccessory.adapter.AllCategoryAdapter
import com.workdo.manaccessory.adapter.CategoriesListAdapter
import com.workdo.manaccessory.adapter.ProductCategoryAdapter
import com.workdo.manaccessory.api.ApiClient
import com.workdo.manaccessory.base.BaseFragment
import com.workdo.manaccessory.databinding.DlgConfirmBinding
import com.workdo.manaccessory.databinding.FragProductsBinding
import com.workdo.manaccessory.model.FeaturedProductsSub
import com.workdo.manaccessory.model.HomeCategoriesItem
import com.workdo.manaccessory.model.ProductListItem
import com.workdo.manaccessory.remote.NetworkResponse
import com.workdo.manaccessory.ui.activity.ActProductDetails
import com.workdo.manaccessory.ui.activity.ActShoppingCart
import com.workdo.manaccessory.ui.authentication.ActWelCome
import com.workdo.manaccessory.utils.Constants
import com.workdo.manaccessory.utils.ExtensionFunctions.hide
import com.workdo.manaccessory.utils.ExtensionFunctions.show
import com.workdo.manaccessory.utils.SharePreference
import com.workdo.manaccessory.utils.Utils
import kotlinx.coroutines.launch
import java.lang.reflect.Type

class FragProducts : BaseFragment<FragProductsBinding>() {

    private lateinit var _binding: FragProductsBinding
    private var categoriesList = ArrayList<HomeCategoriesItem>()
    private lateinit var allCategoriesAdapter: CategoriesListAdapter
    private var managerAllCategories: LinearLayoutManager? = null
    private var categoryProductList = ArrayList<FeaturedProductsSub>()
    private lateinit var categoriesProductAdapter: ProductCategoryAdapter
    internal var isLoading = false
    internal var isLastPage = false
    private var currentPage = 1
    private var total_pages: Int = 0
    var countItem = ""
    var count = 1
    private var currentPageCategorirs = 1
    private var total_pagesCategorirs: Int = 0
    internal var isLoadingCategorirs = false
    internal var isLastPageCategorirs = false
    var subID = ""

    private var manager: GridLayoutManager? = null


    override fun initView(view: View) {
        init()
    }

    override fun getBinding(): FragProductsBinding {
        _binding = FragProductsBinding.inflate(layoutInflater)
        return _binding
    }

    @SuppressLint("NotifyDataSetChanged")
    private fun init() {

        manager = GridLayoutManager(requireActivity(), 1, GridLayoutManager.HORIZONTAL, false)
        managerAllCategories =
            GridLayoutManager(requireActivity(), 2, GridLayoutManager.VERTICAL, false)
        val snapHelper=LinearSnapHelper()
        snapHelper.attachToRecyclerView(_binding.rvAllCategories)
        nestedScrollViewPagination()
        _binding.clcart.setOnClickListener {
            openActivity(ActShoppingCart::class.java)
        }

        if (SharePreference.getStringPref(requireActivity(), SharePreference.cartCount)
                .isNullOrEmpty()
        ) {
            SharePreference.setStringPref(requireActivity(), SharePreference.cartCount, "0")
        }
        productBannerApi()

    }

    private fun productBannerApi() {
        Utils.showLoadingProgress(requireActivity())
        val productBanner = HashMap<String, String>()
        productBanner["theme_id"] = getString(R.string.theme_id)
        lifecycleScope.launch {
            when (val response = ApiClient.getClient(requireActivity())
                .productBanner(productBanner)) {
                is NetworkResponse.Success -> {
                    Utils.dismissLoadingProgress()
                    val bannersResponse = response.body.data?.themJson?.productsHeaderBanner
                    when (response.body.status) {
                        1 -> {
                            Glide.with(requireActivity()).load(ApiClient.ImageURL.BASE_URL.plus(bannersResponse?.productsHeaderBanner)).into(_binding.ivProductBanner)
                            _binding.tvCatName.text = bannersResponse?.productsHeaderBannerTitleText.toString()
                        }
                        0 -> {

                        }
                    }
                }

                is NetworkResponse.ApiError -> {
                    Utils.dismissLoadingProgress()
                    if (response.body.status == 9) {
                        Utils.setInvalidToekn(requireActivity())
                    } else {
                        Utils.errorAlert(requireActivity(), response.body.message.toString())
                    }
                }

                is NetworkResponse.NetworkError -> {
                    Utils.dismissLoadingProgress()
                    Utils.errorAlert(
                        requireActivity(),
                        resources.getString(R.string.internet_connection_error)
                    )
                }

                is NetworkResponse.UnknownError -> {
                    Utils.dismissLoadingProgress()
                    Utils.errorAlert(
                        requireActivity(),
                        "Something went wrong"
                    )
                }
            }
        }
    }

    private fun nestedScrollViewPagination() {
        _binding.rvFeaturedProduct.isNestedScrollingEnabled = false

        _binding.scrollView.setOnScrollChangeListener { v: NestedScrollView?, scrollX: Int, scrollY: Int, oldScrollX: Int, oldScrollY: Int ->
            val nestedScrollView = checkNotNull(v) {
                return@setOnScrollChangeListener
            }

            val lastChild = nestedScrollView.getChildAt(nestedScrollView.childCount - 1)

            if (lastChild != null) {

                if ((scrollY >= (lastChild.measuredHeight - nestedScrollView.measuredHeight)) && scrollY > oldScrollY && !isLoading && !isLastPage) {

                    isLoading = true
                    currentPage++
                    callCategorysProduct(true)
                }
            }
        }

    }

    private fun callCategories() {
        Utils.showLoadingProgress(requireActivity())
        val categorieshashmap = HashMap<String, String>()
        categorieshashmap["theme_id"] = resources.getString(R.string.theme_id)
        lifecycleScope.launch {
            when (val response = ApiClient.getClient(requireActivity())
                .setHomeCategorys(currentPageCategorirs.toString(), categorieshashmap)) {
                is NetworkResponse.Success -> {
                    val categoriesResponse = response.body.data
                    when (response.body.status) {
                        1 -> {
                            if ((response.body.data?.data?.size ?: 0) > 0) {

                                _binding.rvAllCategories.show()
                                currentPageCategorirs =
                                    categoriesResponse?.currentPage ?: 0
                                total_pagesCategorirs = categoriesResponse?.lastPage ?: 0
                                if(currentPageCategorirs==1)
                                {
                                    categoriesList.add(HomeCategoriesItem(name = "All Product", id = 0))
                                }
                                categoriesResponse?.data?.let {
                                    categoriesList.addAll(it)
                                }
                                subID = categoriesList[0].id.toString()
                                currentPage = 1
                                isLastPage=false
                                isLoading=false
                                categoryProductList.clear()
                                callCategorysProduct(false)
                                if (currentPageCategorirs >= total_pagesCategorirs) {
                                    isLastPageCategorirs = true
                                }
                                isLoadingCategorirs = false
                            } else {
                                _binding.rvAllCategories.hide()
                            }
                            allCategoriesAdapter.notifyDataSetChanged()
                        }
                        0 -> {
                            Utils.dismissLoadingProgress()

                            Utils.errorAlert(
                                requireActivity(),
                                categoriesResponse?.message.toString()
                            )
                        }
                        9 -> {
                            Utils.dismissLoadingProgress()

                            Utils.errorAlert(
                                requireActivity(),
                                categoriesResponse?.message.toString()
                            )
                            openActivity(ActWelCome::class.java)
                        }
                    }
                }

                is NetworkResponse.ApiError -> {
                    Utils.dismissLoadingProgress()
                    if (response.body.status == 9) {
                        Utils.setInvalidToekn(requireActivity())
                    } else {
                        Utils.errorAlert(
                            requireActivity(),
                            response.body.message.toString()
                        )
                    }
                }

                is NetworkResponse.NetworkError -> {
                    Utils.dismissLoadingProgress()
                    Utils.errorAlert(
                        requireActivity(),
                        resources.getString(R.string.internet_connection_error)
                    )
                }

                is NetworkResponse.UnknownError -> {
                    Utils.dismissLoadingProgress()
                    Utils.errorAlert(
                        requireActivity(),
                        "Something went wrong"
                    )
                }
            }
        }
    }

    //TODO adapter set Categorirs
    private fun categoriesAdapter(homeCategoriesList: ArrayList<HomeCategoriesItem>) {
        _binding.rvAllCategories.layoutManager = manager

        allCategoriesAdapter =
            CategoriesListAdapter(requireActivity(), homeCategoriesList) { i: Int, s: String ->
                if (s == Constants.ItemClick) {
                    subID = homeCategoriesList[i].id.toString()
                    currentPage = 1
                    isLastPage=false
                    isLoading=false
                    categoryProductList.clear()
                    callCategorysProduct(true)
                }
            }
        _binding.rvAllCategories.adapter = allCategoriesAdapter
    }

    private fun callCategorysProduct(fromClick:Boolean) {
        if(fromClick)
        {
            Utils.showLoadingProgress(requireActivity())

        }
        val categoriesProduct = HashMap<String, String>()
        categoriesProduct["subcategory_id"] = ""
        categoriesProduct["maincategory_id"] = subID
        categoriesProduct["theme_id"] = getString(R.string.theme_id)
        lifecycleScope.launch {
            val response =
                if (SharePreference.getStringPref(requireActivity(), SharePreference.token)
                        ?.isEmpty() == true
                ) {
                    ApiClient.getClient(requireActivity())
                        .setCategorysProductGuest(currentPage.toString(), categoriesProduct)
                } else {
                    ApiClient.getClient(requireActivity())
                        .setCategorysProduct(currentPage.toString(), categoriesProduct)
                }
            when (response) {
                is NetworkResponse.Success -> {
                    val categoriesProductResponse = response.body.data
                    when (response.body.status) {
                        1 -> {


                            if ((response.body.data?.data?.size ?: 0) > 0) {


                                _binding.rvFeaturedProduct.show()
                                _binding.view.hide()



                                currentPage = categoriesProductResponse?.currentPage?.toInt() ?: 0
                                total_pages = categoriesProductResponse?.lastPage?.toInt() ?: 0
                                categoriesProductResponse?.data?.let {
                                    categoryProductList.addAll(it)
                                }

                                if (currentPage >= total_pages) {
                                    isLastPage = true
                                }
                                isLoading = false
                            } else {


                                if (categoryProductList.size == 0) {
                                    _binding.rvFeaturedProduct.hide()
                                    _binding.view.show()
                                }


                            }
                            categoriesProductAdapter.notifyDataSetChanged()
                        }

                        0 -> {
                            Utils.errorAlert(
                                requireActivity(),
                                categoriesProductResponse?.message.toString()
                            )
                        }
                        9 -> {
                            Utils.errorAlert(
                                requireActivity(),
                                categoriesProductResponse?.message.toString()
                            )
                            openActivity(ActWelCome::class.java)
                        }

                    }
                    if (categoriesProductResponse?.to == null) {

                        _binding.tvFoundproduct.text =
                            getString(R.string.found).plus(" 0").plus(" ")
                                .plus(getString(R.string.product))
                    } else {
                        _binding.tvFoundproduct.text = getString(R.string.found).plus(" ").plus(
                            categoriesProductResponse.total
                        ).plus(" ").plus(getString(R.string.product))
                    }
                    Utils.dismissLoadingProgress()

                }

                is NetworkResponse.ApiError -> {
                    Utils.dismissLoadingProgress()
                    if (response.body.status == 9) {
                        Utils.setInvalidToekn(requireActivity())
                    } else {
                        Utils.errorAlert(
                            requireActivity(),
                            response.body.message.toString()
                        )
                    }
                }

                is NetworkResponse.NetworkError -> {
                    Utils.dismissLoadingProgress()
                    Utils.errorAlert(
                        requireActivity(),
                        resources.getString(R.string.internet_connection_error)
                    )
                }

                is NetworkResponse.UnknownError -> {
                    Utils.dismissLoadingProgress()
                    Utils.errorAlert(
                        requireActivity(),
                        "Something went wrong"
                    )
                }
            }
        }
    }

    private fun setCount(count: Int?) {
        _binding.tvCount.text = count.toString()
    }

    //Adapter set Categories Product
    private fun categoriesProductsAdapter(featuredProductsSubList: ArrayList<FeaturedProductsSub>) {
        _binding.rvFeaturedProduct.layoutManager = managerAllCategories
        countItem = featuredProductsSubList.size.toString()
        categoriesProductAdapter =
            ProductCategoryAdapter(
                requireActivity(),
                featuredProductsSubList
            ) { i: Int, s: String ->
                if (s == Constants.FavClick) {
                    if (SharePreference.getBooleanPref(
                            requireActivity(),
                            SharePreference.isLogin
                        )
                    ) {
                        Log.e("FavClick", "FavClick")
                        if (featuredProductsSubList[i].inWhishlist == false) {
                            callWishlist(
                                "add", featuredProductsSubList[i].id, "CategoriesProduct", i
                            )
                        } else {
                            callWishlist(
                                "remove",
                                featuredProductsSubList[i].id,
                                "CategoriesProduct",
                                i
                            )
                        }
                    } else {
                        openActivity(ActWelCome::class.java)
                        activity?.finish()
                    }
                } else if (s == Constants.CartClick) {

                    if (!Utils.isLogin(requireActivity())) {
                        guestUserAddToCart(
                            featuredProductsSubList[i],
                            featuredProductsSubList[i].id
                        )
                    } else {
                        val addtocart = HashMap<String, String>()
                        addtocart["user_id"] =
                            SharePreference.getStringPref(requireActivity(), SharePreference.userId)
                                .toString()
                        addtocart["product_id"] = featuredProductsSubList[i].id.toString()
                        addtocart["variant_id"] =
                            featuredProductsSubList[i].defaultVariantId.toString()
                        addtocart["qty"] = "1"
                        addtocart["theme_id"] = getString(R.string.theme_id)
                        addtocartApi(
                            addtocart,
                            featuredProductsSubList[i].id,
                            featuredProductsSubList[i].defaultVariantId, i
                        )
                    }
                } else if (s == Constants.ItemClick) {
                    val intent = Intent(requireActivity(), ActProductDetails::class.java)
                    intent.putExtra(Constants.ProductId, featuredProductsSubList[i].id.toString())
                    startActivity(intent)
                }
            }
        _binding.rvFeaturedProduct.adapter = categoriesProductAdapter
    }


    private fun guestUserAddToCart(data: FeaturedProductsSub, id: Int?) {
        val cartData = cartData(data)
        Log.e("cartData2", Gson().toJson(cartData))
        val cartListData = SharePreference.getStringPref(
            requireActivity(),
            SharePreference.GuestCartList
        )
        val type: Type = object : TypeToken<ArrayList<ProductListItem>>() {}.type
        var cartList = Gson().fromJson<ArrayList<ProductListItem>>(cartListData, type)
        Log.e("cartListBefore2", Gson().toJson(cartList))
        if (cartList == null) {
            dlgConfirm(data.name.plus(" ").plus("add successfully."))

            val cartDataList = ArrayList<ProductListItem>()
            cartDataList.add(cartData)
            SharePreference.setStringPref(
                requireActivity(),
                SharePreference.GuestCartList,
                Gson().toJson(cartDataList)
            )
            SharePreference.setStringPref(
                requireActivity(),
                SharePreference.cartCount,
                cartDataList.size.toString()
            )
            _binding.tvCount.text = cartDataList.size.toString()

        } else if (isProductAlreadyAdded(cartList, data.id.toString())) {
            //Utils.errorAlert(requireActivity(), "item already added into cart")
            for (i in 0 until cartList.size) {
                if (cartList[i].productId?.toInt() == id) {
                    val position = cartList[i]
                    Log.e("SelectId", position.toString())
                    dlgAlreadyCart(data.message, data.id, data.defaultVariantId, i)
                }
            }
        } else {
            dlgConfirm(data.name.plus(" ").plus("add successfully."))
            cartList.add(cartData)
            SharePreference.setStringPref(
                requireActivity(),
                SharePreference.GuestCartList,
                Gson().toJson(cartList)
            )
            SharePreference.setStringPref(
                requireActivity(),
                SharePreference.cartCount,
                cartList.size.toString()
            )
            _binding.tvCount.text = cartList.size.toString()

        }
    }

    private fun cartData(data: FeaturedProductsSub): ProductListItem {
        return ProductListItem(
            data.coverImagePath.toString(),
            data.defaultVariantId.toString(),
            data.finalPrice.toString(),
            data.discountPrice.toString(),
            data.id.toString(),
            1,
            data.name.toString(),
            "",
            "",
            data.variantName.toString(),
            data.originalPrice.toString()
        )
    }

    private fun isProductAlreadyAdded(cartList: ArrayList<ProductListItem>, id: String): Boolean {
        return cartList.filter { it.productId == id.toString() }.size == 1
    }

    private fun addtocartApi(
        addtocart: HashMap<String, String>,
        id: Int?,
        defaultVariantId: Int?,
        i: Int?
    ) {
        Utils.showLoadingProgress(requireActivity())
        lifecycleScope.launch {
            when (val response = ApiClient.getClient(requireActivity())
                .addtocart(addtocart)) {
                is NetworkResponse.Success -> {
                    Utils.dismissLoadingProgress()
                    val addtocart = response.body.data
                    when (response.body.status) {
                        1 -> {
                            dlgConfirm(response.body.data?.message)
                            SharePreference.setStringPref(
                                requireActivity(),
                                SharePreference.cartCount,
                                addtocart?.count.toString()
                            )
                            setCount(addtocart?.count)
                        }
                        0 -> {
                            dlgAlreadyCart(response.body.data?.message, id, defaultVariantId, i)
                        }
                        9 -> {
                            Utils.errorAlert(
                                requireActivity(),
                                addtocart?.message.toString()
                            )
                            openActivity(ActWelCome::class.java)
                            requireActivity().finish()
                            requireActivity().finishAffinity()
                        }
                    }
                }

                is NetworkResponse.ApiError -> {
                    Utils.dismissLoadingProgress()

                    if (response.body.status == 9) {
                        Utils.setInvalidToekn(requireActivity())
                    } else {
                        Utils.errorAlert(
                            requireActivity(),
                            response.body.message.toString()
                        )
                    }
                }

                is NetworkResponse.NetworkError -> {
                    Utils.dismissLoadingProgress()
                    Utils.errorAlert(
                        requireActivity(),
                        resources.getString(R.string.internet_connection_error)
                    )
                }

                is NetworkResponse.UnknownError -> {
                    Utils.dismissLoadingProgress()
                    Utils.errorAlert(
                        requireActivity(),
                        "Something went wrong"
                    )
                }
            }
        }
    }


    private fun dlgConfirm(message: String?) {
        val confirmDialogBinding = DlgConfirmBinding.inflate(layoutInflater)
        val dialog = Dialog(requireActivity())
        dialog.setContentView(confirmDialogBinding.root)
        confirmDialogBinding.tvProductDesc.text = message
        confirmDialogBinding.tvCheckOut.setOnClickListener {
            dialog.dismiss()
            openActivity(ActShoppingCart::class.java)
        }
        confirmDialogBinding.tvContinueShopping.setOnClickListener {
            dialog.dismiss()
        }

        confirmDialogBinding.tvCancel.setOnClickListener {
            dialog.dismiss()
        }
        dialog.show()
    }

    private fun dlgAlreadyCart(message: String?, id: Int?, defaultVariantId: Int?, i: Int?) {
        val builder = AlertDialog.Builder(requireActivity())
        builder.setTitle(R.string.app_name)
        builder.setMessage(R.string.product_already_again)
        builder.setPositiveButton(getString(R.string.yes)) { dialogInterface, which ->
            dialogInterface.dismiss()
            val cartqty = java.util.HashMap<String, String>()

            if (Utils.isLogin(requireActivity())) {
                count = 1
                cartqty["product_id"] = id.toString()
                cartqty["user_id"] =
                    SharePreference.getStringPref(
                        requireActivity(),
                        SharePreference.userId
                    ).toString()
                cartqty["variant_id"] = defaultVariantId.toString()
                cartqty["quantity_type"] = "increase"
                cartqty["theme_id"] = getString(R.string.theme_id)
            } else {
                val cartListData = SharePreference.getStringPref(
                    requireActivity(),
                    SharePreference.GuestCartList
                )
                val type: Type = object : TypeToken<java.util.ArrayList<ProductListItem>>() {}.type

                var cartList =
                    Gson().fromJson<java.util.ArrayList<ProductListItem>>(cartListData, type)
                var count = cartList[i!!].qty?.toInt()!!
                Log.e("Count", count.toString())
                cartList[i].qty = count.plus(1)

                manageOfflineData(cartList)
            }
        }
        builder.setNegativeButton(getString(R.string.no)) { dialogInterface, which ->
            dialogInterface.dismiss()
            builder.setCancelable(true)
        }
        val alertDialog: AlertDialog = builder.create()
        alertDialog.setCancelable(false)
        alertDialog.show()
    }

    private fun cartQtyApi(
        cartqty: java.util.HashMap<String, String>,
        type: String
    ) {
        Utils.showLoadingProgress(requireActivity())
        lifecycleScope.launch {
            when (val response = ApiClient.getClient(requireActivity())
                .cartQty(cartqty)) {
                is NetworkResponse.Success -> {
                    Utils.dismissLoadingProgress()
                    val cartQtyResponse = response.body.data
                    SharePreference.setStringPref(
                        requireActivity(),
                        SharePreference.cartCount,
                        response.body.count.toString()
                    )

                    when (response.body.status) {
                        1 -> {
                            Utils.dismissLoadingProgress()
                        }
                        0 -> {
                            Utils.errorAlert(
                                requireActivity(),
                                cartQtyResponse?.message.toString()
                            )
                        }
                        9 -> {
                            Utils.errorAlert(
                                requireActivity(),
                                cartQtyResponse?.message.toString()
                            )
                            openActivity(ActWelCome::class.java)
                        }
                    }
                }

                is NetworkResponse.ApiError -> {
                    Utils.dismissLoadingProgress()

                    if (response.body.status == 9) {
                        Utils.setInvalidToekn(requireActivity())
                    } else {
                        Utils.errorAlert(
                            requireActivity(), response.body.message.toString()
                        )
                    }
                }

                is NetworkResponse.NetworkError -> {
                    Utils.dismissLoadingProgress()
                    Utils.errorAlert(
                        requireActivity(),
                        resources.getString(R.string.internet_connection_error)
                    )
                }

                is NetworkResponse.UnknownError -> {
                    Utils.dismissLoadingProgress()
                    Utils.errorAlert(requireActivity(), "Something went wrong")
                }
            }
        }
    }

    private fun manageOfflineData(cartItemList: java.util.ArrayList<ProductListItem>) {
        SharePreference.setStringPref(
            requireActivity(),
            SharePreference.GuestCartList,
            Gson().toJson(cartItemList)
        )
        SharePreference.setStringPref(
            requireActivity(),
            SharePreference.cartCount,
            cartItemList.size.toString()
        )
    }


    private fun callWishlist(type: String, id: Int?, itemType: String, position: Int) {
        Utils.showLoadingProgress(requireActivity())
        val wishlist = HashMap<String, String>()
        wishlist["user_id"] =
            SharePreference.getStringPref(requireActivity(), SharePreference.userId).toString()
        wishlist["product_id"] = id.toString()
        wishlist["wishlist_type"] = type
        wishlist["theme_id"] = getString(R.string.theme_id)
        lifecycleScope.launch {
            when (val response = ApiClient.getClient(requireActivity())
                .setWishlist(wishlist)) {
                is NetworkResponse.Success -> {
                    Utils.dismissLoadingProgress()
                    val wishlistResponse = response.body.data
                    when (response.body.status) {
                        1 -> {
                            if (type == "add") {
                                if (itemType == "CategoriesProduct") {
                                    categoryProductList[position].inWhishlist = true
                                    categoriesProductAdapter.notifyItemChanged(position)
                                }
                            } else {
                                if (itemType == "CategoriesProduct") {
                                    categoryProductList[position].inWhishlist = false
                                    categoriesProductAdapter.notifyItemChanged(position)
                                }
                            }
                        }
                        0 -> {
                            Utils.errorAlert(
                                requireActivity(),
                                wishlistResponse?.message.toString()
                            )
                        }
                        9 -> {
                            Utils.errorAlert(
                                requireActivity(),
                                wishlistResponse?.message.toString()
                            )
                            openActivity(ActWelCome::class.java)
                        }
                    }
                }

                is NetworkResponse.ApiError -> {
                    Utils.dismissLoadingProgress()

                    if (response.body.status == 9) {
                        Utils.setInvalidToekn(requireActivity())
                    } else {
                        Utils.errorAlert(
                            requireActivity(),
                            response.body.message.toString()
                        )
                    }
                }

                is NetworkResponse.NetworkError -> {
                    Utils.dismissLoadingProgress()
                    Utils.errorAlert(
                        requireActivity(),
                        resources.getString(R.string.internet_connection_error)
                    )
                }

                is NetworkResponse.UnknownError -> {
                    Utils.dismissLoadingProgress()
                    Utils.errorAlert(
                        requireActivity(),
                        "Something went wrong"
                    )
                }
            }
        }
    }

    override fun onResume() {
        super.onResume()
        currentPageCategorirs = 1
        currentPage=1
        isLastPage=false
        isLoading=false
        categoriesList.clear()
        categoryProductList.clear()

        categoriesAdapter(categoriesList)

        callCategories()
        categoriesProductsAdapter(categoryProductList)

        _binding.tvCount.text =
            SharePreference.getStringPref(requireActivity(), SharePreference.cartCount)


    }
}