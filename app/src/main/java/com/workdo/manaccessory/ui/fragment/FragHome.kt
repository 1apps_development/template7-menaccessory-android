package com.workdo.manaccessory.ui.fragment

import android.app.AlertDialog
import android.app.Dialog
import android.content.Intent
import android.util.Log
import android.view.View
import androidx.lifecycle.lifecycleScope
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import com.workdo.manaccessory.R
import com.workdo.manaccessory.adapter.*
import com.workdo.manaccessory.api.ApiClient
import com.workdo.manaccessory.base.BaseFragment
import com.workdo.manaccessory.databinding.DlgConfirmBinding
import com.workdo.manaccessory.databinding.FragHomeBinding
import com.workdo.manaccessory.model.*
import com.workdo.manaccessory.remote.NetworkResponse
import com.workdo.manaccessory.ui.activity.ActProductDetails
import com.workdo.manaccessory.ui.activity.ActShoppingCart
import com.workdo.manaccessory.ui.authentication.ActWelCome
import com.workdo.manaccessory.ui.option.ActMenu
import com.workdo.manaccessory.ui.option.ActSearch
import com.workdo.manaccessory.utils.Constants
import com.workdo.manaccessory.utils.ExtensionFunctions.hide
import com.workdo.manaccessory.utils.ExtensionFunctions.show
import com.workdo.manaccessory.utils.PaginationScrollListener
import com.workdo.manaccessory.utils.SharePreference
import com.workdo.manaccessory.utils.Utils
import kotlinx.coroutines.async
import kotlinx.coroutines.coroutineScope
import kotlinx.coroutines.launch
import java.lang.reflect.Type

class FragHome : BaseFragment<FragHomeBinding>() {

    private lateinit var _binding: FragHomeBinding
    internal var isLoading = false
    internal var isLastPage = false
    private var currentPage = 1
    private var total_pages: Int = 0


    private var isLoadingBestSeller = false
    private var isLastPageBestSeller = false
    private var currentPageBestSeller = 1
    private var total_pagesBestSeller: Int = 0

    private var isLoadingTrending = false
    private var isLastPageTrending = false
    private var currentPageTrending = 1
    private var total_pagesTrending: Int = 0

    var maincategory_id: String = ""
    var trendingcategory_id: String = ""
    var searched = ""
    var currency = ""
    var currency_name = ""
    var count = 1

    var searchHint = ""


    private var featuredProductsList = ArrayList<HomeCategoriesItem>()
    private lateinit var categoriesAdapter: CategoriesAdapter
    private var featuredProductsSubList = ArrayList<FeaturedProductsSub>()
    private var manager: LinearLayoutManager? = null

    private lateinit var categoriesProductAdapter: FeaturedAdapter

    private var trendingCategoriesManager: LinearLayoutManager? = null
    private var trendingCategoriesList = ArrayList<TrandingCategoriesItem>()
    private lateinit var trendingCategoriesAdapter: TrendingCategoriesAdapter

    private var trendingProductsSubList = ArrayList<FeaturedProductsSub>()
    private lateinit var trendingProductAdapter: TrendingProductAdapter

    private var bestsellersList = ArrayList<FeaturedProductsSub>()
    private lateinit var bestSellersAdapter: BestsellerAdapter
    private var managerBestsellers: GridLayoutManager? = null

    private var trendingCategoryProductList = ArrayList<FeaturedProductsSub>()
    private lateinit var trendingCategoryProductAdapter: TrendingCategoryProductAdapter

    override fun initView(view: View) {
        init()
    }

    override fun getBinding(): FragHomeBinding {
        _binding = FragHomeBinding.inflate(layoutInflater)
        return _binding
    }

    private fun init() {

        _binding.clSearch.setOnClickListener { openActivity(ActSearch::class.java) }
        _binding.edSearch.setOnClickListener { openActivity(ActSearch::class.java) }

        manager = LinearLayoutManager(requireActivity(), RecyclerView.HORIZONTAL, false)
        trendingCategoriesManager =
            LinearLayoutManager(requireActivity(), RecyclerView.HORIZONTAL, false)
        managerBestsellers =
            GridLayoutManager(requireActivity(), 1, GridLayoutManager.VERTICAL, false)
        featuredProductsAdapter()
        pagination()
        trendingProductCateAdapter()


        initClickListeners()


    }

    private fun initClickListeners() {
        _binding.ivMenu.setOnClickListener {
            openActivity(ActMenu::class.java)
        }

        _binding.clcart.setOnClickListener {
            openActivity(ActShoppingCart::class.java)
        }
    }

    private fun pagination() {
        val paginationListener = object : PaginationScrollListener(manager) {
            override fun isLastPage(): Boolean {
                return isLastPage
            }

            override fun isLoading(): Boolean {
                return isLoading
            }

            override fun loadMoreItems() {
                isLoading = true
                currentPage++
                callCategoryProduct()
            }

        }

        _binding.rvFeaturedProduct.addOnScrollListener(paginationListener)
    }

    private suspend fun callCurrencyApi() {
        val currencyRequest = HashMap<String, String>()
        currencyRequest["theme_id"] = getString(R.string.theme_id)
        when (val response = ApiClient.getClient(requireActivity()).setcurrency(currencyRequest)) {
            is NetworkResponse.Success -> {
                Utils.dismissLoadingProgress()
                val currencyResponse = response.body.data
                when (response.body.status) {
                    1 -> {

                        SharePreference.setStringPref(requireActivity(),
                            SharePreference.currency,
                            currencyResponse?.currency.toString())
                        SharePreference.setStringPref(requireActivity(),
                            SharePreference.currency_name,
                            currencyResponse?.currency_name.toString())
                        Log.e("currency Call", "first Call")

                    }
                    0 -> {
                        Utils.errorAlert(requireActivity(), currencyResponse?.message.toString())
                    }
                    9 -> {
                        startActivity(Intent(requireActivity(), ActWelCome::class.java))
                        // openActivity(ActWelCome::class.java)
                        requireActivity().finish()
                        requireActivity().finishAffinity()
                    }
                }
            }

            is NetworkResponse.ApiError -> {
                if (response.body.status == 9) {
                    Utils.setInvalidToekn(requireActivity())
                } else {
                    Utils.errorAlert(requireActivity(), response.body.message.toString())
                }
            }

            is NetworkResponse.NetworkError -> {
                Utils.errorAlert(requireActivity(),
                    resources.getString(R.string.internet_connection_error))
            }

            is NetworkResponse.UnknownError -> {
                Utils.errorAlert(requireActivity(), "Something went wrong")
            }
        }
    }

    private fun callHeaderContentApi() {
        Utils.showLoadingProgress(requireActivity())
        val hashmap = HashMap<String, String>()
        hashmap["theme_id"] = resources.getString(R.string.theme_id)

        lifecycleScope.launch {
            when (val response = ApiClient.getClient(requireActivity())
                .setLandingPage(hashmap)) {
                is NetworkResponse.Success -> {
                    Utils.dismissLoadingProgress()
                    val headerContenResponse = response.body.data
                    when (response.body.status) {
                        1 -> {
                            //homepage - header
                            _binding.tvMenFashionTitle.text =
                                headerContenResponse?.themJson?.homepageHeader?.homepageHeaderTitleText1
                            _binding.tvDescription.text =
                                headerContenResponse?.themJson?.homepageHeader?.homepageHeaderSubText1
                            /*  _binding.btnShowMore.text =
                                  headerContenResponse?.themJson?.homepageHeader?.homepageHeaderBtn1*/

                            //homepage-feature-products
                            _binding.tvMenTitle.text =
                                headerContenResponse?.themJson?.homepageFeatureProducts?.homepageFeatureProductsHeading
                            _binding.tvFeaturedProductTitle.text =
                                headerContenResponse?.themJson?.homepageFeatureProducts?.homepageFeatureProductsTitleText

                            //homepage-products
                            _binding.tvMenTitle2.text =
                                headerContenResponse?.themJson?.homepageProducts?.homepageProductsHeading
                            _binding.tvSkinCareTitle.text =
                                headerContenResponse?.themJson?.homepageProducts?.homepageProductsTitleText

                            //homepage-trending-products
                            _binding.tvMenTitle3.text =
                                headerContenResponse?.themJson?.homepageTrendingProducts?.homepageTrendingProductsHeading
                            _binding.tvTrendingTitle.text =
                                headerContenResponse?.themJson?.homepageTrendingProducts?.homepageTrendingProductsTitleText

                            //homepage-newsletter
                            _binding.tvSubscribeTitle.text =
                                headerContenResponse?.themJson?.homepageNewsletter?.homepageNewsletterTitleText
                            _binding.cbSubscription.text =
                                headerContenResponse?.themJson?.homepageNewsletter?.homepageNewsletterSubText

                        }
                        0 -> {
                            Utils.errorAlert(
                                requireActivity(),
                                headerContenResponse?.message.toString()
                            )
                        }
                        9 -> {
                            Utils.errorAlert(
                                requireActivity(),
                                headerContenResponse?.message.toString()
                            )
                            startActivity(Intent(requireActivity(), ActWelCome::class.java))
                            requireActivity().finish()
                            requireActivity().finishAffinity()
                        }
                    }
                }

                is NetworkResponse.ApiError -> {
                    Utils.dismissLoadingProgress()

                    if (response.body.status == 9) {
                        Utils.setInvalidToekn(requireActivity())
                    } else {
                        Utils.errorAlert(
                            requireActivity(),
                            response.body.message.toString()
                        )
                    }
                }

                is NetworkResponse.NetworkError -> {
                    Utils.dismissLoadingProgress()
                    Utils.errorAlert(
                        requireActivity(),
                        resources.getString(R.string.internet_connection_error)
                    )
                }

                is NetworkResponse.UnknownError -> {
                    Utils.dismissLoadingProgress()
                    Utils.errorAlert(
                        requireActivity(),
                        "Something went wrong"
                    )
                }
            }
        }

    }

    private fun callFeaturedProduct() {
        Utils.showLoadingProgress(requireActivity())
        val homeCategoryRequest = HashMap<String, String>()
        homeCategoryRequest["theme_id"] = getString(R.string.theme_id)

        lifecycleScope.launch {
            when (val response = ApiClient.getClient(requireActivity())
                .setHomeCategorys("1", homeCategoryRequest)) {
                is NetworkResponse.Success -> {
                    Utils.dismissLoadingProgress()
                    val featuredProductResponse = response.body.data
                    when (response.body.status) {
                        1 -> {
                            /* featuredProductsList.add(
                                 HomeCategoriesItem(
                                     true,
                                     "",
                                     "",
                                     "",
                                     0,
                                     "",
                                     "",
                                     "All Products",
                                     "",
                                     "",
                                     0,
                                     0,
                                     0


                                 )
                             )*/
                            response.body.data?.data?.let { featuredProductsList.addAll(it) }

                            if (featuredProductsList.size > 0) {
                                featuredProductsList[0].isSelect = true
                            }

                            if (featuredProductsList.size == 0) {
                                _binding.rvFeaturedCategories.hide()

                            } else {
                                _binding.rvFeaturedCategories.show()

                            }
                            categoriesAdapter.notifyDataSetChanged()
                            Utils.dismissLoadingProgress()
                        }
                        0 -> {
                            Utils.errorAlert(
                                requireActivity(),
                                featuredProductResponse?.message.toString()
                            )
                        }
                        9 -> {
                            Utils.errorAlert(
                                requireActivity(),
                                featuredProductResponse?.message.toString()
                            )
                            startActivity(Intent(requireActivity(), ActWelCome::class.java))
                            requireActivity().finish()
                            requireActivity().finishAffinity()
                        }

                    }
                }

                is NetworkResponse.ApiError -> {
                    Utils.dismissLoadingProgress()

                    if (response.body.status == 9) {
                        Utils.setInvalidToekn(requireActivity())
                    } else {
                        Utils.errorAlert(
                            requireActivity(),
                            response.body.message.toString()
                        )
                    }
                }

                is NetworkResponse.NetworkError -> {
                    Utils.dismissLoadingProgress()
                    Utils.errorAlert(
                        requireActivity(),
                        resources.getString(R.string.internet_connection_error)
                    )
                }

                is NetworkResponse.UnknownError -> {
                    Utils.dismissLoadingProgress()
                    Utils.errorAlert(requireActivity(), "Something went wrong")
                }
            }
        }
    }

    private fun featuredProductsAdapter() {
        _binding.rvFeaturedCategories.layoutManager =
            GridLayoutManager(requireActivity(), 2, GridLayoutManager.HORIZONTAL, false)
        categoriesAdapter =
            CategoriesAdapter(
                requireActivity(),
                featuredProductsList
            ) { id: String, name: String ->

                maincategory_id = id
                featuredProductsSubList.clear()
                currentPage = 1
                total_pages = 0
                isLastPage = false
                isLoading = false
                callCategoryProduct()
            }
        _binding.rvFeaturedCategories.adapter = categoriesAdapter
    }

    //    Todo Best Seller Api
    private fun callBestseller() {
        Utils.showLoadingProgress(requireActivity())
        val bestSellerRequest = HashMap<String, String>()
        bestSellerRequest["theme_id"] = getString(R.string.theme_id)

        lifecycleScope.launch {

            val response =
                if (SharePreference.getStringPref(requireActivity(), SharePreference.token)
                        ?.isEmpty() == true
                ) {
                    ApiClient.getClient(requireActivity())
                        .setBestSellerGuest(bestSellerRequest)
                } else {
                    ApiClient.getClient(requireActivity())
                        .setBestSeller(bestSellerRequest)
                }

            when (response) {
                is NetworkResponse.Success -> {
                    Utils.dismissLoadingProgress()
                    val bestsellerResponse = response.body.data
                    when (response.body.status) {
                        1 -> {
                            if ((response.body.data?.data?.size ?: 0) > 0) {
                                if (Utils.isLogin(requireActivity())) {
                                    SharePreference.setStringPref(
                                        requireActivity(),
                                        SharePreference.cartCount,
                                        response.body.count.toString()
                                    )
                                    _binding.tvCount.text = response.body.count.toString()
                                }

                                _binding.rvBestsellers.show()

                                this@FragHome.currentPageBestSeller =
                                    bestsellerResponse?.currentPage!!.toInt()
                                this@FragHome.total_pagesBestSeller =
                                    bestsellerResponse.lastPage!!.toInt()
                                bestsellerResponse.data?.let {
                                    bestsellersList.addAll(it)
                                }


                                if (currentPageBestSeller >= total_pagesBestSeller) {
                                    isLastPageBestSeller = true
                                }
                                isLoadingBestSeller = false
                            } else {
                                _binding.rvBestsellers.hide()
                            }
                            bestSellersAdapter.notifyDataSetChanged()
                        }
                        0 -> {
                            Utils.errorAlert(
                                requireActivity(),
                                bestsellerResponse?.message.toString()
                            )
                        }
                        9 -> {
                            Utils.errorAlert(
                                requireActivity(),
                                bestsellerResponse?.data?.get(0)?.message.toString()
                            )
                            Utils.setInvalidToekn(requireActivity())
                        }
                    }
                }

                is NetworkResponse.ApiError -> {
                    Utils.dismissLoadingProgress()

                    if (response.body.status == 9) {
                        Utils.setInvalidToekn(requireActivity())
                    } else {
                        Utils.errorAlert(
                            requireActivity(),
                            response.body.message.toString()
                        )
                    }
                }

                is NetworkResponse.NetworkError -> {
                    Utils.dismissLoadingProgress()
                    Utils.errorAlert(
                        requireActivity(),
                        resources.getString(R.string.internet_connection_error)
                    )
                }

                is NetworkResponse.UnknownError -> {
                    Utils.dismissLoadingProgress()
                    Utils.errorAlert(
                        requireActivity(),
                        getString(R.string.something_went_wrong)
                    )
                }
                else -> {}
            }
        }
    }

    // Todo Adapter set Best seller
    private fun bestsellerAdapter(bestsellersList: ArrayList<FeaturedProductsSub>) {
        _binding.rvBestsellers.layoutManager = managerBestsellers
        bestSellersAdapter =
            BestsellerAdapter(requireActivity(), bestsellersList) { i: Int, s: String ->
                if (s == Constants.FavClick) {
                    if (SharePreference.getBooleanPref(
                            requireActivity(),
                            SharePreference.isLogin
                        )
                    ) {
                        Log.e("FavClick", "FavClick")
                        if (bestsellersList[i].inWhishlist == false) {
                            callWishlist("add", bestsellersList[i].id, "BestsellersList", i)
                        } else {
                            callWishlist("remove", bestsellersList[i].id, "BestsellersList", i)
                        }
                    } else {
                        openActivity(ActWelCome::class.java)
                        activity?.finish()
                    }
                } else if (s == Constants.CartClick) {
                    val data = bestsellersList[i]
                    if (!Utils.isLogin(requireActivity())) {
                        guestUserAddToCart(data, bestsellersList[i].id)
                    } else {
                        val addtocart = HashMap<String, String>()
                        addtocart["user_id"] =
                            SharePreference.getStringPref(requireActivity(), SharePreference.userId)
                                .toString()
                        addtocart["product_id"] = bestsellersList[i].id.toString()
                        addtocart["variant_id"] = bestsellersList[i].defaultVariantId.toString()
                        addtocart["qty"] = "1"
                        addtocartApi(
                            addtocart,
                            bestsellersList[i].id,
                            bestsellersList[i].defaultVariantId, i
                        )
                    }
                } else if (s == Constants.ItemClick) {
                    val intent = Intent(requireActivity(), ActProductDetails::class.java)
                    intent.putExtra(Constants.ProductId, bestsellersList[i].id.toString())
                    startActivity(intent)
                }
            }
        _binding.rvBestsellers.adapter = bestSellersAdapter
    }


    private fun callCategoryProduct() {
        Utils.showLoadingProgress(requireActivity())
        val categoriesProduct = HashMap<String, String>()
        categoriesProduct["subcategory_id"] = ""
        categoriesProduct["maincategory_id"] = maincategory_id
        categoriesProduct["theme_id"] = getString(R.string.theme_id)
        lifecycleScope.launch {
            val response =
                if (SharePreference.getStringPref(requireActivity(), SharePreference.token)
                        ?.isEmpty() == true
                ) {
                    ApiClient.getClient(requireActivity())
                        .setCategorysProductGuest(currentPage.toString(), categoriesProduct)
                } else {
                    ApiClient.getClient(requireActivity())
                        .setCategorysProduct(currentPage.toString(), categoriesProduct)
                }

            when (response) {
                is NetworkResponse.Success -> {
                    Utils.dismissLoadingProgress()
                    val categoriesProductResponse = response.body.data
                    when (response.body.status) {
                        1 -> {
                            if ((response.body.data?.data?.size ?: 0) > 0) {
                                _binding.rvFeaturedProduct.show()
                                this@FragHome.currentPage =
                                    categoriesProductResponse?.currentPage!!.toInt()
                                this@FragHome.total_pages =
                                    categoriesProductResponse.lastPage!!.toInt()
                                categoriesProductResponse.data?.let {
                                    featuredProductsSubList.addAll(it)
                                }
                                if (currentPage >= total_pages) {
                                    isLastPage = true
                                }
                                isLoading = false
                            } else {
                                _binding.rvFeaturedProduct.hide()
                            }
                            categoriesProductAdapter.notifyDataSetChanged()
                        }
                        0 -> {
                            Utils.errorAlert(
                                requireActivity(),
                                categoriesProductResponse?.message.toString()
                            )
                        }
                        9 -> {
                            Utils.errorAlert(
                                requireActivity(),
                                categoriesProductResponse?.message.toString()
                            )
                            startActivity(Intent(requireActivity(), ActWelCome::class.java))
                            requireActivity().finish()
                            requireActivity().finishAffinity()
                        }
                    }
                }

                is NetworkResponse.ApiError -> {
                    Utils.dismissLoadingProgress()

                    if (response.body.status == 9) {
                        Utils.setInvalidToekn(requireActivity())
                    } else {
                        Utils.errorAlert(
                            requireActivity(),
                            response.body.message.toString()
                        )
                    }
                }

                is NetworkResponse.NetworkError -> {
                    Utils.dismissLoadingProgress()
                    Utils.errorAlert(
                        requireActivity(),
                        resources.getString(R.string.internet_connection_error)
                    )
                }

                is NetworkResponse.UnknownError -> {
                    Utils.dismissLoadingProgress()
                    Utils.errorAlert(
                        requireActivity(),
                        "Something went wrong"
                    )
                }
            }
        }
    }

    //Adapter set Categories Product
    private fun categoriesProductsAdapter(featuredProductsSubList: ArrayList<FeaturedProductsSub>) {
        _binding.rvFeaturedProduct.layoutManager = manager
        categoriesProductAdapter =
            FeaturedAdapter(requireActivity(), featuredProductsSubList) { i: Int, s: String ->
                if (s == Constants.FavClick) {
                    if (SharePreference.getBooleanPref(
                            requireActivity(),
                            SharePreference.isLogin
                        )
                    ) {
                        Log.e("FavClick", "FavClick")
                        if (featuredProductsSubList[i].inWhishlist == false) {
                            callWishlist(
                                "add",
                                featuredProductsSubList[i].id,
                                "CategoriesProduct",
                                i
                            )
                        } else {
                            callWishlist(
                                "remove",
                                featuredProductsSubList[i].id,
                                "CategoriesProduct", i
                            )
                        }
                    } else {
                        openActivity(ActWelCome::class.java)
                        activity?.finish()
                    }
                } else if (s == Constants.CartClick) {
                    val data = featuredProductsSubList[i]
                    if (!Utils.isLogin(requireActivity())) {
                        guestUserAddToCart(data, featuredProductsSubList[i].id)
                    } else {
                        val addtocart = HashMap<String, String>()
                        addtocart["user_id"] =
                            SharePreference.getStringPref(requireActivity(), SharePreference.userId)
                                .toString()
                        addtocart["product_id"] = featuredProductsSubList[i].id.toString()
                        addtocart["variant_id"] =
                            featuredProductsSubList[i].defaultVariantId.toString()
                        addtocart["qty"] = "1"
                        addtocart["theme_id"] = getString(R.string.theme_id)
                        addtocartApi(
                            addtocart,
                            featuredProductsSubList[i].id,
                            featuredProductsSubList[i].defaultVariantId, i
                        )
                    }
                } else if (s == Constants.ItemClick) {
                    val intent = Intent(requireActivity(), ActProductDetails::class.java)
                    intent.putExtra(Constants.ProductId, featuredProductsSubList[i].id.toString())
                    startActivity(intent)
                }
            }
        _binding.rvFeaturedProduct.adapter = categoriesProductAdapter
    }

    private fun callTrendingCategories() {
        Utils.showLoadingProgress(requireActivity())
        val hashmap = HashMap<String, String>()
        hashmap["theme_id"] = getString(R.string.theme_id)

        lifecycleScope.launch {
            when (val response = ApiClient.getClient(requireActivity())
                .setTrandingCategory(hashmap)) {
                is NetworkResponse.Success -> {
                    Utils.dismissLoadingProgress()
                    val trendingcategorieResponse = response.body.data
                    when (response.body.status) {
                        1 -> {

                            response.body.data?.let { trendingCategoriesList.addAll(it) }

                            if (trendingCategoriesList.size == 0) {
                                _binding.rvTrendingCategories.hide()

                            } else {
                                _binding.rvTrendingCategories.show()

                            }
                            trendingCategoriesAdapter.notifyDataSetChanged()

                            trendingcategory_id = trendingCategoriesList[0].id.toString()
                            currentPageTrending = 1
                            isLoadingTrending = false
                            isLastPageTrending = false
                            callTrendingCategoryProduct(trendingCategoriesList[0].id.toString())

                            Utils.dismissLoadingProgress()
                        }
                        0 -> {
                            Utils.errorAlert(
                                requireActivity(),
                                trendingcategorieResponse?.get(0)?.message.toString()
                            )
                        }
                        9 -> {
                            Utils.errorAlert(
                                requireActivity(),
                                trendingcategorieResponse?.get(0)?.message.toString()
                            )
                            startActivity(Intent(requireActivity(), ActWelCome::class.java))
                            requireActivity().finish()
                            requireActivity().finishAffinity()
                        }

                    }
                }

                is NetworkResponse.ApiError -> {
                    Utils.dismissLoadingProgress()

                    if (response.body.status == 9) {
                        Utils.setInvalidToekn(requireActivity())
                    } else {
                        Utils.errorAlert(
                            requireActivity(),
                            response.body.message.toString()
                        )
                    }
                }

                is NetworkResponse.NetworkError -> {
                    Utils.dismissLoadingProgress()
                    Utils.errorAlert(
                        requireActivity(),
                        resources.getString(R.string.internet_connection_error)
                    )
                }

                is NetworkResponse.UnknownError -> {
                    Utils.dismissLoadingProgress()
                    Utils.errorAlert(
                        requireActivity(),
                        "Something went wrong"
                    )
                }
            }
        }
    }

    private fun trendingProductCateAdapter() {
        _binding.rvTrendingCategories.layoutManager =
            GridLayoutManager(requireActivity(), 1, GridLayoutManager.HORIZONTAL, false)
        trendingCategoriesAdapter = TrendingCategoriesAdapter(
            requireActivity(),
            trendingCategoriesList
        ) { id: Int, name: String ->

            trendingProductsSubList.clear()
            trendingcategory_id = id.toString()
            currentPageTrending = 1
            total_pagesTrending = 0
            isLastPageTrending = false
            isLoadingTrending = false
            callTrendingProduct()
        }
        _binding.rvTrendingCategories.adapter = trendingCategoriesAdapter
    }

    private fun callTrendingProduct() {
        Utils.showLoadingProgress(requireActivity())
        val trendingProduct = HashMap<String, String>()
        trendingProduct["theme_id"] = getString(R.string.theme_id)
        lifecycleScope.launch {
            val response =
                if (SharePreference.getStringPref(requireActivity(), SharePreference.token)
                        ?.isEmpty() == true
                ) {
                    ApiClient.getClient(requireActivity())
                        .setTrendingProductGuest(trendingProduct)
                } else {
                    ApiClient.getClient(requireActivity())
                        .setTrendingProduct(trendingProduct)
                }

            when (response) {
                is NetworkResponse.Success -> {
                    Utils.dismissLoadingProgress()
                    val trendingProductResponse = response.body.data
                    when (response.body.status) {
                        1 -> {
                            if ((response.body.data?.data?.size ?: 0) > 0) {
                                _binding.rvMenFashion.show()
                                _binding.view.hide()
                                this@FragHome.currentPageTrending =
                                    trendingProductResponse?.currentPage!!.toInt()
                                this@FragHome.total_pagesTrending =
                                    trendingProductResponse.lastPage!!.toInt()
                                trendingProductResponse.data?.let {
                                    trendingProductsSubList.addAll(it)
                                }

                                if (currentPageTrending >= total_pagesTrending) {
                                    isLastPageTrending = true
                                }
                                isLoadingTrending = false
                            } else {
                                _binding.rvMenFashion.hide()
                                _binding.view.show()
                            }
                            trendingProductAdapter.notifyDataSetChanged()
                        }
                        0 -> {
                            Utils.errorAlert(
                                requireActivity(),
                                trendingProductResponse?.message.toString()
                            )
                        }
                        9 -> {
                            Utils.errorAlert(
                                requireActivity(),
                                trendingProductResponse?.message.toString()
                            )
                            startActivity(Intent(requireActivity(), ActWelCome::class.java))
                            requireActivity().finish()
                            requireActivity().finishAffinity()
                        }
                    }
                }

                is NetworkResponse.ApiError -> {
                    Utils.dismissLoadingProgress()

                    if (response.body.status == 9) {
                        Utils.setInvalidToekn(requireActivity())
                    } else {
                        Utils.errorAlert(
                            requireActivity(),
                            response.body.message.toString()
                        )
                    }
                }

                is NetworkResponse.NetworkError -> {
                    Utils.dismissLoadingProgress()
                    Utils.errorAlert(
                        requireActivity(),
                        resources.getString(R.string.internet_connection_error)
                    )
                }

                is NetworkResponse.UnknownError -> {
                    Utils.dismissLoadingProgress()
                    Utils.errorAlert(
                        requireActivity(),
                        "Something went wrong"
                    )
                }
            }
        }
    }


    private fun callTrendingCategoryProduct(trendingCategoryId: String) {
        Utils.showLoadingProgress(requireActivity())
        val trendingProduct = HashMap<String, String>()
        trendingProduct["main_category_id"] = trendingCategoryId
        trendingProduct["theme_id"] = getString(R.string.theme_id)
        lifecycleScope.launch {
            val response =
                if (SharePreference.getStringPref(requireActivity(), SharePreference.token)
                        ?.isEmpty() == true
                ) {
                    ApiClient.getClient(requireActivity())
                        .setTrendingProductGuest(trendingProduct)
                } else {
                    ApiClient.getClient(requireActivity())
                        .setTrendingProduct(trendingProduct)
                }

            when (response) {
                is NetworkResponse.Success -> {
                    Utils.dismissLoadingProgress()
                    val trendingProductResponse = response.body.data
                    when (response.body.status) {
                        1 -> {
                            if ((response.body.data?.data?.size ?: 0) > 0) {
                                _binding.rvTrending.show()
                                _binding.view.hide()

                                trendingProductResponse?.data?.let {
                                    trendingCategoryProductList.addAll(it)
                                }
//                                 trendingCategoryProductList[0].isSelect = true

                            } else {
                                _binding.rvTrending.hide()
                                _binding.view.show()
                            }
                            trendingCategoryProductAdapter.notifyDataSetChanged()
                        }
                        0 -> {
                            Utils.errorAlert(
                                requireActivity(),
                                trendingProductResponse?.message.toString()
                            )
                        }
                        9 -> {
                            Utils.errorAlert(
                                requireActivity(),
                                trendingProductResponse?.message.toString()
                            )
                            startActivity(Intent(requireActivity(), ActWelCome::class.java))
                            requireActivity().finish()
                            requireActivity().finishAffinity()
                        }
                    }
                }

                is NetworkResponse.ApiError -> {
                    Utils.dismissLoadingProgress()

                    if (response.body.status == 9) {
                        Utils.setInvalidToekn(requireActivity())
                    } else {
                        Utils.errorAlert(
                            requireActivity(),
                            response.body.message.toString()
                        )
                    }
                }

                is NetworkResponse.NetworkError -> {
                    Utils.dismissLoadingProgress()
                    Utils.errorAlert(
                        requireActivity(),
                        resources.getString(R.string.internet_connection_error)
                    )
                }

                is NetworkResponse.UnknownError -> {
                    Utils.dismissLoadingProgress()
                    Utils.errorAlert(
                        requireActivity(),
                        "Something went wrong"
                    )
                }
            }
        }
    }

    private fun trendingProductAdapter(trendingProductsSubLists: ArrayList<FeaturedProductsSub>) {
        _binding.rvMenFashion.layoutManager =
            GridLayoutManager(requireActivity(), 1, GridLayoutManager.VERTICAL, false)
        trendingProductAdapter =
            TrendingProductAdapter(
                requireActivity(),
                trendingProductsSubLists
            ) { i: Int, s: String ->
                if (s == Constants.FavClick) {
                    if (SharePreference.getBooleanPref(
                            requireActivity(),
                            SharePreference.isLogin
                        )
                    ) {
                        Log.e("FavClick", "FavClick")
                        if (trendingProductsSubLists[i].inWhishlist == false) {
                            callWishlist(
                                "add", trendingProductsSubLists[i].id, "TrendingList", i
                            )
                        } else {
                            callWishlist(
                                "remove",
                                trendingProductsSubLists[i].id,
                                "TrendingList",
                                i
                            )
                        }

                    } else {
                        openActivity(ActWelCome::class.java)
                        activity?.finish()
                    }
                } else if (s == Constants.CartClick) {
                    val data = trendingProductsSubLists[i]
                    if (!Utils.isLogin(requireActivity())) {
                        guestUserAddToCart(data, trendingProductsSubLists[i].id)
                    } else {
                        val addtocart = HashMap<String, String>()
                        addtocart["user_id"] =
                            SharePreference.getStringPref(requireActivity(), SharePreference.userId)
                                .toString()
                        addtocart["product_id"] = trendingProductsSubLists[i].id.toString()
                        addtocart["variant_id"] =
                            trendingProductsSubLists[i].defaultVariantId.toString()
                        addtocart["qty"] = "1"
                        addtocart["theme_id"] = getString(R.string.theme_id)
                        addtocartApi(
                            addtocart,
                            trendingProductsSubLists[i].id,
                            trendingProductsSubLists[i].defaultVariantId, i
                        )
                    }
                } else if (s == Constants.ItemClick) {
                    val intent = Intent(requireActivity(), ActProductDetails::class.java)
                    intent.putExtra(Constants.ProductId, trendingProductsSubLists[i].id.toString())
                    startActivity(intent)
                }
            }
        _binding.rvMenFashion.adapter = trendingProductAdapter
    }


    private fun trendingCategoryProductAdapter(trendingProductsSubLists: ArrayList<FeaturedProductsSub>) {
        _binding.rvTrending.layoutManager =
            GridLayoutManager(requireActivity(), 1, GridLayoutManager.VERTICAL, false)
        trendingCategoryProductAdapter =
            TrendingCategoryProductAdapter(
                requireActivity(),
                trendingProductsSubLists
            ) { i: Int, s: String ->
                if (s == Constants.FavClick) {
                    if (SharePreference.getBooleanPref(
                            requireActivity(),
                            SharePreference.isLogin
                        )
                    ) {
                        Log.e("FavClick", "FavClick")
                        if (trendingProductsSubLists[i].inWhishlist == false) {
                            callWishlist(
                                "add",
                                trendingProductsSubLists[i].id,
                                Constants.TrendingCategoryProduct,
                                i
                            )
                        } else {
                            callWishlist(
                                "remove",
                                trendingProductsSubLists[i].id,
                                Constants.TrendingCategoryProduct,
                                i
                            )
                        }

                    } else {
                        openActivity(ActWelCome::class.java)
                        activity?.finish()
                    }
                } else if (s == Constants.CartClick) {
                    val data = trendingProductsSubLists[i]
                    if (!Utils.isLogin(requireActivity())) {
                        guestUserAddToCart(data, trendingProductsSubLists[i].id)
                    } else {
                        val addtocart = HashMap<String, String>()
                        addtocart["user_id"] =
                            SharePreference.getStringPref(requireActivity(), SharePreference.userId)
                                .toString()
                        addtocart["product_id"] = trendingProductsSubLists[i].id.toString()
                        addtocart["variant_id"] =
                            trendingProductsSubLists[i].defaultVariantId.toString()
                        addtocart["qty"] = "1"
                        addtocart["theme_id"] = getString(R.string.theme_id)
                        addtocartApi(
                            addtocart,
                            trendingProductsSubLists[i].id,
                            trendingProductsSubLists[i].defaultVariantId,
                            i
                        )
                    }
                } else if (s == Constants.ItemClick) {
                    val intent = Intent(requireActivity(), ActProductDetails::class.java)
                    intent.putExtra(Constants.ProductId, trendingProductsSubLists[i].id.toString())
                    startActivity(intent)
                }
            }
        _binding.rvTrending.adapter = trendingCategoryProductAdapter
    }

    private fun addtocartApi(
        addtocart: HashMap<String, String>,
        id: Int?,
        defaultVariantId: Int?,
        i: Int?,
    ) {
        Utils.showLoadingProgress(requireActivity())
        lifecycleScope.launch {
            when (val response = ApiClient.getClient(requireActivity())
                .addtocart(addtocart)) {
                is NetworkResponse.Success -> {
                    Utils.dismissLoadingProgress()
                    val addtocart = response.body.data
                    when (response.body.status) {
                        1 -> {
                            dlgConfirm(response.body.data?.message)
                            SharePreference.setStringPref(
                                requireActivity(),
                                SharePreference.cartCount,
                                addtocart?.count.toString()
                            )
                            setCount(addtocart?.count)
                        }
                        0 -> {
                            dlgAlreadyCart(response.body.data?.message, id, defaultVariantId, i)
                        }
                        9 -> {
                            Utils.errorAlert(
                                requireActivity(),
                                addtocart?.message.toString()
                            )
                            openActivity(ActWelCome::class.java)
                            requireActivity().finish()
                            requireActivity().finishAffinity()
                        }
                    }
                }

                is NetworkResponse.ApiError -> {
                    Utils.dismissLoadingProgress()

                    if (response.body.status == 9) {
                        Utils.setInvalidToekn(requireActivity())
                    } else {
                        Utils.errorAlert(
                            requireActivity(),
                            response.body.message.toString()
                        )
                    }
                }

                is NetworkResponse.NetworkError -> {
                    Utils.dismissLoadingProgress()
                    Utils.errorAlert(
                        requireActivity(),
                        resources.getString(R.string.internet_connection_error)
                    )
                }

                is NetworkResponse.UnknownError -> {
                    Utils.dismissLoadingProgress()
                    Utils.errorAlert(
                        requireActivity(),
                        "Something went wrong"
                    )
                }
            }
        }
    }

    private fun setCount(count: Int?) {
        _binding.tvCount.text = count.toString()
    }

    private fun guestUserAddToCart(data: FeaturedProductsSub, id: Int?) {
        Log.e("Position", id.toString())
        val cartData = cartData(data)
        Log.e("cartData2", Gson().toJson(cartData))

        val cartListData = SharePreference.getStringPref(
            requireActivity(),
            SharePreference.GuestCartList
        )
        val type: Type = object : TypeToken<ArrayList<ProductListItem>>() {}.type

        val cartList =
            Gson().fromJson<ArrayList<ProductListItem>>(cartListData, type)
        Log.e("cartListBefore2", Gson().toJson(cartList))

        if (cartList == null) {
            dlgConfirm(data.name + " " + R.string.addsuccess)
            val cartDataList = ArrayList<ProductListItem>()
            cartDataList.add(cartData)

            SharePreference.setStringPref(
                requireActivity(),
                SharePreference.GuestCartList,
                Gson().toJson(cartDataList)
            )
            SharePreference.setStringPref(
                requireActivity(),
                SharePreference.cartCount,
                cartDataList.size.toString()
            )
            _binding.tvCount.text = cartDataList.size.toString()
        } else if (isProductAlreadyAdded(cartList, data.id.toString())) {
            // Utils.errorAlert(requireActivity(), "item already added into cart")
            for (i in 0 until cartList.size) {
                if (cartList[i].productId?.toInt() == id) {
                    val position = cartList[i]
                    Log.e("SelectId", position.toString())
                    dlgAlreadyCart(data.message, data.id, data.defaultVariantId, i)
                }
            }
        } else {
            dlgConfirm(data.name.plus(" ").plus("add successfully."))
            cartList.add(cartData)
            SharePreference.setStringPref(
                requireActivity(),
                SharePreference.GuestCartList,
                Gson().toJson(cartList)
            )
            SharePreference.setStringPref(
                requireActivity(),
                SharePreference.cartCount,
                cartList.size.toString()
            )
            _binding.tvCount.text = cartList.size.toString()
        }
    }

    private fun dlgConfirm(message: String?) {
        val confirmDialogBinding = DlgConfirmBinding.inflate(layoutInflater)
        val dialog = Dialog(requireActivity())
        dialog.setContentView(confirmDialogBinding.root)
        confirmDialogBinding.tvProductDesc.text = message
        confirmDialogBinding.tvCheckOut.setOnClickListener {
            dialog.dismiss()
            openActivity(ActShoppingCart::class.java)
        }
        confirmDialogBinding.tvContinueShopping.setOnClickListener {
            dialog.dismiss()
        }

        confirmDialogBinding.tvCancel.setOnClickListener {
            dialog.dismiss()
        }
        dialog.show()
    }

    private fun dlgAlreadyCart(message: String?, id: Int?, defaultVariantId: Int?, i: Int?) {
        val builder = AlertDialog.Builder(requireActivity())
        builder.setTitle(R.string.app_name)
        builder.setMessage(R.string.product_already_again)
        builder.setPositiveButton(getString(R.string.yes)) { dialogInterface, which ->
            val cartqty = java.util.HashMap<String, String>()

            if (Utils.isLogin(requireActivity())) {
                count = 1
                cartqty["product_id"] = id.toString()
                cartqty["user_id"] =
                    SharePreference.getStringPref(
                        requireActivity(),
                        SharePreference.userId
                    ).toString()
                cartqty["variant_id"] = defaultVariantId.toString()
                cartqty["quantity_type"] = "increase"
                cartqty["theme_id"] = getString(R.string.theme_id)
                cartQtyApi(cartqty, "increase")
            } else {
                val cartListData = SharePreference.getStringPref(
                    requireActivity(),
                    SharePreference.GuestCartList
                )
                val type: Type = object : TypeToken<java.util.ArrayList<ProductListItem>>() {}.type

                var cartList =
                    Gson().fromJson<java.util.ArrayList<ProductListItem>>(cartListData, type)
                var count = cartList[i!!].qty?.toInt()!!
                Log.e("Count", count.toString())
                cartList[i!!].qty = count.plus(1)

                manageOfflineData(cartList)
            }
        }
        builder.setNegativeButton(getString(R.string.no)) { dialogInterface, which ->
            builder.setCancelable(true)
        }
        val alertDialog: AlertDialog = builder.create()
        alertDialog.setCancelable(false)
        alertDialog.show()
    }

    private fun cartQtyApi(
        cartqty: java.util.HashMap<String, String>,
        type: String,
    ) {
        Utils.showLoadingProgress(requireActivity())
        lifecycleScope.launch {
            when (val response = ApiClient.getClient(requireActivity())
                .cartQty(cartqty)) {
                is NetworkResponse.Success -> {
                    Utils.dismissLoadingProgress()
                    val cartQtyResponse = response.body.data
                    SharePreference.setStringPref(
                        requireActivity(),
                        SharePreference.cartCount,
                        response.body.count.toString()
                    )

                    when (response.body.status) {
                        1 -> {

                            Utils.dismissLoadingProgress()
                        }
                        0 -> {
                            Utils.errorAlert(
                                requireActivity(),
                                cartQtyResponse?.message.toString()
                            )
                        }
                        9 -> {
                            Utils.errorAlert(
                                requireActivity(),
                                cartQtyResponse?.message.toString()
                            )
                            openActivity(ActWelCome::class.java)
                        }
                    }
                }

                is NetworkResponse.ApiError -> {
                    Utils.dismissLoadingProgress()

                    if (response.body.status == 9) {
                        Utils.setInvalidToekn(requireActivity())
                    } else {
                        Utils.errorAlert(
                            requireActivity(), response.body.message.toString()
                        )
                    }
                }

                is NetworkResponse.NetworkError -> {
                    Utils.dismissLoadingProgress()
                    Utils.errorAlert(
                        requireActivity(),
                        resources.getString(R.string.internet_connection_error)
                    )
                }

                is NetworkResponse.UnknownError -> {
                    Utils.dismissLoadingProgress()
                    Utils.errorAlert(requireActivity(), "Something went wrong")
                }
            }
        }
    }

    private fun manageOfflineData(cartItemList: java.util.ArrayList<ProductListItem>) {

        SharePreference.setStringPref(
            requireActivity(),
            SharePreference.GuestCartList,
            Gson().toJson(cartItemList)
        )
        SharePreference.setStringPref(
            requireActivity(),
            SharePreference.cartCount,
            cartItemList.size.toString()
        )
    }

    private fun cartData(data: FeaturedProductsSub): ProductListItem {
        return ProductListItem(
            data.coverImagePath.toString(),
            data.defaultVariantId.toString(),
            data.finalPrice.toString(),
            data.discountPrice.toString(),
            data.id.toString(),
            1,
            data.name.toString(),
            "",
            "",
            data.variantName.toString(),
            data.originalPrice.toString()
        )
    }

    private fun isProductAlreadyAdded(cartList: ArrayList<ProductListItem>, id: String): Boolean {
        return cartList.filter { it.productId == id.toString() }.size == 1
    }


    private fun callWishlist(type: String, id: Int?, itemType: String, position: Int) {
        Utils.showLoadingProgress(requireActivity())
        val wishlist = HashMap<String, String>()
        wishlist["user_id"] =
            SharePreference.getStringPref(requireActivity(), SharePreference.userId).toString()
        wishlist["product_id"] = id.toString()
        wishlist["wishlist_type"] = type
        wishlist["theme_id"] = getString(R.string.theme_id)
        lifecycleScope.launch {
            when (val response = ApiClient.getClient(requireActivity())
                .setWishlist(wishlist)) {
                is NetworkResponse.Success -> {
                    Utils.dismissLoadingProgress()
                    val wishlistResponse = response.body.data
                    when (response.body.status) {
                        1 -> {
                            if (type == "add") {
                                when (itemType) {
                                    "CategoriesProduct" -> {
                                        featuredProductsSubList[position].inWhishlist = true
                                        categoriesProductAdapter.notifyItemChanged(position)
                                    }
                                    "BestsellersList" -> {
                                        bestsellersList[position].inWhishlist = true
                                        bestSellersAdapter.notifyItemChanged(position)
                                    }
                                    "TrendingList" -> {
                                        trendingProductsSubList[position].inWhishlist = true
                                        trendingProductAdapter.notifyItemChanged(position)
                                    }
                                    Constants.TrendingCategoryProduct -> {
                                        trendingCategoryProductList[position].inWhishlist = true
                                        trendingCategoryProductAdapter.notifyItemChanged(position)
                                    }
                                }
                            } else {
                                when (itemType) {
                                    "CategoriesProduct" -> {
                                        featuredProductsSubList[position].inWhishlist = false
                                        categoriesProductAdapter.notifyItemChanged(position)
                                    }
                                    "BestsellersList" -> {
                                        bestsellersList[position].inWhishlist = false
                                        bestSellersAdapter.notifyItemChanged(position)
                                    }
                                    "TrendingList" -> {
                                        trendingProductsSubList[position].inWhishlist = false
                                        trendingProductAdapter.notifyItemChanged(position)
                                    }
                                    Constants.TrendingCategoryProduct -> {
                                        trendingCategoryProductList[position].inWhishlist = false
                                        trendingCategoryProductAdapter.notifyItemChanged(position)
                                    }
                                }
                            }
                        }
                        0 -> {
                            Utils.errorAlert(
                                requireActivity(),
                                wishlistResponse?.message.toString()
                            )
                        }
                        9 -> {
                            Utils.errorAlert(
                                requireActivity(),
                                wishlistResponse?.message.toString()
                            )
                            openActivity(ActWelCome::class.java)
                            requireActivity().finish()
                            requireActivity().finishAffinity()
                        }

                    }
                }

                is NetworkResponse.ApiError -> {
                    Utils.dismissLoadingProgress()

                    if (response.body.status == 9) {
                        Utils.setInvalidToekn(requireActivity())
                    } else {
                        Utils.errorAlert(
                            requireActivity(),
                            response.body.message.toString()
                        )
                    }
                }

                is NetworkResponse.NetworkError -> {
                    Utils.dismissLoadingProgress()
                    Utils.errorAlert(
                        requireActivity(),
                        resources.getString(R.string.internet_connection_error)
                    )
                }

                is NetworkResponse.UnknownError -> {
                    Utils.dismissLoadingProgress()
                    Utils.errorAlert(
                        requireActivity(),
                        "Something went wrong"
                    )
                }
            }
        }
    }


    override fun onResume() {
        super.onResume()
        lifecycleScope.launch {
            coroutineScope {

                val currency = lifecycleScope.async {
                    callCurrencyApi()
                }
                Log.e("currency",
                    SharePreference.getStringPref(requireActivity(), SharePreference.currency)
                        ?: "")
                currency.await()
                val headerContent=async {
                    callHeaderContentApi()
                }
                headerContent.await()
                trendingProductsSubList.clear()
                trendingProductAdapter(trendingProductsSubList)
                val trendingProduct=async {
                    callTrendingProduct()
                }
                trendingProduct.await()

                featuredProductsList.clear()
                featuredProductsSubList.clear()

                val featureProduct=async {     callFeaturedProduct()
                }
                featureProduct.await()

                categoriesProductsAdapter(featuredProductsSubList)
                val categoryProduct=async {  callCategoryProduct() }
                categoryProduct.await()


                bestsellersList.clear()
                bestsellerAdapter(bestsellersList)
                val bestSeller=async {      callBestseller()}
                bestSeller.await()





                trendingCategoryProductList.clear()
                trendingCategoryProductAdapter(trendingCategoryProductList)
                val trending=async { callTrendingCategories()}
                trending.await()
                Log.e("api calling done ","done")

            }
        }



        _binding.tvCount.text =
            SharePreference.getStringPref(requireActivity(), SharePreference.cartCount)
    }
}