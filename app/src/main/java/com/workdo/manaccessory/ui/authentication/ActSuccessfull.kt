package com.workdo.manaccessory.ui.authentication

import android.view.View
import com.workdo.manaccessory.base.BaseActivity
import com.workdo.manaccessory.databinding.ActSuccessfullBinding
import com.workdo.manaccessory.ui.activity.MainActivity

class ActSuccessfull : BaseActivity() {
    private lateinit var _binding: ActSuccessfullBinding
    override fun setLayout(): View = _binding.root

    override fun initView() {
        _binding = ActSuccessfullBinding.inflate(layoutInflater)
        init()
    }

    private fun init() {
        _binding.btnGotoyourstore.setOnClickListener { openActivity(MainActivity::class.java) }
    }
}
