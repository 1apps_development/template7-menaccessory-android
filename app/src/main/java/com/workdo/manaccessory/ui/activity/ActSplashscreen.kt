package com.workdo.manaccessory.ui.activity

import android.annotation.SuppressLint
import android.app.Activity
import android.content.Intent
import android.content.pm.PackageInfo
import android.content.pm.PackageManager
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.util.Base64
import android.util.Log
import android.view.View
import androidx.lifecycle.lifecycleScope
import com.workdo.manaccessory.R
import com.workdo.manaccessory.utils.Utils
import com.workdo.manaccessory.api.ApiClient
import com.workdo.manaccessory.base.BaseActivity
import com.workdo.manaccessory.databinding.ActSplashscreenBinding
import com.workdo.manaccessory.utils.SharePreference
import com.workdo.manaccessory.remote.NetworkResponse
import com.workdo.manaccessory.ui.authentication.ActWelCome
import kotlinx.coroutines.launch
import java.security.MessageDigest
import java.security.NoSuchAlgorithmException

class ActSplashscreen : BaseActivity() {
    private lateinit var _binding: ActSplashscreenBinding

    override fun setLayout(): View = _binding.root

    override fun initView() {
        _binding = ActSplashscreenBinding.inflate(layoutInflater)
        init()
    }

    private fun init() {
        getApiUrl()
        printKeyHash(this@ActSplashscreen)
        Utils.getLog("getShaKey", printKeyHash(this@ActSplashscreen)!!)
        Handler(Looper.getMainLooper()).postDelayed({
            openActivity(MainActivity::class.java)
            finish()
        }, 3000)
    }

    private fun getApiUrl() {
        lifecycleScope.launch {
            val request=HashMap<String,String>()
            request["theme_id"]=resources.getString(R.string.theme_id)
            when (val response = ApiClient.getBaseClient(this@ActSplashscreen).getApiurl(request)) {
                is NetworkResponse.Success -> {
                    Utils.dismissLoadingProgress()
                    val apiUrl = response.body
                    when (response.body.status) {
                        1 -> {
                            SharePreference.setStringPref(
                                this@ActSplashscreen,
                                SharePreference.BaseUrl,
                                apiUrl.data?.base_url.toString().plus("/")
                            )

                            SharePreference.setStringPref(
                                this@ActSplashscreen,
                                SharePreference.PaymentUrl,
                                apiUrl.data?.payment_url.toString().plus("/")
                            )
                            SharePreference.setStringPref(
                                this@ActSplashscreen,
                                SharePreference.ImageUrl,
                                apiUrl.data?.image_url.toString()
                            )
                            getExterUrl()
                        }

                        0 -> {
                            Utils.errorAlert(
                                this@ActSplashscreen,
                                apiUrl.message.toString()
                            )
                        }
                        9 -> {
                            Utils.errorAlert(
                                this@ActSplashscreen,
                                apiUrl.message.toString()
                            )
                            openActivity(ActWelCome::class.java)

                        }
                    }
                }

                is NetworkResponse.ApiError -> {
                    if (response.body.status == 9) {
                        Utils.setInvalidToekn(this@ActSplashscreen)
                    } else {
                        Utils.errorAlert(
                            this@ActSplashscreen,
                            response.body.message.toString()
                        )
                    }
                }

                is NetworkResponse.NetworkError -> {
                    Utils.errorAlert(
                        this@ActSplashscreen,
                        resources.getString(R.string.internet_connection_error)
                    )
                }

                is NetworkResponse.UnknownError -> {
                    Utils.errorAlert(
                        this@ActSplashscreen,
                        "Something went wrong"
                    )
                }
            }
        }
    }

    private fun getExterUrl() {
        val hashmap = HashMap<String,String>()
        hashmap["theme_id"]=getString(R.string.theme_id)
        lifecycleScope.launch {
            when (val response = ApiClient.getClient(this@ActSplashscreen).extraUrl(hashmap)) {
                is NetworkResponse.Success -> {
                    Utils.dismissLoadingProgress()
                    val extraUrl = response.body
                    when (response.body.status) {
                        1 -> {
                            SharePreference.setStringPref(
                                this@ActSplashscreen,
                                SharePreference.Terms,
                                extraUrl.data?.terms.toString()
                            )
                            SharePreference.setStringPref(
                                this@ActSplashscreen,
                                SharePreference.Contact_Us,
                                extraUrl.data?.contactUs.toString()
                            )
                            SharePreference.setStringPref(
                                this@ActSplashscreen,
                                SharePreference.insta,
                                extraUrl.data?.insta.toString()
                            )
                            SharePreference.setStringPref(
                                this@ActSplashscreen,
                                SharePreference.youtube,
                                extraUrl.data?.youtube.toString()
                            )
                            SharePreference.setStringPref(
                                this@ActSplashscreen,
                                SharePreference.messanger,
                                extraUrl.data?.messanger.toString()
                            )
                            SharePreference.setStringPref(
                                this@ActSplashscreen,
                                SharePreference.twitter,
                                extraUrl.data?.twitter.toString()
                            )
                            SharePreference.setStringPref(
                                this@ActSplashscreen,
                                SharePreference.returnPolicy,
                                extraUrl.data?.returnPolicy.toString()
                            )
                        }

                        0 -> {
                            Utils.errorAlert(
                                this@ActSplashscreen,
                                extraUrl.message.toString()
                            )
                        }
                        9 -> {
                            Utils.errorAlert(
                                this@ActSplashscreen,
                                extraUrl.message.toString()
                            )
                            openActivity(ActWelCome::class.java)

                        }
                    }
                }

                is NetworkResponse.ApiError -> {
                    if (response.body.status == 9) {
                        Utils.setInvalidToekn(this@ActSplashscreen)
                    } else {
                        Utils.errorAlert(
                            this@ActSplashscreen,
                            response.body.message.toString()
                        )
                    }
                }

                is NetworkResponse.NetworkError -> {
                    Utils.errorAlert(
                        this@ActSplashscreen,
                        resources.getString(R.string.internet_connection_error)
                    )
                }

                is NetworkResponse.UnknownError -> {
                    Utils.errorAlert(
                        this@ActSplashscreen,
                        "Something went wrong"
                    )
                }
            }
        }
    }

    @SuppressLint("PackageManagerGetSignatures")
    fun printKeyHash(context: Activity): String? {
        val packageInfo: PackageInfo
        var key: String? = null
        try {
            val packageName = context.applicationContext.packageName
            packageInfo = context.packageManager.getPackageInfo(
                packageName,
                PackageManager.GET_SIGNATURES
            )
            Log.e("Package Name=", context.applicationContext.packageName)
            for (signature in packageInfo.signatures) {
                val md: MessageDigest = MessageDigest.getInstance("SHA")
                md.update(signature.toByteArray())
                key = String(Base64.encode(md.digest(), 0))
            }
        } catch (e1: PackageManager.NameNotFoundException) {
            Log.e("Name not found", e1.toString())
        } catch (e: NoSuchAlgorithmException) {
            Log.e("No such an algorithm", e.toString())
        } catch (e: Exception) {
            Log.e("Exception", e.toString())
        }
        return key
    }
}